﻿Public Class HomeController
    Inherits System.Web.Mvc.Controller

    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Index() As ActionResult

        Return SetIndex()

    End Function

    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Index1() As ActionResult

        Return SetIndex()
       
    End Function


    'Function Refresh() As ActionResult


    '    Return SetIndex()

    'End Function

    Function SetIndex()
        If Session("UserID") = Nothing Then
            Return RedirectToAction("Login")
        End If

        ViewData("Message") = "Overview of error messages and rows different between Vessel Manager and Fleet Manager"

        Dim newErrorsCount As Integer = ErrorsBL.GetNumberOfNewErrors()
        Dim unequalRowsCount As Integer = TableComparesBL.GetNumberOfUnequalRows()
        Dim unequalTablesCount As Integer = TableComparesBL.GetNumberOfUnequalTables()

        ViewData("NewErrorMessages") = newErrorsCount



        ViewData("TableCompareUnequalRows") = unequalRowsCount
        ViewData("TableCompareUnequalTables") = unequalTablesCount

        If (newErrorsCount > 0) Then
            ViewBag.ErrorsStatusBoxCSS = "redbox"
        Else
            ViewBag.ErrorsStatusBoxCSS = "greenbox"
        End If

        If (unequalRowsCount > 0 Or unequalTablesCount > 0) Then
            ViewBag.TableCompareStatusBoxCSS = "redbox"
        Else
            ViewBag.TableCompareStatusBoxCSS = "greenbox"
        End If

        Dim newErrors = ErrorsBL.GetErrors(DateTime.Now())
        Dim newErrorsDone = ErrorsBL.GetErrors("Done", DateTime.Now())

        ViewData("NewErrors24Hrs") = newErrors.Count
        ViewData("NewErrorsDone24Hrs") = newErrorsDone.Count

        'Dim unequalRowsCount24Hrs As Integer = TableComparesBL.GetNumberOfUnequalRows(DateTime.Now())
        'Dim unequalTablesCount24Hrs As Integer = TableComparesBL.GetNumberOfUnequalTables(DateTime.Now())


        'ViewData("TableCompareUnequalRows24Hrs") = unequalRowsCount24Hrs
        'ViewData("TableCompareUnequalTables24Hrs") = unequalTablesCount24Hrs

        TempData("Top10Errors") = ErrorsBL.GetTop10Errors()

        Return View()
    End Function
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function About() As ActionResult
        ViewData("Message") = "Your app description page."

        Return View()
    End Function
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Contact() As ActionResult
        ViewData("Message") = "Your contact page."

        Return View()
    End Function

    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Login() As ActionResult
        ViewData("Message") = "Your app description page."

        'Dim s As String = HttpContext.Current.User.Identity.Name

        'HttpCookie myCookie = Request.Cookies["myCookie"];
        Dim crypt As CryptographyBL = New CryptographyBL()

        Dim myCookie As HttpCookie = Request.Cookies("SCSCookie")
        Dim uName As String
        Dim PW As String
        If Not IsNothing(myCookie) Then
            uName = myCookie("UserName")
            PW = crypt.Decrypt(myCookie("Password"))

            Return RedirectToAction("Login", "Account", New With {.username = uName, .password = PW})
        End If




        Return RedirectToAction("Login", "Account")
    End Function

 
End Class
