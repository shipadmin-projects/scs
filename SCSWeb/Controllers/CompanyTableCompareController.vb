﻿Public Class CompanyTableCompareController
    Inherits System.Web.Mvc.Controller

    Private db As New SCSDB
    '
    ' GET: /CompanyTableCompare
    ''' <summary>
    ''' return a list of companies with number of errors and color for status
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function Index() As ActionResult

        Dim l As List(Of CompanyTableCompare) = New List(Of CompanyTableCompare)

        Dim companies = From Company In db.Companies.ToList()


        For Each c As Company In companies
            Dim ctc As CompanyTableCompare = New CompanyTableCompare()
            ctc.Name = c.Name
            ctc.ID = c.ID
            ctc.RowsDifferent = TableComparesBL.GetNumberOfRowDifferences(c.ID)
            ctc.RowsEqual = TableComparesBL.GetNumberOfEqualRows(c.ID)
            l.Add(ctc)
        Next

        Return View(l)
        'gfhgfhgfhgfh


    End Function

End Class