﻿Imports System.Web.Helpers
Imports System.Data.Objects

Public Class StatisticsController
    Inherits System.Web.Mvc.Controller
    Private db As New SCSDB

    '
    ' GET: /Statistics
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Index() As ActionResult
        GetName()
        Return View(GetDataGraph(DateTime.UtcNow.Year))
    End Function
    <HttpPost()>
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Index(ByVal YearSelect As String) As ActionResult
        GetName()
        Return View(GetDataGraph(YearSelect))
    End Function
    <HttpPost()>
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Public Function SvComment(Comment As String) As JsonResult
        Dim SvCom As New Comments
        Try
            SvCom.CommentMessage = Comment
            SvCom.Created = DateTime.Now
            SvCom.Updated = DateTime.Now
            SvCom.UserID = Convert.ToInt32(Session("UserID"))
            SvCom.UserName = GetAllName(Convert.ToInt32(Session("UserID")))
            db.Comments.Add(SvCom)
            db.SaveChanges()
            db.Dispose()
        Catch ex As Exception

        End Try

        Return Json(SvCom)
    End Function

    <HttpPost()>
  <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Public Function DelComment(CommentID As Integer) As JsonResult
        Dim DelCom As Comments = db.Comments.Find(CommentID)
        If IsNothing(DelCom) Then
        Else
            db.Comments.Remove(DelCom)
            db.SaveChanges()
        End If
        Return Json(True)
    End Function
    '
    ' GET: /Comment Load
    Public Function CommentLoad() As JsonResult
        Dim nDate As Date = DateTime.Now.Date.ToString("yyyy-MM-dd") 'Date variable for the Comment
        Dim SvCom As List(Of Comments) = db.Comments.Where(Function(x) EntityFunctions.TruncateTime(x.Created) = EntityFunctions.TruncateTime(nDate)).ToList
        Return Json(SvCom, JsonRequestBehavior.AllowGet)
    End Function

    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function GetDataGraph(YearSelect As String) As StatsDisplay
        Dim xValuesList As New List(Of String)()
        Dim yValuesList As New List(Of Integer)()
        Dim filename = "~/Images/GraphA.jpg"
        Dim aStat As New StatsDisplay
        aStat.Statistics = New List(Of Statistics)
        'aStat.CommentList = db.Comments.Where(Function(x) EntityFunctions.TruncateTime(x.Created) = EntityFunctions.TruncateTime(nDate)).ToList 'Query for the comments
        ViewData("SelectYear") = aStat.YearList
        aStat.ErrCount = db.Messages.Where(Function(x) x.Timestamp.Year = YearSelect).Count

        xValuesList.Add("Total Errors")
        yValuesList.Add(aStat.ErrCount)
        For i As Integer = 1 To 12
            Dim StatList As Statistics = New Statistics
            StatList.ErrCount = db.Messages.Where(Function(x) x.Timestamp.Year = YearSelect And x.Timestamp.Month = i).Count
            StatList.MonthName = i
            xValuesList.Add(StatList.ActualMonth)
            yValuesList.Add(StatList.ErrCount)

            aStat.Statistics.Add(StatList)
        Next
        aStat.CurrentYear = YearSelect
        '''''''''''''''''''''''''''''''''''''''
        Dim temp As String = "<Chart>" & vbCr & vbLf &
"                      <ChartAreas>" & vbCr & vbLf &
"                        <ChartArea Name=""Default"" _Template_=""All"">" & vbCr & vbLf &
"                          <AxisY>" & vbCr & vbLf &
"                            <LabelStyle Font=""Verdana, 12px"" />" & vbCr & vbLf &
"                          </AxisY>" & vbCr & vbLf &
"                          <AxisX LineColor=""64, 64, 64, 64"" Interval=""1"">" & vbCr & vbLf &
"                            <LabelStyle Font=""Verdana, 12px"" />" & vbCr & vbLf &
"                          </AxisX>" & vbCr & vbLf &
"                        </ChartArea>" & vbCr & vbLf &
"                      </ChartAreas>" & vbCr & vbLf &
"                    </Chart>"
        '''''''''''''''''''''''''''''''''''''''

        Dim ch1 As New Chart(width:=960, height:=1000, theme:=temp)

        ch1.AddTitle("Monthly Statistics for the year " & YearSelect)
        ch1.AddSeries(chartType:="Bar", xValue:=xValuesList, yValues:=yValuesList)
        ch1.Save(filename)
        aStat.ImagePath = filename
        Return aStat
    End Function
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function GetName() As Integer
        Dim i As Integer = Convert.ToInt32(Session("UserID"))
        Dim U As SCSWeb.User = db.Users.Where(Function(x) x.ID = i).FirstOrDefault()
        If U Is Nothing Then
            Redirect("~/Account/Login")
        Else
            ViewData("aName") = U.Name
        End If


        Return 0
    End Function
    Function GetAllName(UserID As Integer) As String
        Dim i As Integer = Convert.ToInt32(UserID)
        Dim U As SCSWeb.User = db.Users.Where(Function(x) x.ID = i).FirstOrDefault()
        Return U.Name
    End Function

    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        db.Dispose()
        MyBase.Dispose(disposing)
    End Sub
End Class