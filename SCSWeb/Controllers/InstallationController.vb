﻿Imports System.Data.Entity

Public Class InstallationController
    Inherits System.Web.Mvc.Controller

    Private db As New SCSDB

    '
    ' GET: /Installation/
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Index() As ActionResult
        SetViewData()

        Dim data = From Installation In db.Installations.OrderBy(Function(a) a.Name).ToList()

        For Each d As Installation In data
            d.Company = (From x In db.Companies Where x.ID = d.CompanyID Select x).First()
        Next

        Return View(data)

    End Function
    <HttpPost>
    Function Index(ByVal SearchApplication As String, ByVal SearchCompany As String) As ActionResult
        'SetViewData()
        Dim selectedCompany As Integer = -1
        Dim selectedInstallation As Integer = -1
        Dim selectedApplication As Integer = -1



        If (SearchCompany <> "") Then
            selectedCompany = Integer.Parse(SearchCompany)
        End If

        Dim cmps = From Companies In db.Companies Select Companies

        Dim apps = ApplicationBL.GetApplications(selectedCompany)

        Dim appExistForCompany As Boolean = False

        If (SearchApplication <> "") Then
            selectedApplication = Integer.Parse(SearchApplication)
            For Each app As Application In apps
                If app.ID = SearchApplication Then
                    appExistForCompany = True
                End If
            Next            
        End If

        If Not appExistForCompany Then
            selectedApplication = -1
        End If



        ViewData("SearchApplication") = New SelectList(apps.OrderBy(Function(a) a.Name).ToList(), "ID", "Name")
        ViewData("SearchCompany") = New SelectList(cmps.OrderBy(Function(a) a.Name).ToList(), "ID", "Name")

        'Dim data = From Installation In db.Installations.OrderBy(Function(a) a.Name).ToList()

        Dim data = InstallationBL.GetInstallations(selectedCompany, selectedApplication).OrderBy(Function(a) a.Name).ToList()

        For Each d As Installation In data
            d.Company = (From x In db.Companies Where x.ID = d.CompanyID Select x).First()
        Next

        Return View(data)

    End Function
    '
    ' INSTALLATION INACTIVE
    '<HttpPost>
    'Sub DisplayJquery(IDList As List(Of InstallationUpdate))
    '    Try
    '        For Each IntID As InstallationUpdate In IDList
    '            Dim iChk As Installation = db.Installations.Find(IntID.ID)
    '            If IntID.isChk = True Then
    '                iChk.InActive = 1
    '            Else
    '                iChk.InActive = 0
    '            End If
    '            db.SaveChanges()
    '        Next
    '        db.Dispose()
    '    Catch ex As Exception

    '    End Try

    'End Sub



    '
    ' GET: /Installation/Details/5
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Details(Optional ByVal id As Integer = Nothing) As ActionResult
        Dim installation As Installation = db.Installations.Find(id)
        If IsNothing(installation) Then
            Return HttpNotFound()
        End If
        Return View(installation)
    End Function

    '
    ' GET: /Installation/Create
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Create() As ActionResult
        Return View()
    End Function

    '
    ' POST: /Installation/Create

    <HttpPost()> _
    <ValidateAntiForgeryToken()> _
     <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Create(ByVal installation As Installation) As ActionResult
        If ModelState.IsValid Then
            db.Installations.Add(installation)
            db.SaveChanges()
            Return RedirectToAction("Index")
        End If

        Return View(installation)
    End Function

    '
    ' GET: /Installation/Edit/5
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Edit(Optional ByVal id As Integer = Nothing) As ActionResult
        Dim installation As Installation = db.Installations.Find(id)
        If IsNothing(installation) Then
            Return HttpNotFound()
        End If
        Return View(installation)
    End Function

    '
    ' POST: /Installation/Edit/5

    <HttpPost()> _
    <ValidateAntiForgeryToken()> _
    Function Edit(ByVal installation As Installation) As ActionResult
        If ModelState.IsValid Then
            db.Entry(installation).State = EntityState.Modified
            db.SaveChanges()
            Return RedirectToAction("Index")
        End If

        Return View(installation)
    End Function

    '
    ' GET: /Installation/Delete/5
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Delete(Optional ByVal id As Integer = Nothing) As ActionResult
        Dim installation As Installation = db.Installations.Find(id)
        If IsNothing(installation) Then
            Return HttpNotFound()
        End If
        Return View(installation)
    End Function

    '
    ' POST: /Installation/Delete/5

    <HttpPost()> _
    <ActionName("Delete")> _
    <ValidateAntiForgeryToken()> _
     <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function DeleteConfirmed(ByVal id As Integer) As RedirectToRouteResult
        Dim installation As Installation = db.Installations.Find(id)
        db.Installations.Remove(installation)
        db.SaveChanges()
        Return RedirectToAction("Index")
    End Function

    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        db.Dispose()
        MyBase.Dispose(disposing)
    End Sub


    Sub SetViewData()

        Dim cmps = From Companies In db.Companies Select Companies
        Dim apps = From Application In db.Applications Select Application

        ViewData("SearchApplication") = New SelectList(apps.OrderBy(Function(a) a.Name).ToList(), "ID", "Name")
        ViewData("SearchCompany") = New SelectList(cmps.OrderBy(Function(a) a.Name).ToList(), "ID", "Name")

    End Sub

End Class