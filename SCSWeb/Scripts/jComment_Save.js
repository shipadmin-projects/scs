﻿$(function () {
    /* Load Events */
    LoadComments();

    function LoadComments() {
        var UrlConn = $("#CommentList").data("url");
        //alert(UrlConn);
        $.ajax({
            type: 'GET',
            url: UrlConn,
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function (data) {
                var divDetails = "";
                $.each(data, function (i, value) {
                    var date = new Date(parseInt(value.Updated.substr(6)));
                    divDetails += "<li ID='" + value.ID + "'><div class='bubble clearfix'>";
                    divDetails += "<span style='width:5%'></span>";
                    divDetails += "<span class='bubble-u' style='width:30%'>" + value.UserName + "<br><div class='DateFont'>" + date.toDateString() + "</div></span>";
                    divDetails += "<span class='bubble-content' style='width:50%'><div class='point'></div>" + value.CommentMessage + "</span>";
                    divDetails += "<span style='width:15%'><input type='button' class='btnDelComment' value='x' data-cmdel='" + value.ID + "'></input></span>";
                    divDetails += "</div></li>";
                });

                $("#CommentList").html(divDetails);

                regDelete();
            },
            error: function () {
                alert('Failed to Load');
            }
        });
    }

    function regDelete() {
        //Delete line
        $(".btnDelComment").click(function () {
            var myUrl = $(".URLHidden").data("url");
            var CommentID = $(this).data("cmdel");
            $.ajax({
                type: 'POST',
                url: myUrl,
                data: "{'CommentID':'" + CommentID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                success: function (data) {
                    //alert('Entered');
                    var elem = document.getElementById(CommentID);
                    elem.parentNode.removeChild(elem);
                    //return false;
                },
                error: function () {
                    alert('Failed');
                }
            });
        });
    }

    //Appending Line
    $("#btnSvComment").click(function () {
        var comments = $("#txtComments").val();
        comments = comments.replace("'", "\\'");
        var UrlConn = $(this).data("url");
        $.ajax({
            type: 'POST',
            //url: '@Url.Action("SvComment", "Statistics")',
            url: UrlConn,            
            data: "{'Comment':'" + comments + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function (data)
            {
                $("#txtComments").val("");
                var URLData = "@Url.Action('SvComment', 'Statistics')";
                var date = new Date(parseInt(data.Updated.substr(6)));
                var divDetails = "<li ID='" + data.ID + "'><div class='bubble clearfix'>";
                divDetails += "<span style='width:5%'></span>";
                divDetails += "<span class='bubble-u' style='width:30%'>" + data.UserName + "<br><div class='DateFont'>" + date.toDateString() + "</div></span>";
                divDetails += "<span class='bubble-content' style='width:50%'><div class='point'></div>" + data.CommentMessage + "</span>";
                divDetails += "<span style='width:15%'><input type='button' class='btnDelComment' value='x' data-cmdel='" + data.ID + "'></input></span>";
                divDetails += "</div></li>";
                $("#CommentList").append(divDetails);

                regDelete();
            },
            error: function ()
            {
                alert('Failed');
            }
        });
    });          
});

