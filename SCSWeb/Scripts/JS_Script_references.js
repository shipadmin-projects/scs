﻿
    $('#sortbtn').click(function () {
        var arrXIDList = [];

        $("#iList input[type=checkbox]").each(function (i, value) {

            var strXIDVal = {
                ID: value.id,
                isChk: value.checked
            }
            arrXIDList.push(strXIDVal);
        });

        var json = JSON.stringify({ IDList: arrXIDList });

        // For debugging the JSON array.
        //alert(json);

        $.ajax({
            type: 'POST',
            url: "Installation/DisplayJquery",
            data: json,
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function () {
                //alert('Success');
                $("#AlertList").html('<p><strong>Saving Complete</strong></p>')
            },
            error: function () {
                //alert('Failed');
                $("#AlertList").html('<p><strong>Saving Failed</strong></p>')
            }
        });
    });
