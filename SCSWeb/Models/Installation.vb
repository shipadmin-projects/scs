﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Globalization
Imports System.Data.Entity

Public Class Installation
    Inherits BaseDBO

    <Display(Name:="Installation")> _
    Public Property Name As String
    Public Property CompanyID As Integer
    Public Property ApplicationID As Integer
    Public Property InActive As Integer
    Public Property ID As Integer

    Public Company As Company
    Public Application As Application
    Public Messages As ICollection(Of Message)
    Public TableCompares As ICollection(Of TableCompare)

    Public ReadOnly Property InActiveBool As Boolean
        Get
            If InActive = 1 Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

End Class
