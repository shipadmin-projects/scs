﻿Public Class InstallationUpdate
    Private _ID As Integer
    Public Property ID() As Integer
        Get
            Return _ID
        End Get
        Set(ByVal value As Integer)
            _ID = value
        End Set
    End Property

    Private _isChk As Boolean
    Public Property isChk() As Boolean
        Get
            Return _isChk
        End Get
        Set(ByVal value As Boolean)
            _isChk = value
        End Set
    End Property

End Class
