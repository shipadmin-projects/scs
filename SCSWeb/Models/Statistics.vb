﻿Public Class Statistics
    Public Property MonthName As String
    Public Property ErrCount As Integer
    Public Property YearSelected As Integer

    Public ReadOnly Property ActualMonth As String
        Get
            Dim x As String
            Select Case MonthName
                Case "1"
                    x = "January"
                Case "2"
                    x = "Febraury"
                Case "3"
                    x = "March"
                Case "4"
                    x = "April"
                Case "5"
                    x = "May"
                Case "6"
                    x = "June"
                Case "7"
                    x = "July"
                Case "8"
                    x = "August"
                Case "9"
                    x = "September"
                Case "10"
                    x = "October"
                Case "11"
                    x = "November"
                Case "12"
                    x = "December"
                Case Else
                    x = "Invalid Month"
            End Select
            Return x
        End Get
    End Property
    Public ReadOnly Property StatusColor As String
        Get
            If (ErrCount > 0) Then
                Return "Red"
            Else
                Return "Green"
            End If
        End Get
    End Property

End Class
