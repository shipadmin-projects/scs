﻿Public Class TableCompareCompany
    Public Property ID As Integer
    Public Property Name As String
    Public Property RowsEqual As Integer
    Public Property RowsUnequal As Integer
    Public Property TablesEqual As Integer
    Public Property TablesUnequal As Integer
    Public Property RowsFM As Integer
    Public Property RowsVM As Integer
    Public Property Updated As DateTime



    Public ReadOnly Property RowsTotal As Integer
        Get
            Return RowsEqual + RowsUnequal
        End Get
    End Property

    Public ReadOnly Property TablesTotal As Integer
        Get
            Return TablesEqual + TablesUnequal
        End Get
    End Property


    Public ReadOnly Property StatusColor As String
        Get
            If (RowsUnequal > 0) Then
                Return "Red"
            Else
                Return "Green"
            End If
        End Get
    End Property
End Class
