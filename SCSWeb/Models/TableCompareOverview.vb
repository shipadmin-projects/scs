﻿
Public Class TableCompareOverview
    Public Property totalVM As Integer
    Public Property totalFM As Integer
    Public Property Name As String
    Public ReadOnly Property RowDifference As Integer
        Get
            Return Math.Abs(totalVM - totalFM)
        End Get
    End Property
    Public ReadOnly Property IsRowsDifferent As Boolean
        Get
            Return Not (totalVM = totalFM)
        End Get
    End Property
    Public ReadOnly Property StatusColor As String
        Get
            If (IsRowsDifferent) Then
                Return "Red"
            Else
                Return "Green"
            End If
        End Get
    End Property
End Class
