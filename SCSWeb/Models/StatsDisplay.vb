﻿
Public Class StatsDisplay

    Public Property CurrentYear As String
    Public Property ErrCount As Integer
    Public Property ImagePath As String
    Public Property CommentList As List(Of Comments)
    Private _YearList As List(Of SelectListItem)
    Public ReadOnly Property YearList() As List(Of SelectListItem)
        Get
            _YearList = New List(Of SelectListItem)
            For x As Integer = 0 To 10
                Dim xItem As New SelectListItem()
                xItem.Text = DateTime.UtcNow.Year + x
                xItem.Value = DateTime.UtcNow.Year + x
                _YearList.Add(xItem)
            Next
            Return _YearList
        End Get
        'Set(ByVal value As List(Of SelectListItem))
        '    _YearList = value
        'End Set
    End Property
    Private _Statistics As List(Of Statistics)
    Public Property Statistics() As List(Of Statistics)
        Get
            Return _Statistics
        End Get
        Set(ByVal value As List(Of Statistics))
            _Statistics = value
        End Set
    End Property


End Class
