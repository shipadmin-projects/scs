﻿Imports System.ComponentModel.DataAnnotations

Public Class Company
    Inherits BaseDBO

    <Display(Name:="Company")> _
    Public Property Name As String

    Public Messages As ICollection(Of Message)

End Class
