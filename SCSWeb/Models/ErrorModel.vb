﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Globalization
Imports System.Data.Entity


Public Class ErrorModel
    Public Property ID As Integer
    Public Property MessageID As Guid
    Public Property Status As String
    Public Property MessageHeader As String
    Public Property MessageDetails As String
    Public Property Updated As DateTime
    Public Property ApplicationName As String
    Public Property CompanyName As String
    Public Property InstallationName As String
    Public Property StatusChangedByUserName As String
    Public Property Timestamp As DateTime

    Public ReadOnly Property MessageHeaderShortened() As String
        Get
            If (MessageHeader.Count > 80) Then
                Return MessageHeader.Substring(0, 80)
            Else
                Return MessageHeader
            End If
        End Get
    End Property

    Public ReadOnly Property MessageDetailsShortened() As String
        Get
            If (MessageDetails.Count > 400) Then
                Return MessageDetails.Substring(0, 400)
            Else
                Return MessageDetails
            End If
        End Get
    End Property

    Public ReadOnly Property HiddenHtml() As String
        Get
            If (Me.Status <> "New") Then
                Return "style='display:none;'"
            Else
                Return ""
            End If
        End Get
    End Property


End Class
