﻿@ModelType SCSWeb.User

@Code
    ViewData("Title") = "Edit"
End Code

<h2>Edit</h2>

@Using Html.BeginForm()
    @Html.AntiForgeryToken()
    @Html.ValidationSummary(True)

    @<fieldset>
        <legend>User</legend>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Email)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Email)
            @Html.ValidationMessageFor(Function(model) model.Email)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Username)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Username)
            @Html.ValidationMessageFor(Function(model) model.Username)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Password)
        </div>
        <div class="editor-field">
            @Html.PasswordFor(Function(model) model.Password)
            @Html.ValidationMessageFor(Function(model) model.Password)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Name)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Name)
            @Html.ValidationMessageFor(Function(model) model.Name)
        </div>

        @Html.HiddenFor(Function(model) model.ID)

                  <div class="editor-label">
            @Html.LabelFor(Function(model) model.ForViewing)
        </div>
        <div class="editor-field">
            @Html.CheckBoxFor(Function(model) model.ForViewing)
            @Html.ValidationMessageFor(Function(model) model.ForViewing)
        </div>

         @Html.HiddenFor(Function(model) model.Created)


@*        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Updated)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Updated)
            @Html.ValidationMessageFor(Function(model) model.Updated)
        </div>*@


        <p>
            <input type="submit" value="Save" />
        </p>
    </fieldset>
End Using

<div>
    @Html.ActionLink("Back to List", "Index")
</div>

@Section Scripts
    @Scripts.Render("~/bundles/jqueryval")
End Section
