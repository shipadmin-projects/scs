﻿@ModelType IEnumerable(Of SCSWeb.User)

@Code
    ViewBag.title = "Users"
End Code

    
<h2>@ViewBag.title</h2>

<p>
    @Using Html.BeginForm("Search", "User", FormMethod.Post)
        @Html.TextBox("SearchString")
        @<input type="submit" value="Search" />
    End Using
       
</p>
<p>
    You searched for: @ViewBag.SearchString
</p>

<table>
    <tr>
        <th>
           Email
        </th>
        <th>
            Username
        </th>
        <th>
            Password
        </th>
        <th>
           Name
        </th>
        <th>
            Updated
        </th>
        <th>
           Created
        </th>
   
    </tr>

@For Each item In Model
    Dim currentItem = item
    @<tr>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Email)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Username)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Password)
        </td>
        
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Name)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Updated)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Created)
        </td>
    </tr>
Next

</table>
