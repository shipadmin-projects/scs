﻿@ModelType IEnumerable(Of SCSWeb.CompanyTableCompare)

@Code
    ViewData("Title") = "Companies"
End Code

<h2>Companies</h2>

<table>
    <tr>
        <th>
           Company
        </th>
        <th>
            Equal
        </th>
        <th>
            Different
        </th>
         <th>
            Total
        </th>

    </tr>

@For Each item In Model
    Dim currentItem = item
    @<tr>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Name)
        </td>
        <td >
            @Html.DisplayFor(Function(modelItem) currentItem.RowsEqual)
        </td>
           <td style="color:@currentItem.StatusColor">
            @Html.DisplayFor(Function(modelItem) currentItem.RowsDifferent)
        </td>
          <td>
            @Html.DisplayFor(Function(modelItem) currentItem.RowsTotal)
        </td>
    </tr>
Next

</table>
