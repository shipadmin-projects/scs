﻿@ModelType SCSWeb.StatsDisplay

@Code
    ViewData("Title") = "Index"
End Code
<body id="StatBody">
    <form id="Statistics" method="post">
        <h2>Statistics Total of @Html.DisplayFor(Function(model) model.ErrCount) for the year @Html.DisplayFor(Function(model) model.CurrentYear)</h2>
        Choose a year @Html.DropDownList("YearSelect", New SelectList(ViewData("SelectYear"), "Value", "Text"), "-- Select Year -- ", New With {.onChange = "this.form.submit();"})
        <table style="width:100%">
            <tr id="box1head">
                <th style="width:5%">

                </th>
                <th style="width:15%">
                    Year
                </th>
                <th style="width:20%">
                    Month
                </th>
                <th style="width:20%">
                    Error Count
                </th>
                <th style="width:25%">

                </th>
                <th style="width:10%">

                </th>
            </tr>
            @For Each item In Model.Statistics
                @<tr id="StatTable">
                    <td style="width:5%"></td>
                    <td style="width:15%">
                        @Html.DisplayFor(Function(model) model.CurrentYear)
                        @*@Html.ActionLink(currentItem.Name, "Table", New With {.id = currentItem.Name})*@
                    </td>
                    <td style="width:20%">
                        @Html.DisplayFor(Function(x) item.ActualMonth)
                    </td>
                    <td style="width:20%;color:@item.StatusColor;">
                        @Html.DisplayFor(Function(x) item.ErrCount)
                    </td>
                    <td style="width:25%"></td>
                    <td style="width:10%">
                        @*@Html.DisplayFor(Function(modelItem) currentItem.Updated)*@
                    </td>

                </tr>
            Next

        </table>

        <img src="@Url.Content(Model.ImagePath)" alt="Image" />

        <table style="width:100%">
            <tr id="box1head">
                <th style="width:5%">

                </th>
                <th style="width:15%">
                    Comments
                </th>
                <th style="width:40%">
                    User : @ViewData("aName")
                </th>
                <th style="width:25%">

                </th>
                <th style="width:10%">

                </th>
            </tr>
        </table>
        <table style="width:100%">
            <tr id="Commentbox">
                <td style="width:5%"></td>
                <td style="width:15%"></td>
                <td style="width:40%">
                    <textarea id="txtComments"></textarea>
                </td>
                <td style="width:35%">
                    <input type="button" id="btnSvComment" value="Save" data-url="@Url.Action("SvComment", "Statistics")">
                </td>
            </tr>
        </table>
        @*<div id="CommentList" data-url="@Url.Action("CommentLoad", "Statistics")"></div>*@
        <input type="hidden" class="URLHidden" data-url="@Url.Action("DelComment", "Statistics")">
        <ul id="CommentList" style="width: 100%; list-style-type:none" data-url="@Url.Action("CommentLoad", "Statistics")">
            
        </ul>
    </form>
</body>

@Scripts.Render("~/Scripts/jComment_Save.js")