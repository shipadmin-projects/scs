﻿@ModelType IEnumerable(Of SCSWeb.Application)

@Code
    ViewData("Title") = "Applications"
End Code

<h2>Applications</h2>

<table>
    <tr>
        <th>
            @Html.DisplayNameFor(Function(model) model.Name)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.Updated)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.Created)
        </th>
        <th></th>
    </tr>

@For Each item In Model
    Dim currentItem = item
    @<tr>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Name)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Updated)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Created)
        </td>
    </tr>
Next

</table>
