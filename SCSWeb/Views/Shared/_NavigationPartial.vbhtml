﻿<nav>
    @If Session("UserID") <> Nothing Then
    @<ul id="menu">
        <li>@Html.ActionLink("Home", "Index", "Home")</li>
        <li>@Html.ActionLink("Errors", "Index", "Errors")</li>
        <li>@Html.ActionLink("Table Compare", "Index", "TableCompare")</li>
        <li>@Html.ActionLink("Applications", "Index", "Application")</li>
        <li>@Html.ActionLink("Companies", "Index", "Company")</li>
        @*<li>@Html.ActionLink("Messages", "Index", "Message")</li>*@
        <li>@Html.ActionLink("Installations", "Index", "Installation")</li>

        @If Session("ViewingOnly") = False Then
            @<li>@Html.ActionLink("Users", "Index", "User")</li>
        End If
        

    </ul>
        End If
</nav>
