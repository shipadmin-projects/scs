﻿@ModelType IEnumerable(Of SCSWeb.ErrorModel)


<table style="width: 800px;">
    @For Each item In Model
        Dim currentItem = item
        @<tr style="vertical-align: top;">

            <td>
                <input type="checkbox" name="selectedObjects" value="@currentItem.ID">

            </td>
            <td style="width: 50px;">
                @Html.ActionLink(currentItem.ID, "Details", New With {.id = currentItem.ID})
            </td>
            <td>
                @Html.DisplayFor(Function(modelItem) currentItem.CompanyName)
            </td>
            <td>
                @Html.DisplayFor(Function(modelItem) currentItem.ApplicationName)
            </td>
            <td>
                @Html.DisplayFor(Function(modelItem) currentItem.InstallationName)
            </td>
            <td style="width: 100px;">
                <div style="width: 180px;">
                    @Html.DisplayFor(Function(modelItem) currentItem.Timestamp)
                </div>
            </td>
            <td title="@Html.DisplayFor(Function(modelItem) currentItem.MessageHeader)">

                @Html.DisplayFor(Function(modelItem) currentItem.MessageHeaderShortened)

            </td>
            <td title="@Html.DisplayFor(Function(modelItem) currentItem.MessageDetails)">

                @Html.DisplayFor(Function(modelItem) currentItem.MessageDetailsShortened)

            </td>

        </tr>
    Next
</table>




