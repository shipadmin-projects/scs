﻿@Code
    Layout = Nothing
End Code

<!DOCTYPE html>

<html>
<head runat="server">
    
        <meta charset="utf-8" />
        <title>@ViewData("Title") - SCS</title>
        <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
        <meta name="viewport" content="width=device-width" />
        @Styles.Render("~/Content/Shipadmin")
         <script src="@Url.Content("~/Scripts/jquery-1.8.2.min.js")" type="text/javascript"></script>
     
</head>
<body>
    <div>
        
    </div>
    <div id="body" style="width:100%;height:100%;">
            @RenderSection("featured", required:=false)
            <section>
               <table style="width:100%;height:100%;">
                    <tr style="height:100%;vertical-align:middle;">
                        <td style="width:100%;padding-left:500px;padding-top:100px;">
                             @RenderBody()
                        </td>
                    </tr>
                </table>

            </section>
        </div>
</body>
</html>
 