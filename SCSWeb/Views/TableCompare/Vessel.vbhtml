﻿@ModelType IEnumerable(Of SCSWeb.TableCompare)

<h2> @Html.ActionLink("Table Compare", "Index") -  @Html.ActionLink(ViewBag.CompanyName, "Company", New With {.id = ViewBag.CompanyID}) - @ViewBag.VesselName</h2>

<table style="width:70%">
    <tr>
        <th style="width:20%">
           Table
        </th>
        <th style="width:10%">
            VM
        </th>
         <th style="width:10%">
            FM
        </th>
        <th style="width:10%">
            Diff
        </th>
         <th style="width:20%">
            Last Update
        </th>
    </tr>

@For Each item In Model
    Dim currentItem = item
    @<tr>
        <td style="width:20%">
          @*  @Html.DisplayFor(Function(modelItem) currentItem.TableNameVM)*@
              @Html.ActionLink(currentItem.TableNameVM, "Table", New With {.id = currentItem.ID})
        </td>
        <td style="width:10%">
            @Html.DisplayFor(Function(modelItem) currentItem.TableRowsVM)
        </td>
          <td style="width:10%">
            @Html.DisplayFor(Function(modelItem) currentItem.TableRowsFM)
        </td>
         <td  style="width:10%;color:@currentItem.StatusColor;">
            @Html.DisplayFor(Function(modelItem) currentItem.RowDifference)
        </td>
         <td style="width:20%">
            @Html.DisplayFor(Function(modelItem) currentItem.Updated)
        </td>
        
    </tr>
Next

</table>
