﻿@ModelType IEnumerable(Of SCSWeb.TableCompare)

@Code
    ViewBag.title = "Table Compare"
End Code

    
<h2>@ViewBag.title</h2>

<p>
    @Using Html.BeginForm("Search", "TableCompare", FormMethod.Post)
        @Html.TextBox("SearchString")
        @<input type="submit" value="Search" />
    End Using
       
</p>
<p>
    You searched for: @ViewBag.SearchString
</p>

<table>
    <tr>
        <th>
           Vm ID
        </th>
        <th>
            Fm ID
        </th>
        <th>
            FM TableName 
        </th>
        <th>
          VM TableName 
        </th>
        <th>
            TableRows
        </th>
        <th>
            Updated
        </th>
        <th>
           Created
        </th>
    <th>
           Test
        </th>
    </tr>

@For Each item In Model
    Dim currentItem = item
    @<tr>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.VmID)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.FmID)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.TableNameFM)
        </td>
        
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.TableNameVM)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.TableRowsVM)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.TableRowsFM)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Updated)
        </td>
         <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Created)
        </td>
         @*<td>
            @Html.DisplayFor(Function(modelItem) currentItem.Test)
        </td>*@
    </tr>
Next

</table>
