﻿@ModelType IEnumerable(Of SCSWeb.TableCompareoverview)
@Code
    ViewData("Title") = "Overview"
End Code

<h2>@Html.ActionLink("Table Compare", "Index")  -  Overview</h2>
<table style="width:70%">
    <tr>
        <th style="width:20%">
            Table
        </th>
        <th style="width:10%">
            VM
        </th>
        <th style="width:10%">
            FM
        </th>
        <th style="width:10%">
            Diff
        </th>
        <th style="width:20%">
            
        </th>
    </tr>

    @For Each item In Model
        Dim currentItem = item
        @<tr>
            <td style="width:20%">
                @*  @Html.DisplayFor(Function(modelItem) currentItem.TableNameVM)*@
                @Html.ActionLink(currentItem.Name, "Table", New With {.id = currentItem.Name})
            </td>
            <td style="width:10%">
                @Html.DisplayFor(Function(modelItem) currentItem.totalVM)
            </td>
            <td style="width:10%">
                @Html.DisplayFor(Function(modelItem) currentItem.totalFM)
            </td>
            <td style="width:10%;color:@currentItem.StatusColor;">
                @Html.DisplayFor(Function(modelItem) currentItem.RowDifference)
            </td>
            <td style="width:20%">
                @*@Html.DisplayFor(Function(modelItem) currentItem.Updated)*@
            </td>

        </tr>
    Next

</table>