﻿@ModelType IEnumerable(Of SCSWeb.TableCompareVessel)

<h2>@Html.ActionLink("Table Compare", "Index") -  @ViewBag.CompanyName</h2>

<table style="width:80%">
    <tr>
        <th style="width:20%">
           Vessel
        </th>
        <th colspan="2" style="width:10%">
            Tables
        </th>
        <th style="width:10%">
            Rows VM
        </th>
        <th style="width:10%">
            Rows FM
        </th>
        <th style="width:10%">
            Rows diff
        </th>
         <th style="width:20%">
           Last Update
        </th>
    </tr>

@For Each item In Model
    Dim currentItem = item
    @<tr>
        <td style="width:20%">
             @Html.ActionLink(currentItem.Name, "Vessel", New With {.id = currentItem.ID})
        </td>
          <td style="text-align:left;">
            @Html.DisplayFor(Function(modelItem) currentItem.TablesTotal)
        </td>
           <td style="text-align:left;color:@currentItem.StatusColor">
            @Html.DisplayFor(Function(modelItem) currentItem.TablesUnequal)
        </td>
        <td style="text-align:left;width:10%">
            @Html.DisplayFor(Function(modelItem) currentItem.RowsVM)
        </td>
        <td style="text-align:left;width:10%">
            @Html.DisplayFor(Function(modelItem) currentItem.RowsFM)
        </td>
           <td style="text-align:left;width:10%;color:@currentItem.StatusColor">
            @Html.DisplayFor(Function(modelItem) currentItem.RowsUnequal)
        </td>
         <td style="text-align:left;width:20%">
            @Html.DisplayFor(Function(modelItem) currentItem.Updated)
        </td>
    </tr>
Next
    
     <tr>
        <td>
             
        </td>
        <td style="text-align:right;">
          
        </td>
        <td style="text-align:right;">
            
        </td>
        <td style="text-align:right;">
            
        </td>
        <td style="text-align:right;">
            
        </td>
        <td style="text-align:right;">
            
        </td>
          <td style="text-align:right;">
            
        </td>
    </tr>
</table>
