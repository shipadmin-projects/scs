﻿@ModelType IEnumerable(Of SCSWeb.TableCompareVessel)

@Code
    ViewData("Title") = "Table"
End Code

<h2> @Html.ActionLink("Table Compare", "Index") -  @Html.ActionLink(ViewBag.CompanyName, ViewBag.ActionNameCompany, New With {.id = ViewBag.CompanyID}) -  @Html.ActionLink(ViewBag.VesselName, ViewBag.ActionName, New With {.id = ViewBag.Vesselid})  - @ViewBag.TableName
</h2>

<p>
 </p>
<table>
    <tr>
       <th style="width:20%">
           Vessel
        </th>
        <th colspan="2" style="width:10%">
            Tables
        </th>
        <th style="width:10%">
            Rows VM
        </th>
        <th style="width:10%">
            Rows FM
        </th>
        <th style="width:10%">
            Rows diff
        </th>
         <th style="width:20%">
           Last Update
        </th>
    </tr>

@For Each item In Model
    Dim currentItem = item
    @<tr>
       <td style="width:20%">
            @Html.DisplayFor(Function(modelItem) currentItem.Name)
        </td>
          <td style="text-align:left;">
            @Html.DisplayFor(Function(modelItem) currentItem.TablesTotal)
        </td>
           <td style="text-align:left;color:@currentItem.StatusColor">
            @Html.DisplayFor(Function(modelItem) currentItem.TablesUnequal)
        </td>
        <td style="text-align:left;width:10%">
            @Html.DisplayFor(Function(modelItem) currentItem.RowsVM)
        </td>
        <td style="text-align:left;width:10%">
            @Html.DisplayFor(Function(modelItem) currentItem.RowsFM)
        </td>
           <td style="text-align:left;width:10%;color:@currentItem.StatusColor">
            @Html.DisplayFor(Function(modelItem) currentItem.RowsUnequal)
        </td>
         <td style="text-align:left;width:20%">
            @Html.DisplayFor(Function(modelItem) currentItem.Updated)
        </td>
    </tr>
Next

</table>
