﻿@ModelType SCSWeb.Message

@Code
    ViewData("Title") = "Edit"
End Code

<h2>Edit</h2>

@Using Html.BeginForm()
    @Html.AntiForgeryToken()
    @Html.ValidationSummary(True)

    @<fieldset>
        <legend>Message</legend>

        @Html.HiddenFor(Function(model) model.ID)

        @Html.HiddenFor(Function(model) model.MessageID)

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Application)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Application)
            @Html.ValidationMessageFor(Function(model) model.Application)
        </div>

        <div class="editor-label">
            @Html.LabelFor(Function(model) model.Company)
        </div>
        <div class="editor-field">
            @Html.EditorFor(Function(model) model.Company)
            @Html.ValidationMessageFor(Function(model) model.Company)
        </div>

        <p>
            <input type="submit" value="Save" />
        </p>
    </fieldset>
End Using

<div>
    @Html.ActionLink("Back to List", "Index")
</div>

@Section Scripts
    @Scripts.Render("~/bundles/jqueryval")
End Section
