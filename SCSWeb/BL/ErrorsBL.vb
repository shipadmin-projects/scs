﻿Public Class ErrorsBL

    Public Shared Property NumberOfErrors As Integer = 50

    ''' <summary>
    ''' Gets number of registered new errors for the selected installation for the selected company
    ''' </summary>
    Public Shared Function GetNumberOfNewErrors(ByVal CID As Integer, ByVal IID As Integer, ByVal AID As Integer, ByVal SearchString As String) As Integer
        'Dim db As SCSDB = New SCSDB()

        'Dim sql As String = "SELECT * FROM [Message] WHERE [Type] = 'Error' AND Status = 'New'"

        'If (CID > 0) Then
        '    sql += " AND CompanyID = " + CID.ToString()
        'End If

        'If (IID > 0) Then
        '    sql += " AND InstallationID = " + IID.ToString()
        'End If

        'If (AID > 0) Then
        '    sql += " AND ApplicationID = " + AID.ToString()
        'End If

        'If (SearchString <> "") Then
        '    sql += " AND (MessageHeader LIKE '%" + SearchString + "%' OR MessageDetails LIKE '%" + SearchString + "%')"
        'End If

        'sql += " ORDER BY Timestamp DESC"

        'Dim numberOfErrors As Integer = db.Database.SqlQuery(Of Message)(sql).Count


        'Return errors

        Dim db As New SCSDB
        Dim numberOfErrors As Integer = 0

        If db.Messages.Count = 0 Then
            Return numberOfErrors
        End If

        If (CID > 0 And IID > 0 And AID < 0) Then
            If (SearchString <> "") Then
                numberOfErrors = (From m In db.Messages
                                        Where m.Type = "Error" And m.Status = "New" And m.CompanyID = IID And m.InstallationID = IID And (m.MessageHeader.Contains(SearchString) _
                                            Or m.MessageDetails.Contains(SearchString)) Select m.ID).Count
            Else
                numberOfErrors = (From m In db.Messages
                                     Where m.Type = "Error" And m.Status = "New" And m.CompanyID = IID And m.InstallationID = IID
                                     Select m.ID).Count
            End If


        ElseIf (CID > 0 And IID < 0 And AID < 0) Then
            If (SearchString <> "") Then
                numberOfErrors = (From m In db.Messages
                       Where m.Type = "Error" And m.Status = "New" And m.CompanyID = CID And (m.MessageHeader.Contains(SearchString) _
                                            Or m.MessageDetails.Contains(SearchString)) Select m.ID).Count
            Else
                numberOfErrors = (From m In db.Messages
                       Where m.Type = "Error" And m.Status = "New" And m.CompanyID = CID
                       Select m.ID).Count
            End If

        ElseIf (CID < 0 And IID > 0 And AID < 0) Then
            If (SearchString <> "") Then
                numberOfErrors = (From m In db.Messages
                      Where m.Type = "Error" And m.Status = "New" And m.InstallationID = IID _
                      And (m.MessageHeader.Contains(SearchString) Or m.MessageDetails.Contains(SearchString)) Select m.ID).Count
            Else
                numberOfErrors = (From m In db.Messages
                      Where m.Type = "Error" And m.Status = "New" And m.InstallationID = IID
                      Select m.ID).Count
            End If 'end
        ElseIf (CID > 0 And IID > 0 And AID > 0) Then
            If (SearchString <> "") Then
                numberOfErrors = (From m In db.Messages
                                        Where m.Type = "Error" And m.Status = "New" And m.CompanyID = IID And m.InstallationID = IID And m.ApplicationID = AID And (m.MessageHeader.Contains(SearchString) _
                                            Or m.MessageDetails.Contains(SearchString)) Select m.ID).Count
            Else
                numberOfErrors = (From m In db.Messages
                                     Where m.Type = "Error" And m.Status = "New" And m.CompanyID = IID And m.InstallationID = IID And m.ApplicationID = AID
                                     Select m.ID).Count
            End If


        ElseIf (CID > 0 And IID < 0 And AID > 0) Then
            If (SearchString <> "") Then
                numberOfErrors = (From m In db.Messages
                       Where m.Type = "Error" And m.Status = "New" And m.CompanyID = CID And m.ApplicationID = AID And (m.MessageHeader.Contains(SearchString) _
                                            Or m.MessageDetails.Contains(SearchString)) Select m.ID).Count
            Else
                numberOfErrors = (From m In db.Messages
                       Where m.Type = "Error" And m.Status = "New" And m.CompanyID = CID And m.ApplicationID = AID
                       Select m.ID).Count
            End If

        ElseIf (CID < 0 And IID > 0 And AID > 0) Then
            If (SearchString <> "") Then
                numberOfErrors = (From m In db.Messages
                      Where m.Type = "Error" And m.Status = "New" And m.InstallationID = IID And m.ApplicationID = AID _
                      And (m.MessageHeader.Contains(SearchString) Or m.MessageDetails.Contains(SearchString)) Select m.ID).Count
            Else
                numberOfErrors = (From m In db.Messages
                      Where m.Type = "Error" And m.Status = "New" And m.InstallationID = IID And m.ApplicationID = AID
                      Select m.ID).Count
            End If
        Else
            If (SearchString <> "") Then
                numberOfErrors = (From m In db.Messages
                       Where m.Type = "Error" And m.Status = "New" _
                         And (m.MessageHeader.Contains(SearchString) Or m.MessageDetails.Contains(SearchString)) Select m.ID).Count
            Else
                numberOfErrors = (From m In db.Messages
                                       Where m.Type = "Error" And m.Status = "New"
                                       Select m.ID).Count
            End If

        End If

        Return numberOfErrors
    End Function

    Public Shared Function GetNumberOfNewErrors(ByVal CompanyID As Integer, ByVal InstallationID As Integer) As Integer
        Return GetNumberOfNewErrors(CompanyID, InstallationID, -1, "")
    End Function

    Public Shared Function GetNumberOfNewErrors(ByVal CompanyID As Integer) As Integer
        Return GetNumberOfNewErrors(CompanyID, -1, -1, "")
    End Function

    Public Shared Function GetNumberOfNewErrors() As Integer
        Return GetNumberOfNewErrors(-1, -1, -1, "")
    End Function

    ''' <summary>
    ''' Gets the [NumberOfErrors] sorted by Timestamp DESC
    ''' </summary>
    ''' 
    Public Shared Function GetErrors(ByVal Status As String, ByVal CID As Integer, ByVal IID As Integer, ByVal AID As Integer, ByVal SearchString As String, Optional ByVal compareDate As DateTime = Nothing, Optional ByVal updateDate As DateTime = Nothing) As List(Of SCSWeb.ErrorModel)
        Dim db As SCSDB = New SCSDB()

        Dim sql As String = "SET DATEFORMAT DMY; SELECT TOP " + NumberOfErrors.ToString() + " m.ID, m.MessageID, a.Name AS ApplicationName, c.Name AS CompanyName, i.Name AS InstallationName, u.Name AS StatusChangedByUserName  ,m.MessageDetails, m.MessageHeader, m.MessageID, m.ID, m.Updated, m.[Status], m.Timestamp FROM [Message] m LEFT JOIN [Installations] i ON i.ID = m.InstallationID LEFT JOIN [Applications] a ON a.ID = m.ApplicationID LEFT JOIN [Companies] c ON c.ID = m.CompanyID  LEFT JOIN [Users] u ON u.ID = m.StatusChangedByUserID "
        ' Dim sql As String = "SET DATEFORMAT DMY; SELECT m.ID, m.MessageID, a.Name AS ApplicationName, c.Name AS CompanyName, i.Name AS InstallationName, u.Name AS StatusChangedByUserName  ,m.MessageDetails, m.MessageHeader, m.MessageID, m.ID, m.Updated, m.[Status], m.Timestamp FROM [Message] m LEFT JOIN [Installations] i ON i.ID = m.InstallationID LEFT JOIN [Applications] a ON a.ID = m.ApplicationID LEFT JOIN [Companies] c ON c.ID = m.CompanyID  LEFT JOIN [Users] u ON u.ID = m.StatusChangedByUserID "
        sql += " WHERE 1=1"
        If (CID > 0) Then
            sql += " AND m.CompanyID = " + CID.ToString()
        End If

        If (IID > 0) Then
            sql += " AND m.InstallationID = " + IID.ToString()
        End If

        If (AID > 0) Then
            sql += " AND m.ApplicationID = " + AID.ToString()
        End If

        If (SearchString <> "") Then
            sql += " AND (m.MessageHeader LIKE '%" + SearchString + "%' OR m.MessageDetails LIKE '%" + SearchString + "%')"
        End If

        If (Status <> "") Then
            sql += " AND m.Status = '" + Status + "'"
        End If

        If IsNothing(compareDate) = False Then
            If compareDate <> DateTime.MinValue Then
                'sql += " AND m.Created >= '" + compareDate.ToString() + "'"
                sql += " AND m.Created >=  GETDATE() - 1 "
            End If
        End If

        If IsNothing(updateDate) = False Then
            If updateDate <> DateTime.MinValue Then
                sql += " AND m.Updated >= GETDATE() - 1 "
            End If
        End If



        sql += " ORDER BY m.Updated DESC"

        'I used Linq Extension Method
        'GroupJoin is Equivalent to Left Join

        'Dim error2 = db.Messages.GroupJoin(db.Installations, Function(mess) mess.InstallationID, Function(inst) inst.ID, Function(mess, inst) New With {mess, inst}) _
        '                        .GroupJoin(db.Applications, Function(mess2) mess2.mess.ApplicationID, Function(appl) appl.ID, Function(mess2, appl) New With {mess2, appl}) _
        '                        .GroupJoin(db.Companies, Function(mess3) mess3.mess2.mess.CompanyID, Function(comp) comp.ID, Function(mess3, comp) New With {mess3, comp}) _
        '                        .GroupJoin(db.Users, Function(mess4) mess4.mess3.mess2.mess.StatusChangedByUserID, Function(user) user.ID, Function(mess4, user) New With {.Application = mess4.mess3.appl, .Installation = mess4.mess3.mess2.inst, .Company = mess4.comp, .Message = mess4.mess3.mess2.mess, .Users = user}) _
        '                        .Take(NumberOfErrors).ToList()

        ''Dim sql As String = "SELECT TOP " + NumberOfErrors.ToString() + " m.ID, m.MessageID, a.Name AS ApplicationName, c.Name AS CompanyName, i.Name AS InstallationName, u.Name AS StatusChangedByUserName  ,m.MessageDetails, m.MessageHeader, m.MessageID, m.ID, m.Updated, m.[Status] FROM [Message] m LEFT JOIN [Installations] i ON i.ID = m.InstallationID LEFT JOIN [Applications] a ON a.ID = m.ApplicationID LEFT JOIN [Companies] c ON c.ID = m.CompanyID  LEFT JOIN [Users] u ON u.ID = m.StatusChangedByUserID "


        'If (CID > 0) Then
        '    error2 = error2.Where(Function(c) c.Message.CompanyID = CID).ToList()
        '    'sql += " AND m.CompanyID = " + CID.ToString()
        'End If

        'If (IID > 0) Then
        '    error2 = error2.Where(Function(i) i.Message.InstallationID = IID).ToList()
        '    'sql += " AND m.InstallationID = " + IID.ToString()
        'End If

        'If (AID > 0) Then
        '    error2 = error2.Where(Function(a) a.Message.ApplicationID = AID).ToList()
        '    'sql += " AND m.ApplicationID = " + AID.ToString()
        'End If

        'If (SearchString <> "") Then
        '    error2 = error2.Where(Function(hd) hd.Message.MessageHeader.Contains(SearchString) Or hd.Message.MessageDetails.Contains(SearchString)).ToList()
        '    'sql += " AND (m.MessageHeader LIKE '%" + SearchString + "%' OR m.MessageDetails LIKE '%" + SearchString + "%')"
        'End If

        'If (Status <> "") Then
        '    error2 = error2.Where(Function(st) st.Message.Status = Status).ToList()
        '    'sql += " AND m.Status = '" + Status + "'"
        'End If


        Dim errors = db.Database.SqlQuery(Of ErrorModel)(sql).ToList()

        'Dim db As New SCSDB
        'Dim errors

        'If SearchString <> "" And Status <> "" Then
        '    errors = (From m In db.Messages Join i In db.Installations On i.ID Equals m.InstallationID _
        '                         Join a In db.Applications On a.ID Equals m.ApplicationID _
        '                         Join c In db.Companies On c.ID Equals m.CompanyID _
        '                         Join u In db.Users On u.ID Equals m.StatusChangedByUserID _
        '                         Where 1 = 1 And (m.MessageHeader.Contains(SearchString) Or m.MessageDetails.Contains(SearchString)) _
        '                         And m.Status = "New" Select m.MessageID, ApplicationName = a.Name, CompanyName = c.Name, InstallationName = i.Name, _
        '                                        StatusChangedByUserName = u.Name, m.MessageDetails, m.MessageHeader, m.ID, m.Updated, m.Timestamp)

        '    Return errors
        'ElseIf SearchString = "" And Status <> "" Then
        '    errors = (From m In db.Messages Join i In db.Installations On i.ID Equals m.InstallationID _
        '                       Join a In db.Applications On a.ID Equals m.ApplicationID _
        '                       Join c In db.Companies On c.ID Equals m.CompanyID _
        '                       Join u In db.Users On u.ID Equals m.StatusChangedByUserID _
        '                       Where 1 = 1 And m.Status = "New" Select m.MessageID, ApplicationName = a.Name, CompanyName = c.Name, InstallationName = i.Name, _
        '                                      StatusChangedByUserName = u.Name, m.MessageDetails, m.MessageHeader, m.ID, m.Updated, m.Timestamp)

        '    
        'Else


        'End If


        Return errors

    End Function


    Public Shared Function GetErrorChunks(ByVal number As String, ByVal Status As String, ByVal CID As Integer, ByVal IID As Integer, ByVal AID As Integer, ByVal SearchString As String, Optional ByVal compareDate As DateTime = Nothing, Optional ByVal updateDate As DateTime = Nothing, Optional ByVal Pages As Integer = 0, Optional ByVal SpecifiedColumn As String = "") As List(Of SCSWeb.ErrorModel)
        Dim db As SCSDB = New SCSDB()
        Dim sql As String = ""

        If number <> "" Then
            sql = "SET DATEFORMAT DMY; SELECT TOP " + number.ToString() + "  m.ID, m.MessageID, a.Name AS ApplicationName, c.Name AS CompanyName, i.Name AS InstallationName, u.Name AS StatusChangedByUserName  ,m.MessageDetails, m.MessageHeader, m.MessageID, m.ID, m.Updated, m.[Status], m.Timestamp FROM [Message] m LEFT JOIN [Installations] i ON i.ID = m.InstallationID LEFT JOIN [Applications] a ON a.ID = m.ApplicationID LEFT JOIN [Companies] c ON c.ID = m.CompanyID  LEFT JOIN [Users] u ON u.ID = m.StatusChangedByUserID "
        Else
            sql = "SELECT ID, MessageID,ApplicationName, CompanyName, InstallationName, StatusChangedByUserName  ,MessageDetails, MessageHeader, Updated, [Status], [Timestamp] FROM    ( SELECT    ROW_NUMBER() OVER ( ORDER BY m.[Timestamp] DESC ) AS RowNum, m.ID, m.MessageID, a.Name AS ApplicationName, c.Name AS CompanyName, i.Name AS InstallationName, u.Name AS StatusChangedByUserName  ,m.MessageDetails, m.MessageHeader, m.Updated, m.[Status], m.Timestamp FROM [Message] m LEFT JOIN [Installations] i ON i.ID = m.InstallationID LEFT JOIN [Applications] a ON a.ID = m.ApplicationID LEFT JOIN [Companies] c ON c.ID = m.CompanyID  LEFT JOIN [Users] u ON u.ID = m.StatusChangedByUserID "
        End If

        sql += " WHERE 1=1"

        If SpecifiedColumn = "" Then

            If (CID > 0) Then
                sql += " AND m.CompanyID = " + CID.ToString()
            End If

            If (IID > 0) Then
                sql += " AND m.InstallationID = " + IID.ToString()
            End If

            If (AID > 0) Then
                sql += " AND m.ApplicationID = " + AID.ToString()
            End If

            If (SearchString <> "") Then
                sql += " AND (m.MessageHeader LIKE '%" + SearchString + "%' OR m.MessageDetails LIKE '%" + SearchString + "%')"
            End If

            If (Status <> "") Then
                sql += " AND m.Status = '" + Status + "'"
            End If

            If IsNothing(compareDate) = False Then
                If compareDate <> DateTime.MinValue Then
                    sql += " AND m.Created >= '" + compareDate.ToString() + "'"
                End If
            End If

            If IsNothing(updateDate) = False Then
                If updateDate <> DateTime.MinValue Then
                    sql += " AND m.Updated >= '" + updateDate.ToString() + "'"
                End If
            End If

        Else
            If SpecifiedColumn.Contains("Timestamp") = False Then
                sql += " AND " + SpecifiedColumn + " like '%" + SearchString.ToString() + "%'"
            ElseIf SpecifiedColumn = "Timestamp" Then
                Dim dateValue As Date
                If DateTime.TryParse(SearchString, dateValue) Then
                    sql += " AND " + SpecifiedColumn + " = '" + SearchString.ToString() + "'"
                End If
                'ElseIf SpecifiedColumn = "TimestampRange" Then
                '    SpecifiedColumn = "Timestamp"
                '    If SearchString.Contains("~") Then

                '        Dim range As String() = SearchString.Split("~")
                '        sql += " AND (" + SpecifiedColumn + " >= '" + range(0).Trim() + "' and " + SpecifiedColumn + " <= '" + range(0).Trim() + "') "

                '    End If

            End If

        End If


        If (Status <> "") Then
            sql += " AND m.Status = '" + Status + "'"
        End If


        Dim StartRow As Integer = 0
        Dim EndRow As Integer = 0

        Dim totRecords As Integer = Pages * 30
        StartRow = totRecords + 1
        EndRow = totRecords + 31
        'ViewBag.TotalErrors = totRecords

        If number <> "" Then
            sql += " ORDER BY m.Updated DESC"
        Else
            sql += " ) AS RowConstrainedResult WHERE RowNum >= " & StartRow & " AND RowNum < " & EndRow & " ORDER BY RowNum"
        End If



        Dim errors = db.Database.SqlQuery(Of ErrorModel)(sql).ToList()

        Return errors

    End Function
    Public Shared Function showallErrors(ByVal number As String, ByVal Status As String, ByVal CID As Integer, ByVal IID As Integer, ByVal AID As Integer, ByVal SearchString As String, ByVal columnId As String) As List(Of SCSWeb.ErrorModel)
        Dim db As SCSDB = New SCSDB()
        Dim sql As String

        If number <> "" Then
            sql = "SET DATEFORMAT DMY; SELECT TOP " + number.ToString() + "  m.ID, m.MessageID, a.Name AS ApplicationName, c.Name AS CompanyName, i.Name AS InstallationName, u.Name AS StatusChangedByUserName  ,m.MessageDetails, m.MessageHeader, m.MessageID, m.ID, m.Updated, m.[Status], m.Timestamp FROM [Message] m LEFT JOIN [Installations] i ON i.ID = m.InstallationID LEFT JOIN [Applications] a ON a.ID = m.ApplicationID LEFT JOIN [Companies] c ON c.ID = m.CompanyID  LEFT JOIN [Users] u ON u.ID = m.StatusChangedByUserID "

        Else
            sql = "SET DATEFORMAT DMY; SELECT m.ID, m.MessageID, a.Name AS ApplicationName, c.Name AS CompanyName, i.Name AS InstallationName, u.Name AS StatusChangedByUserName  ,m.MessageDetails, m.MessageHeader, m.MessageID, m.ID, m.Updated, m.[Status], m.Timestamp FROM [Message] m LEFT JOIN [Installations] i ON i.ID = m.InstallationID LEFT JOIN [Applications] a ON a.ID = m.ApplicationID LEFT JOIN [Companies] c ON c.ID = m.CompanyID  LEFT JOIN [Users] u ON u.ID = m.StatusChangedByUserID "
        End If

        sql += " WHERE 1=1"

        If (columnId = "") Then
            If (CID > 0) Then
                sql += " AND m.CompanyID = " + CID.ToString()
            End If

            If (IID > 0) Then
                sql += " AND m.InstallationID = " + IID.ToString()
            End If

            If (AID > 0) Then
                sql += " AND m.ApplicationID = " + AID.ToString()
            End If

            If (SearchString <> "") Then
                sql += " AND (m.MessageHeader LIKE '%" + SearchString + "%' OR m.MessageDetails LIKE '%" + SearchString + "%')"
            End If

        Else
            If columnId.Contains("Timestamp") = False Then
                sql += " AND " + columnId + " like '%" + SearchString.ToString() + "%'"
            ElseIf columnId = "Timestamp" Then
                Dim dateValue As Date
                If DateTime.TryParse(SearchString, dateValue) Then
                    sql += " AND " + columnId + " = '" + SearchString.ToString() + "'"
                End If
            End If

        End If


        If (Status <> "") Then
            sql += " AND m.Status = '" + Status + "'"
        End If

        sql += " ORDER BY m.Updated DESC"

        Dim errors = db.Database.SqlQuery(Of ErrorModel)(sql).ToList()

        Return errors

    End Function

    ''' <summary>
    ''' Gets errors based ons status and company
    ''' </summary>
    Public Shared Function GetErrors(ByVal Status As String, ByVal CompanyID As Integer) As List(Of SCSWeb.ErrorModel)
        Return GetErrors(Status, CompanyID, -1, -1, "")
    End Function

    ''' <summary>
    ''' Gets errors based on status
    ''' </summary>
    Public Shared Function GetErrors(ByVal Status As String) As List(Of SCSWeb.ErrorModel)
        Return GetErrors(Status, -1, -1, -1, "")
    End Function

    ''' <summary>
    ''' Gets all errors registered
    ''' </summary>
    Public Shared Function GetErrors() As List(Of SCSWeb.ErrorModel)
        Return GetErrors("", -1, -1, -1, "")
    End Function

    ''' <summary>
    ''' Gets new errors within the last 24hrs
    ''' </summary>
    Public Shared Function GetErrors(ByVal compareDate As DateTime) As List(Of SCSWeb.ErrorModel)
        compareDate = DateAdd(DateInterval.Day, -1, compareDate)
        Return GetErrors("New", -1, -1, -1, "", compareDate)
    End Function

    ''' <summary>
    ''' Gets errors changed to Done within the last 24hrs
    ''' </summary>
    Public Shared Function GetErrors(ByVal status As String, ByVal updateDate As DateTime) As List(Of SCSWeb.ErrorModel)
        updateDate = DateAdd(DateInterval.Day, -1, updateDate)
        Return GetErrors(status, -1, -1, -1, "", Nothing, updateDate)
    End Function


    Public Shared Function GetTop10Errors() As List(Of SCSWeb.TopErrorsModel)
        Dim db As SCSDB = New SCSDB()

        Dim sql As String = "select top 10 count(ID) as Number, MessageDetails from Message where Status='New' and MessageDetails <> ''"
        sql += " group by MessageDetails order by Number desc"

        Dim errors = db.Database.SqlQuery(Of TopErrorsModel)(sql).ToList()

        Return errors

    End Function

    Public Shared Function GetErrorCount(ByVal Status As String, ByVal CID As Integer, ByVal IID As Integer, ByVal AID As Integer, ByVal SearchString As String, Optional ByVal compareDate As DateTime = Nothing, Optional ByVal updateDate As DateTime = Nothing, Optional ByVal SpecifiedColumn As String = "") As Integer

        Dim errorcount As Integer = 0
        Dim db As SCSDB = New SCSDB()

        Dim sql As String = ""

        sql = "SELECT Count(m.ID) FROM [Message] m LEFT JOIN [Installations] i ON i.ID = m.InstallationID LEFT JOIN [Applications] a ON a.ID = m.ApplicationID LEFT JOIN [Companies] c ON c.ID = m.CompanyID  LEFT JOIN [Users] u ON u.ID = m.StatusChangedByUserID "

        sql += " WHERE 1=1"

        If SpecifiedColumn = "" Then


            If (CID > 0) Then
                sql += " AND m.CompanyID = " + CID.ToString()
            End If

            If (IID > 0) Then
                sql += " AND m.InstallationID = " + IID.ToString()
            End If

            If (AID > 0) Then
                sql += " AND m.ApplicationID = " + AID.ToString()
            End If

            If (SearchString <> "") Then
                sql += " AND (m.MessageHeader LIKE '%" + SearchString + "%' OR m.MessageDetails LIKE '%" + SearchString + "%')"
            End If

            If (Status <> "") Then
                sql += " AND m.Status = '" + Status + "'"
            End If

        Else

            If SpecifiedColumn.Contains("Timestamp") = False Then
                sql += " AND " + SpecifiedColumn + " like '%" + SearchString.ToString() + "%'"
            ElseIf SpecifiedColumn = "Timestamp" Then
                Dim dateValue As Date
                If DateTime.TryParse(SearchString, dateValue) Then
                    sql += " AND " + SpecifiedColumn + " = '" + SearchString.ToString() + "'"
                End If
                'ElseIf SpecifiedColumn = "TimestampRange" Then
                '    SpecifiedColumn = "Timestamp"
                '    If SearchString.Contains("~") Then

                '        Dim range As String() = SearchString.Split("~")
                '        sql += " AND (" + SpecifiedColumn + " >= '" + range(0).Trim() + "' and " + SpecifiedColumn + " <= '" + range(0).Trim() + "') "

                '    End If

            End If

        End If

        If (Status <> "") Then
            sql += " AND m.Status = '" + Status + "'"
        End If

        errorcount = db.Database.SqlQuery(Of Integer)(sql).Single()

        Return errorcount

    End Function
End Class
