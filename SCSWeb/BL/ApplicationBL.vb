﻿Public Class ApplicationBL

    Public Shared Function GetApplications(ByVal CID As Integer) As List(Of Application)
        Dim db As New SCSDB
        Dim items As List(Of Application)

        If (CID > 0) Then
            Dim sql As String = "SELECT DISTINCT a.* FROM Installations i INNER JOIN Applications a ON a.ID = i.ApplicationID WHERE i.CompanyID = " + CID.ToString()
            items = db.Database.SqlQuery(Of Application)(sql).ToList()
        Else
            items = (From a In db.Applications
                         Select a).ToList()
        End If

        Return items
    End Function

    Public Shared Function GetApplications() As List(Of Application)
        Return GetApplications(-1)
    End Function




End Class
