﻿Public Class TableComparesBL

    ''get the table difference of each vessel for the selected tablename
    Public Shared Function GetNumberOfUnequalTableRows(ByVal ID As String, ByVal CompanyID As Integer, ByVal vmid As Integer, Optional ByVal TblVmName As String = "") As Integer
        Dim db As New SCSDB
        Dim num As Integer = 0

        If db.TableCompares.Count = 0 Then
            Return num
        End If

        Try
            'num = (From m In db.TableCompares
            '               Where m.TableRowsFM <> m.TableRowsVM And m.TableNameVM = TblVmName And m.CompanyID = CompanyID And m.VmID = vmid
            '               Select Math.Abs((m.TableRowsFM - m.TableRowsVM))).Sum
            num = (From m In db.TableCompares
                           Where m.TableNameVM = TblVmName And m.CompanyID = CompanyID And m.VmID = vmid
                           Select Math.Abs((m.TableRowsFM - m.TableRowsVM))).Sum
        Catch ex As Exception
            num = 0
        End Try

        Return num
    End Function
  
    ''get the equal rows of each vessel for the selected tablename
    Public Shared Function GetNumberOfEqualRowsTable(ByVal id As String, ByVal CompanyID As Integer, ByVal vmid As Integer, Optional ByVal TblVmName As String = "") As Integer
        Dim db As New SCSDB
        Dim num As Integer = 0

        If db.TableCompares.Count = 0 Then
            Return num
        End If
        Try

            num = (From m In db.TableCompares
                           Where m.CompanyID = CompanyID And m.TableNameVM = TblVmName And m.VmID = vmid
                           Select m.TableRowsVM - Math.Abs((m.TableRowsFM - m.TableRowsVM))).Sum

        Catch ex As Exception
            num = 0
        End Try
        Return num
    End Function
    ''get the equal table of each vessel for the selected tablename
    Public Shared Function GetNumberOfUnequalTableSelected(ByVal tid As String, ByVal CompanyID As Integer, ByVal vmid As Integer, Optional ByVal TblVmName As String = "") As Integer
        Dim db As New SCSDB
        Dim num As Integer = 0

        If db.TableCompares.Count = 0 Then
            Return num
        End If

        Try
            num = (From m In db.TableCompares
                        Where m.TableRowsFM <> m.TableRowsVM And m.CompanyID = CompanyID And m.TableNameVM = TblVmName And m.VmID = vmid
                        Select m.ID).Count
        Catch ex As Exception
            num = 0
        End Try

        Return num
    End Function
    ''get the number of equal table of each vessel for the selected tablename
    Public Shared Function GetNumberOfEqualTablesSelected(ByVal id_ As String, ByVal CompanyID As Integer, ByVal VmID As Integer, Optional ByVal TblVmName As String = "") As Integer
        Dim db As New SCSDB
        Dim num As Integer = 0

        If db.TableCompares.Count = 0 Then
            Return num
        End If


        Try
            num = (From m In db.TableCompares
                          Where m.TableRowsFM = m.TableRowsVM And m.CompanyID = CompanyID And m.VmID = VmID And m.TableNameVM = TblVmName
                          Select m.ID).Count
        Catch ex As Exception
            num = 0
        End Try

        Return num
    End Function
    ''get the number of rows in fm for the selected tablename
    Public Shared Function GetNumberOfTableRowsFM(ByVal id_ As String, ByVal CompanyID As Integer, ByVal VmID As Integer, Optional ByVal TblVmName As String = "") As Integer
        Dim db As New SCSDB
        Dim num As Integer = 0
        Try
            num = (From m In db.TableCompares
                           Where m.CompanyID = CompanyID And m.TableNameVM = TblVmName And m.VmID = VmID
                           Select m.TableRowsFM).Sum
        Catch ex As Exception
            num = 0
        End Try
        Return num
    End Function
    ''get the number of rows in vm for the selected tablename
    Public Shared Function GetNumberOfTableRowsVM(ByVal id_ As String, ByVal CompanyID As Integer, ByVal VmID As Integer, Optional ByVal TblVmName As String = "") As Integer
        Dim db As New SCSDB
        Dim num As Integer = 0
        Try

            num = (From m In db.TableCompares
                           Where m.CompanyID = CompanyID And m.TableNameVM = TblVmName And m.VmID = VmID
                           Select m.TableRowsVM).Sum
        Catch ex As Exception
            num = 0
        End Try
        Return num
    End Function
    ''' <summary>
    ''' Gets number of tables with difference for the selected installation and company
    ''' </summary>
    Public Shared Function GetNumberOfUnequalTables(ByVal CompanyID As Integer, ByVal VmID As Integer, Optional ByVal compareDate As DateTime = Nothing) As Integer
        Dim db As New SCSDB
        Dim num As Integer = 0

        If db.TableCompares.Count = 0 Then
            Return num
        End If

        If (CompanyID > 0 And VmID > 0) Then
            num = (From m In db.TableCompares
                           Where m.TableRowsFM <> m.TableRowsVM And m.CompanyID = CompanyID And m.VmID = VmID
                           Select m.ID).Count
        ElseIf (CompanyID > 0 And VmID < 0) Then
            num = (From m In db.TableCompares
                          Where m.TableRowsFM <> m.TableRowsVM And m.CompanyID = CompanyID
                          Select m.ID).Count
        ElseIf (CompanyID < 0 And VmID > 0) Then
            num = (From m In db.TableCompares
                           Where m.TableRowsFM <> m.TableRowsVM And m.VmID = VmID
                           Select m.ID).Count
        ElseIf (CompanyID < 0 And VmID < 0 And compareDate <> DateTime.MinValue) Then

            num = (From m In db.TableCompares
                       Where m.TableRowsFM <> m.TableRowsVM And m.Created >= compareDate
                       Select m.ID).Count


        Else
            num = (From m In db.TableCompares
                         Where m.TableRowsFM <> m.TableRowsVM
                         Select m.ID).Count
        End If

        Return num
    End Function
    ''' <summary>
    ''' Gets number of tables with difference for the selected installation
    ''' </summary>
    Public Shared Function GetNumberOfUnequalTables(ByVal CompanyID As Integer) As Integer
        Return GetNumberOfUnequalTables(CompanyID, -1)
    End Function

    ''' <summary>
    ''' Gets number of tables with difference for all installations
    ''' </summary>
    Public Shared Function GetNumberOfUnequalTables() As Integer
        Return GetNumberOfUnequalTables(-1, -1)
    End Function

    ''' <summary>
    ''' Gets number of tables with difference within the last 24hrs
    ''' </summary>
    Public Shared Function GetNumberOfUnequalTables(ByVal comparedate As DateTime) As Integer
        comparedate = DateAdd(DateInterval.Day, -1, comparedate)
        Return GetNumberOfUnequalTables(-1, -1, comparedate)
    End Function

    ''' <summary>
    ''' Gets number of rows with difference for the selected installation and company
    ''' </summary>
    Public Shared Function GetNumberOfUnequalRows(ByVal CompanyID As Integer, ByVal VmID As Integer, Optional ByVal compareDate As DateTime = Nothing) As Integer
        Dim db As New SCSDB
        Dim num As Integer = 0

        If db.TableCompares.Count = 0 Then
            Return num
        End If

        Try
            If (CompanyID > 0 And VmID > 0) Then
                num = (From m In db.TableCompares
                               Where m.TableRowsFM <> m.TableRowsVM And m.CompanyID = CompanyID And m.VmID = VmID
                               Select Math.Abs((m.TableRowsFM - m.TableRowsVM))).Sum
            ElseIf (CompanyID > 0 And VmID < 0) Then
                num = (From m In db.TableCompares
                              Where m.TableRowsFM <> m.TableRowsVM And m.CompanyID = CompanyID
                              Select Math.Abs((m.TableRowsFM - m.TableRowsVM))).Sum()
            ElseIf (CompanyID < 0 And VmID > 0) Then
                num = (From m In db.TableCompares
                              Where m.TableRowsFM <> m.TableRowsVM And m.VmID = VmID
                              Select Math.Abs((m.TableRowsFM - m.TableRowsVM))).Sum
            ElseIf (CompanyID < 0 And VmID < 0 And compareDate <> DateTime.MinValue) Then
                num = (From m In db.TableCompares
                              Where m.TableRowsFM <> m.TableRowsVM And m.Created >= compareDate
                              Select Math.Abs((m.TableRowsFM - m.TableRowsVM))).Sum
            Else
                num = (From m In db.TableCompares
                             Where m.TableRowsFM <> m.TableRowsVM
                              Select Math.Abs((m.TableRowsFM - m.TableRowsVM))).Sum
            End If
        Catch ex As Exception
            num = 0
        End Try

        Return num
    End Function
    ''' <summary>
    ''' Gets number of rows with difference for the selected installation
    ''' </summary>  
    Public Shared Function GetNumberOfUnequalRows(ByVal CompanyID As Integer) As Integer
        Return GetNumberOfUnequalRows(CompanyID, -1)
    End Function

    ''' <summary>
    ''' Gets number of rows with difference for all installations
    ''' </summary>
    Public Shared Function GetNumberOfUnequalRows() As Integer
        Return GetNumberOfUnequalRows(-1, -1)
    End Function

    ''' <summary>
    ''' Gets number of rows with difference within the last 24hrs
    ''' </summary>
    Public Shared Function GetNumberOfUnequalRows(ByVal compareDate As DateTime) As Integer
        compareDate = DateAdd(DateInterval.Day, -1, compareDate)
        Return GetNumberOfUnequalRows(-1, -1, compareDate)
    End Function


    ''' <summary>
    ''' Gets number equal tables for the selected installation and company
    ''' </summary>
    Public Shared Function GetNumberOfEqualTables(ByVal CompanyID As Integer, ByVal VmID As Integer) As Integer
        Dim db As New SCSDB
        Dim num As Integer = 0

        If db.TableCompares.Count = 0 Then
            Return num
        End If

        If (CompanyID > 0 And VmID > 0) Then
            num = (From m In db.TableCompares
                           Where m.TableRowsFM = m.TableRowsVM And m.CompanyID = CompanyID And m.VmID = VmID
                           Select m.ID).Count
        ElseIf (CompanyID > 0 And VmID < 0) Then
            num = (From m In db.TableCompares
                          Where m.TableRowsFM = m.TableRowsVM And m.CompanyID = CompanyID
                          Select m.ID).Count
        ElseIf (CompanyID < 0 And VmID > 0) Then
            num = (From m In db.TableCompares
                                     Where m.TableRowsFM = m.TableRowsVM And m.VmID = VmID
                                     Select m.ID).Count
        Else
            num = (From m In db.TableCompares
                         Where m.TableRowsFM <> m.TableRowsVM
                         Select m.ID).Count
        End If

        Return num
    End Function
    ''' <summary>
    ''' Gets number of equal tables for the selected installation
    ''' </summary>
    Public Shared Function GetNumberOfEqualTables(ByVal CompanyID As Integer) As Integer
        Return GetNumberOfEqualTables(CompanyID, -1)
    End Function

    ''' <summary>
    ''' Gets number of equal tables for all installations
    ''' </summary>
    Public Shared Function GetNumberOfEqualTables() As Integer
        Return GetNumberOfEqualTables(-1, -1)
    End Function

    ''' <summary>
    ''' Gets number equal rows for the selected installation and company
    ''' </summary>
    Public Shared Function GetNumberOfEqualRows(ByVal CompanyID As Integer, ByVal VmID As Integer) As Integer
        Dim db As New SCSDB
        Dim num As Integer = 0

        If db.TableCompares.Count = 0 Then
            Return num
        End If
        Try


            If (CompanyID > 0 And VmID > 0) Then
                num = (From m In db.TableCompares
                               Where m.CompanyID = CompanyID And m.VmID = VmID
                               Select m.TableRowsVM - Math.Abs((m.TableRowsFM - m.TableRowsVM))).Sum
            ElseIf (CompanyID > 0 And VmID < 0) Then
                num = (From m In db.TableCompares
                              Where m.CompanyID = CompanyID
                              Select m.TableRowsVM - Math.Abs((m.TableRowsFM - m.TableRowsVM))).Sum
            ElseIf (CompanyID < 0 And VmID > 0) Then
                num = (From m In db.TableCompares
                                Where m.VmID = VmID
                                Select m.TableRowsVM - Math.Abs((m.TableRowsFM - m.TableRowsVM))).Sum
            Else
                num = (From m In db.TableCompares
                             Select m.TableRowsVM - Math.Abs((m.TableRowsFM - m.TableRowsVM))).Sum
            End If
        Catch ex As Exception
            num = 0
        End Try
        Return num
    End Function
    ''' <summary>
    ''' Gets number of equal rows for the selected installation
    ''' </summary>
    Public Shared Function GetNumberOfEqualRows(ByVal CompanyID As Integer) As Integer
        Return GetNumberOfEqualRows(CompanyID, -1)
    End Function

    ''' <summary>
    ''' Gets number of equal rows for all installations
    ''' </summary>
    Public Shared Function GetNumberOfEqualRows() As Integer
        Return GetNumberOfEqualRows(-1, -1)
    End Function


    Public Shared Function GetNumberOfRowsFM(ByVal CompanyID As Integer, ByVal VmID As Integer) As Integer
        Dim db As New SCSDB
        Dim num As Integer = 0
        Try

            If (CompanyID > 0 And VmID > 0) Then
                num = (From m In db.TableCompares
                               Where m.CompanyID = CompanyID And m.VmID = VmID
                               Select m.TableRowsFM).Sum
            ElseIf (CompanyID > 0 And VmID < 0) Then
                num = (From m In db.TableCompares
                              Where m.CompanyID = CompanyID
                              Select m.TableRowsFM).Sum
            ElseIf (CompanyID < 0 And VmID > 0) Then
                num = (From m In db.TableCompares
                                Where m.VmID = VmID
                                Select m.TableRowsFM).Sum
            Else
                num = (From m In db.TableCompares
                             Select m.TableRowsFM).Sum
            End If
        Catch ex As Exception
            num = 0
        End Try
        Return num
    End Function

    Public Shared Function GetNumberOfRowsFM(ByVal CompanyID As Integer) As Integer
        Return GetNumberOfRowsFM(CompanyID, -1)
    End Function

    Public Shared Function GetNumberOfRowsFM() As Integer
        Return GetNumberOfRowsFM(-1, -1)
    End Function

    Public Shared Function GetLastUpdate(id_ As String, ByVal cid As Integer, vmid As Integer, Optional ByVal TblVmName As String = "") As DateTime
        Dim db As New SCSDB

        Try
            'Dim update = (From u In db.TableCompares Where u.CompanyID = cid And u.ID = id_ And u.VmID = vmid).Max.Updated
            Dim update = (From u In db.TableCompares Where u.CompanyID = cid And u.VmID = vmid And u.TableNameVM = TblVmName).Single.Updated
            Return update
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function GetNumberOfRowsVM(ByVal CompanyID As Integer, ByVal VmID As Integer) As Integer
        Dim db As New SCSDB
        Dim num As Integer = 0
        Try

            If (CompanyID > 0 And VmID > 0) Then
                num = (From m In db.TableCompares
                               Where m.CompanyID = CompanyID And m.VmID = VmID
                               Select m.TableRowsVM).Sum
            ElseIf (CompanyID > 0 And VmID < 0) Then
                num = (From m In db.TableCompares
                              Where m.CompanyID = CompanyID
                              Select m.TableRowsVM).Sum
            ElseIf (CompanyID < 0 And VmID > 0) Then
                num = (From m In db.TableCompares
                                Where m.VmID = VmID
                                Select m.TableRowsVM).Sum
            Else
                num = (From m In db.TableCompares
                             Select m.TableRowsVM).Sum
            End If
        Catch ex As Exception
            num = 0
        End Try
        Return num
    End Function

    Public Shared Function GetNumberOfRowsVM(ByVal CompanyID As Integer) As Integer
        Return GetNumberOfRowsVM(CompanyID, -1)
    End Function

    Public Shared Function GetNumberOfRowsVM() As Integer
        Return GetNumberOfRowsVM(-1, -1)
    End Function


End Class
