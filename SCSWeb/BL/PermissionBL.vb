﻿Imports System.Web
Public Class PermissionBL
    Inherits AuthorizeAttribute
    Private _login As String

    Public Property LoginPage() As String
        Get
            Return _login
        End Get
        Set(ByVal value As String)
            _login = value
        End Set
    End Property
    Private _needFullAccess As Boolean

    Public Property NeedFullAccess() As Boolean
        Get
            Return _needFullAccess
        End Get
        Set(ByVal value As Boolean)
            _needFullAccess = value
        End Set
    End Property

    Private _errorMessage As String

    Public Property ErrorMessage() As String
        Get
            Return _errorMessage
        End Get
        Set(ByVal value As String)
            _errorMessage = value
        End Set
    End Property

    Public Overrides  Sub OnAuthorization(ByVal filterContext As AuthorizationContext)
        HttpContext.Current.Session("ErrorMessage") = ""
        Dim retUrl As String = ""

        Dim rawURL As String = filterContext.HttpContext.Request.RawUrl.Replace(LoginPage + "?ReturnUrl=", "")

        retUrl = LoginPage + "?ReturnUrl=" + rawURL
        If (IsNothing(HttpContext.Current.Session("UserID"))) Then
            filterContext.HttpContext.Response.Redirect(retUrl)
        Else

            'will need to redirect to the login page if ever for viewing only to login with FullAccess
            If (IsNothing(HttpContext.Current.Session("ViewingOnly"))) Then
                filterContext.HttpContext.Response.Redirect(retUrl)
            End If

            If (NeedFullAccess.ToString() = "True") Then
                If (HttpContext.Current.Session("ViewingOnly").ToString() = "True") Then
                    If (IsNothing(ErrorMessage)) Then
                        HttpContext.Current.Session("ErrorMessage") = "This can only be accessed by a Full Access User"
                    Else
                        HttpContext.Current.Session("ErrorMessage") = ErrorMessage
                    End If
                    filterContext.HttpContext.Response.Redirect(retUrl)

                End If
            End If

            End If
    End Sub
End Class
