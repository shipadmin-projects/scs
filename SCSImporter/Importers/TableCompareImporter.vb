﻿Imports System.Xml
Imports System.IO

Namespace Importers

    Public Class TableCompareImporter



        Public Shared Sub Import(ByVal company As String, ByVal installation As String, ByVal message As String)
            Dim FMInstallationID As Integer = -1
            Dim VMInstallationID As Integer = -1
            Dim CompanyID As Integer = -1
            Dim FMApplicationID As Integer = -1
            Dim VMApplicationID As Integer = -1

            VMApplicationID = DB.Application.Import("VM")
            FMApplicationID = DB.Application.Import("FM")
            CompanyID = DB.Company.Import(company)

            FMInstallationID = DB.Installation.Import(installation, CompanyID, FMApplicationID)

            Dim xtr As New XmlTextReader(New System.IO.StringReader(message))
            xtr.WhitespaceHandling = WhitespaceHandling.None
            xtr.Read()
            Dim vessel As String = ""

            While Not xtr.EOF

                If (xtr.Name = "vessel") Then
                    vessel = xtr.GetAttribute("id")
                    VMInstallationID = DB.Installation.Import(vessel, CompanyID, VMApplicationID)
                    xtr.Read()
                    While xtr.Name = "vesseltable"
                        DB.TableCompare.Import(VMInstallationID, FMInstallationID, CompanyID, xtr.GetAttribute("TableNameFM"), xtr.GetAttribute("TableNameVM"), xtr.GetAttribute("TableRowsFM"), xtr.GetAttribute("TableRowsVM"))
                        xtr.Read()
                    End While
                End If

                xtr.Read()
            End While
            xtr.Close()

        End Sub

    End Class

End Namespace