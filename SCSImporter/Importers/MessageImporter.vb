﻿Imports System.Xml
Imports System.IO
Imports System.Globalization

Namespace Importers


    Public Class MessageImporter

        Private Shared Function ReplaceHexadecimalSymbols(ByVal txt As String) As String
            Dim r As String = "[\x00-\x08\x0B\x0C\x0E-\x1F\x26]"
            Return Regex.Replace(txt, r, "", RegexOptions.Compiled)
        End Function

        Public Shared Sub Import(ByVal message As String)
            Dim InstallationID As Integer = -1
            Dim CompanyID As Integer = -1
            Dim ApplicationID As Integer = -1
            Dim MessageID As Guid = Guid.Empty
            Dim MessageHeader As String = ""
            Dim MessageDetails As String = ""
            Dim MessageTime As String = ""

            message = ReplaceHexadecimalSymbols(message)


            Dim xtr As New XmlTextReader(New System.IO.StringReader(message))
            xtr.WhitespaceHandling = WhitespaceHandling.None

            xtr.Read()
            Dim vessel As String = ""

            Dim provider As CultureInfo = CultureInfo.InvariantCulture

            While Not xtr.EOF

                If (xtr.Name = "Error") Then
                    MessageID = New Guid(xtr.GetAttribute("uniqueid"))
                    CompanyID = DB.Company.Import(xtr.GetAttribute("company"))
                    ApplicationID = DB.Application.Import(xtr.GetAttribute("application"))
                    InstallationID = DB.Installation.Import(xtr.GetAttribute("Installation"), CompanyID, ApplicationID)
                    MessageTime = xtr.GetAttribute("time")
                    xtr.Read()

                    If (xtr.Name = "Message") Then
                        '  Dim flag As Boolean = xtr.ReadToFollowing("Message")
                        '  If (flag) Then
                        MessageHeader = xtr.ReadElementContentAsString()
                        'End If
                    End If

                    If (xtr.Name = "Exception") Then
                        ' Dim flag As Boolean = xtr.ReadToFollowing("Exception")
                        ' If (flag) Then
                        MessageDetails = xtr.ReadElementContentAsString()
                    End If


                    Dim stamp As DateTime = New DateTime(1900, 1, 1)

                    Try
                        stamp = DateTime.ParseExact(MessageTime, "dd.MM.yyyy HH:mm:ss", provider)
                    Catch
                        Try
                            stamp = DateTime.ParseExact(MessageTime, "dd/MM/yyyy HH:mm:ss", provider)
                        Catch
                        End Try
                    End Try

                    If (ShouldImportToArchive(MessageDetails)) Then
                        DB.ArchivedMessage.Import(MessageID, ApplicationID, CompanyID, InstallationID, MessageHeader, MessageDetails, stamp)
                    Else
                        DB.Message.Import(MessageID, ApplicationID, CompanyID, InstallationID, MessageHeader, MessageDetails, stamp)
                    End If
                End If


                xtr.Read()
            End While
            xtr.Close()

        End Sub

        Private Shared Function ShouldImportToArchive(ByVal MessageDetails As String) As Boolean
            If (MessageDetails.Contains("System.Net.WebException: The operation has timed out") Or MessageDetails.Contains("System.Web.HttpException: Validation of viewstate MAC failed.") Or MessageDetails.Contains("System.Net.WebException: Unable to connect to the remote server")) Then
                Return True
            Else
                Return False
            End If
        End Function

    End Class
End Namespace


