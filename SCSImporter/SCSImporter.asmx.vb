﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class SCSImporter
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function TestService() As String
        Return "Hello World"
    End Function

    <WebMethod()> _
    Public Sub ImportTableCompareData(ByVal application As String, ByVal company As String, ByVal installation As String, ByVal message As String)
        Try
            message = System.Web.HttpUtility.HtmlDecode(message)
            Importers.TableCompareImporter.Import(company, installation, message)
        Catch ex As Exception
            Helpers.LogHelper.LogError(ex, application + " " + company + " " + installation + " " + message)
            Throw
        End Try
    End Sub

    <WebMethod()> _
    Public Sub ImportErrorMessage(ByVal message As String)
        Try
            message = System.Web.HttpUtility.HtmlDecode(message)
            Importers.MessageImporter.Import(message)
        Catch ex As Exception
            Helpers.LogHelper.LogError(ex, message)
            Throw
        End Try

    End Sub

End Class