﻿Imports System.Data
Imports System.Data.SqlClient

Namespace DB
    Public Class Installation

        ''' <summary>
        ''' Imports installation if name does not exist. 
        ''' </summary>
        ''' <param name="Name">ID of installation. Example is VM or FM</param>
        ''' <returns>ID</returns>
        Public Shared Function Import(ByVal Name As String, ByVal CompanyID As Integer, ByVal ApplicationID As Integer) As Integer
            Dim cmd As SqlCommand = DBHelper.GetSqlCommand()
            Dim id As Integer = -1
            Try
                id = GetID(Name, CompanyID, ApplicationID)

                If (id <= 0) Then
                    cmd.Parameters.AddWithValue("@Name", Name)
                    cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
                    cmd.Parameters.AddWithValue("@ApplicationID", ApplicationID)
                    cmd.CommandText = "INSERT INTO [Installations] (Name, CompanyID, ApplicationID, Updated, Created) VALUES (@Name, @CompanyID, @ApplicationID, getdate(), getdate()); SELECT @@IDENTITY;"
                    cmd.Connection.Open()
                    id = Integer.Parse(cmd.ExecuteScalar().ToString())
                Else
                    cmd.Parameters.AddWithValue("@ID", id)
                    cmd.CommandText = "UPDATE [Installations] SET Updated = getdate() WHERE ID = @ID"
                    cmd.Connection.Open()
                    cmd.ExecuteNonQuery()
                End If
            Finally
                If (cmd.Connection IsNot Nothing) Then cmd.Connection.Close()
                If (cmd.Connection IsNot Nothing) Then cmd.Connection.Dispose()
                If (cmd IsNot Nothing) Then cmd.Dispose()
            End Try
            Return id
        End Function

        Public Shared Function GetID(ByVal Name As String, ByVal CompanyID As Integer, ByVal ApplicationID As Integer) As Integer
            Dim returnValue As Object = Nothing
            Dim cmd As SqlCommand = DBHelper.GetSqlCommand()
            Dim id As Integer = -1
            Try
                cmd.Parameters.AddWithValue("@Name", Name)
                cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
                cmd.Parameters.AddWithValue("@ApplicationID", ApplicationID)
                cmd.CommandText = "SELECT ID FROM [Installations] WHERE Name = @Name AND CompanyID = @CompanyID And ApplicationID = @ApplicationID "
                cmd.Connection.Open()
                returnValue = cmd.ExecuteScalar()
                If (returnValue IsNot Nothing) Then
                    id = Integer.Parse(returnValue.ToString())
                End If
            Catch
                Throw
            Finally
                If (cmd.Connection IsNot Nothing) Then cmd.Connection.Close()
                If (cmd.Connection IsNot Nothing) Then cmd.Connection.Dispose()
                If (cmd IsNot Nothing) Then cmd.Dispose()
            End Try
            Return id
        End Function


    End Class
End Namespace

