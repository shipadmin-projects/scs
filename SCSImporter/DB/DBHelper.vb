﻿Imports System.Data
Imports System.Data.SqlClient

Namespace DB

    Public Class DBHelper

        Public Shared MINIMUM_SQL_DATE As DateTime = New DateTime(1900, 1, 2)


        Private Shared ReadOnly Property DBConnectionString() As String
            Get
                Return System.Configuration.ConfigurationManager.ConnectionStrings("SCSConnection").ConnectionString
            End Get
        End Property

        Private Sub New()
            MyBase.New()
        End Sub

        Public Shared Function GetConnection() As SqlConnection
            Return New SqlConnection(DBConnectionString)
        End Function

        Public Shared Function GetSqlCommand() As SqlCommand
            If (DBConnectionString = "") Then Throw New System.Exception("No connection string found, Give value to SCSConnection property")
            Return New SqlCommand("", GetConnection())
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="sql"></param>
        ''' <returns>-1 if no rows are found</returns>
        ''' <remarks></remarks>
        Public Shared Function GetIntegerFromQuery(ByVal sql As String) As Integer
            If (DBConnectionString = "") Then Throw New System.Exception("No connection string found, Give value to SCSConnection property")
            Dim con As SqlConnection = GetConnection()
            Dim returnValue As Integer
            Dim cmd As SqlCommand = New SqlCommand("", con)
            Dim returnObject As Object = Nothing
            returnValue = -1

            Try
                con.Open()
                returnObject = cmd.ExecuteScalar()
                If (returnObject IsNot Nothing) Then returnValue = Integer.Parse(returnObject.ToString())
            Catch ex As System.Exception
                Throw New Exception("GetIntegerFromQuery exception for SQL: " + cmd.CommandText, ex)
            Finally
                con.Close()
                con.Dispose()
            End Try
            GetIntegerFromQuery = returnValue
        End Function

        Public Shared Function GetStringFromQuery(ByVal sql As String) As String
            If (DBConnectionString = "") Then Throw New System.Exception("No connection string found, Give value to SCSConnection property")
            Dim con As SqlConnection = GetConnection()
            Dim returnValue As String = ""
            Dim cmd As SqlCommand = New SqlCommand("", con)
            Dim returnObject As Object = Nothing

            Try
                con.Open()
                cmd.CommandText = sql
                returnObject = cmd.ExecuteScalar()
                If (returnObject IsNot Nothing) Then returnValue = returnObject.ToString()
            Catch ex As System.Exception
                Throw New Exception("GetStringFromQuery exception for SQL: " + cmd.CommandText, ex)
            Finally
                con.Close()
                con.Dispose()
            End Try
            Return returnValue
        End Function

        Public Shared Sub FillSqlDataTable(ByRef tb As DataTable, ByVal sql As String)
            If (DBConnectionString = "") Then Throw New System.Exception("No connection string found, Give value to SCSConnection property")
            Dim adp As SqlDataAdapter = Nothing
            Try
                If (tb Is Nothing) Then
                    tb = New DataTable()
                End If
                adp = New SqlDataAdapter(sql, DBConnectionString)
                adp.Fill(tb)
                adp.SelectCommand.Connection.Close()
            Catch ex As System.Exception
                Throw New Exception("FillSqlDataTable exception for SQL: " + sql, ex)
            Finally
                If (adp IsNot Nothing) Then
                    If (adp.SelectCommand IsNot Nothing) Then
                        If (adp.SelectCommand.Connection IsNot Nothing) Then
                            adp.SelectCommand.Connection.Close()
                            adp.SelectCommand.Connection.Dispose()
                        End If
                        adp.SelectCommand.Dispose()
                        adp.Dispose()
                    End If
                End If
            End Try
        End Sub

    End Class

End Namespace
