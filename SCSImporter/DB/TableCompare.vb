﻿Imports System.Data
Imports System.Data.SqlClient

Namespace DB
    Public Class TableCompare

        ''' <summary>
        ''' Imports table compare, updates if exist
        ''' </summary>
        Public Shared Sub Import(ByVal VmID As Integer, ByVal FmID As Integer, ByVal CompanyID As Integer, ByVal TableNameFM As String, ByVal TableNameVM As String, ByVal TableRowsFM As Integer, ByVal TableRowsVM As Integer)
            Dim cmd As SqlCommand = DBHelper.GetSqlCommand()
            Dim ID As Integer = -1
            Try
                ID = GetID(VmID, FmID, TableNameFM, TableNameVM, CompanyID)
                cmd.Parameters.AddWithValue("@ID", ID)
                cmd.Parameters.AddWithValue("@VmID", VmID)
                cmd.Parameters.AddWithValue("@FmID", FmID)
                cmd.Parameters.AddWithValue("@TableNameFM", TableNameFM)
                cmd.Parameters.AddWithValue("@TableNameVM", TableNameVM)
                cmd.Parameters.AddWithValue("@TableRowsVM", TableRowsVM)
                cmd.Parameters.AddWithValue("@TableRowsFM", TableRowsFM)
                cmd.Parameters.AddWithValue("@CompanyID", CompanyID)


                If (ID <= 0) Then
                    cmd.CommandText = "INSERT INTO [TableCompares] (VmID, FmID, CompanyID, TableNameFM, TableNameVM, TableRowsVM, TableRowsFM, Updated, Created) VALUES (@VmID, @FmID, @CompanyID, @TableNameFM, @TableNameVM, @TableRowsVM, @TableRowsFM, getdate(), getdate()); SELECT @@IDENTITY;"
                    cmd.Connection.Open()
                    ID = Integer.Parse(cmd.ExecuteScalar().ToString())
                Else
                    cmd.CommandText = "UPDATE [TableCompares] SET TableRowsVM = @TableRowsVM, TableRowsFM = @TableRowsFM, Updated = getdate() WHERE ID = @ID"
                    cmd.Connection.Open()
                    cmd.ExecuteNonQuery()
                End If
            Catch
                Throw
            Finally
                If (cmd.Connection IsNot Nothing) Then cmd.Connection.Close()
                If (cmd.Connection IsNot Nothing) Then cmd.Connection.Dispose()
                If (cmd IsNot Nothing) Then cmd.Dispose()
            End Try
        End Sub

        Public Shared Function GetID(ByVal VmID As Integer, ByVal FmID As Integer, ByVal TableNameFM As String, ByVal TableNameVM As String, ByVal CompanyID As Integer) As Integer
            Dim returnValue As Object = Nothing
            Dim id As Integer = -1
            Dim cmd As SqlCommand = DBHelper.GetSqlCommand()
            Try
                cmd.Parameters.AddWithValue("@VmID", VmID)
                cmd.Parameters.AddWithValue("@FmID", FmID)
                cmd.Parameters.AddWithValue("@TableNameFM", TableNameFM)
                cmd.Parameters.AddWithValue("@TableNameVM", TableNameVM)
                cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
                cmd.CommandText = "SELECT ID FROM [TableCompares] WHERE VmID = @VmID AND FmID = @FmID AND TableNameFM = @TableNameFM AND TableNameVM = @TableNameVM AND CompanyID = @CompanyID"
                cmd.Connection.Open()
                returnValue = cmd.ExecuteScalar()
                If (returnValue IsNot Nothing) Then
                    id = Integer.Parse(returnValue.ToString())
                End If
            Catch
                Throw
            Finally
                If (cmd.Connection IsNot Nothing) Then cmd.Connection.Close()
                If (cmd.Connection IsNot Nothing) Then cmd.Connection.Dispose()
                If (cmd IsNot Nothing) Then cmd.Dispose()
            End Try
            Return id
        End Function


    End Class
End Namespace

