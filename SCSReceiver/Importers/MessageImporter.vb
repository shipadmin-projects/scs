﻿Imports System.Xml
Imports System.IO
Imports System.Globalization

Namespace Importers

    Public Class MessageImporter

        Public Shared Sub Import(ByVal message As String)
           Dim InstallationID As Integer = -1
            Dim CompanyID As Integer = -1
            Dim ApplicationID As Integer = -1
            Dim MessageID As Guid = Guid.Empty
            Dim MessageHeader As String = ""
            Dim MessageDetails As String = ""
            Dim MessageTime As String = ""

            Dim xtr As New XmlTextReader(New System.IO.StringReader(message))
            xtr.WhitespaceHandling = WhitespaceHandling.None
            xtr.Read()
            Dim vessel As String = ""

            Dim provider As CultureInfo = CultureInfo.InvariantCulture

            While Not xtr.EOF

                If (xtr.Name = "Error") Then
                    MessageID = New Guid(xtr.GetAttribute("uniqueid"))
                    InstallationID = DB.Installation.Import(xtr.GetAttribute("Installation"))
                    CompanyID = DB.Company.Import(xtr.GetAttribute("company"))
                    ApplicationID = DB.Application.Import(xtr.GetAttribute("application"))
                    MessageTime = DB.Application.Import(xtr.GetAttribute("time"))
                    xtr.Read()

                    If (xtr.Name = "Message") Then
                            MessageHeader = xtr.ReadElementContentAsString()
                    End If

                    If (xtr.Name = "Exception") Then
                        MessageDetails = xtr.ReadElementContentAsString()
                    End If

                    If (ShouldImportToArchive(MessageDetails)) Then
                        DB.Message.Import(MessageID, ApplicationID, CompanyID, InstallationID, MessageHeader, MessageDetails, DateTime.ParseExact(MessageTime, "dd.MM.yyyy HH:mm:ss", provider))
                    Else
                        DB.Message.Import(MessageID, ApplicationID, CompanyID, InstallationID, MessageHeader, MessageDetails, DateTime.ParseExact(MessageTime, "dd.MM.yyyy HH:mm:ss", provider))
                    End If

                End If

                xtr.Read()
            End While
            xtr.Close()

        End Sub

        Private Shared Function ShouldImportToArchive(ByVal MessageDetails As String) As Boolean
            If (MessageDetails.Contains("System.Net.WebException: The operation has timed out") Or MessageDetails.Contains("System.Web.HttpException: Validation of viewstate MAC failed.") Or MessageDetails.Contains("System.Net.WebException: Unable to connect to the remote server")) Then
                Return True
            Else
                Return False
            End If
        End Function

    End Class
End Namespace


