﻿Imports System.Xml
Imports System.IO

Namespace Importers

    Public Class TableCompareImporter



        Public Shared Sub Import(ByVal application As String, ByVal company As String, ByVal installation As String, ByVal message As String)
            Dim FmID As Integer = -1
            Dim VmID As Integer = -1

            DB.Application.Import("VM")
            DB.Application.Import(application)
            DB.Company.Import(company)
            FmID = DB.Installation.Import(installation)

            Dim xtr As New XmlTextReader(New System.IO.StringReader(message))
            xtr.WhitespaceHandling = WhitespaceHandling.None
            xtr.Read()
            Dim vessel As String = ""

            While Not xtr.EOF

                If (xtr.Name = "vessel") Then
                    vessel = xtr.GetAttribute("id")
                    VmID = DB.Installation.Import(vessel)
                    xtr.Read()
                    While xtr.Name = "vesseltable"
                        DB.TableCompare.Import(VmID, FmID, xtr.GetAttribute("TableNameFM"), xtr.GetAttribute("TableNameVM"), xtr.GetAttribute("TableRowsFM"), xtr.GetAttribute("TableRowsVM"))
                        xtr.Read()
                    End While
                End If

                xtr.Read()
            End While
            xtr.Close()

        End Sub

    End Class

End Namespace