﻿Imports System.Data
Imports System.Data.SqlClient

Namespace DB
    Public Class Company

        ''' <summary>
        ''' Imports application if name does not exist 
        ''' </summary>
        ''' <param name="Name">ID of Company. Example is VM or FM</param>
        ''' <returns>ID</returns>
        Public Shared Function Import(ByVal Name As String) As Integer
            Dim cmd As SqlCommand = DBHelper.GetSqlCommand()
            Dim id As Integer = -1
            Try
                id = GetID(Name)

                If (id <= 0) Then
                    cmd.Parameters.AddWithValue("@Name", Name)
                    cmd.CommandText = "INSERT INTO [Companies] (Name, Updated, Created) VALUES (@Name, getdate(), getdate()); SELECT @@IDENTITY;"
                    cmd.Connection.Open()
                    id = Integer.Parse(cmd.ExecuteScalar().ToString())
                Else
                    cmd.Parameters.AddWithValue("@ID", id)
                    cmd.CommandText = "UPDATE [Companies] SET Updated = getdate() WHERE ID = @ID"
                    cmd.Connection.Open()
                    cmd.ExecuteNonQuery()
                End If
            Finally
                If (cmd.Connection IsNot Nothing) Then cmd.Connection.Close()
                If (cmd.Connection IsNot Nothing) Then cmd.Connection.Dispose()
                If (cmd IsNot Nothing) Then cmd.Dispose()
            End Try
            Return id

        End Function

        Public Shared Function GetID(ByVal Name As String) As Integer
            Dim returnValue As Object = Nothing
            Dim cmd As SqlCommand = DBHelper.GetSqlCommand()
            Dim id As Integer = -1
            Try
                cmd.Parameters.AddWithValue("@Name", Name)
                cmd.CommandText = "SELECT ID FROM [Companies] WHERE NAME = @Name"
                cmd.Connection.Open()
                returnValue = cmd.ExecuteScalar()
                If (returnValue IsNot Nothing) Then
                    id = Integer.Parse(returnValue.ToString())
                End If
            Catch
                Throw
            Finally
                If (cmd.Connection IsNot Nothing) Then cmd.Connection.Close()
                If (cmd.Connection IsNot Nothing) Then cmd.Connection.Dispose()
                If (cmd IsNot Nothing) Then cmd.Dispose()
            End Try
            Return id
        End Function


    End Class
End Namespace

