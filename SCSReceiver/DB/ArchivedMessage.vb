﻿Imports System.Data.SqlClient
Namespace DB

    Public Class ArchivedMessage

        ''' <summary>
        ''' Imports message compare
        ''' </summary>
        Public Shared Sub Import(ByVal MessageID As Guid, ByVal ApplicationID As Integer, ByVal CompanyID As Integer, ByVal InstallationID As Integer, ByVal MessageHeader As String, ByVal MessageDetails As String, ByVal Timestamp As DateTime)
            Dim cmd As SqlCommand = DBHelper.GetSqlCommand()
            Dim id As Integer = -1
            Try

                If (id <= 0) Then
                    id = GetID(MessageID)
                    cmd.Parameters.AddWithValue("@MessageID", MessageID)
                    cmd.Parameters.AddWithValue("@ApplicationID", ApplicationID)
                    cmd.Parameters.AddWithValue("@CompanyID", CompanyID)
                    cmd.Parameters.AddWithValue("@InstallationID", InstallationID)
                    cmd.Parameters.AddWithValue("@MessageHeader", MessageHeader)
                    cmd.Parameters.AddWithValue("@MessageDetails", MessageDetails)
                    cmd.Parameters.AddWithValue("@Status", "New")
                    cmd.Parameters.AddWithValue("@Type", "Error")
                    cmd.Parameters.AddWithValue("@Timestamp", "Timestamp")
                    cmd.Parameters.AddWithValue("@StatusChangedByUserID", -1)
                    cmd.CommandText = "INSERT INTO [ArchivedMessage] (MessageID, ApplicationID, CompanyID, InstallationID, MessageHeader, MessageDetails, Status, Type, StatusChangedByUserID, Updated, Created, Timestamp) VALUES (@MessageID, @ApplicationID, @CompanyID, @InstallationID, @MessageHeader, @MessageDetails, @Status, @Type, @StatusChangedByUserID, getdate(), getdate(), Timestamp())"
                    cmd.Connection.Open()
                    cmd.ExecuteNonQuery()
                Else
                    cmd.CommandText = "UPDATE [ArchivedMessage] SET Updated = getdate() WHERE MessageID = @MessageID"
                    cmd.Connection.Open()
                    cmd.ExecuteNonQuery()
                End If
            Finally
                If (cmd.Connection IsNot Nothing) Then cmd.Connection.Close()
                If (cmd.Connection IsNot Nothing) Then cmd.Connection.Dispose()
                If (cmd IsNot Nothing) Then cmd.Dispose()
            End Try
        End Sub

        Public Shared Function GetID(ByVal MessageID As Guid) As Integer
            Dim returnValue As Object = Nothing
            Dim cmd As SqlCommand = DBHelper.GetSqlCommand()
            Try
                cmd.Parameters.AddWithValue("@MessageID", MessageID)
                cmd.CommandText = "SELECT ID FROM [ArchivedMessage] WHERE MessageID = @MessageID"
                cmd.Connection.Open()
                returnValue = cmd.ExecuteScalar()
                If (returnValue Is Nothing) Then
                    Return -1
                Else
                    Return Integer.Parse(returnValue.ToString())
                End If
            Finally
                If (cmd.Connection IsNot Nothing) Then cmd.Connection.Close()
                If (cmd.Connection IsNot Nothing) Then cmd.Connection.Dispose()
                If (cmd IsNot Nothing) Then cmd.Dispose()
            End Try
        End Function
    End Class

End Namespace