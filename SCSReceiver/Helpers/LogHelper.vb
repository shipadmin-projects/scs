﻿Namespace Helpers

    Public Class LogHelper

        Private Sub New()
        End Sub

        Private Shared ReadOnly Property ApplicationFolder() As String
            Get
                Return System.Configuration.ConfigurationManager.AppSettings("ApplicationPath")
            End Get
        End Property

        Private Shared ErrorLogFileName As String = System.IO.Path.Combine(ApplicationFolder, "ErrorLog.txt")
        Private Shared InformationLogFileName As String = System.IO.Path.Combine(ApplicationFolder, "InformationLog.txt")

        Public Shared Sub LogError(ByVal ex As Exception, ByVal msg As String)
            System.IO.File.AppendAllText(ErrorLogFileName, DateTime.Now.ToString() + " " + msg + "      :      " + ex.ToString() + System.Environment.NewLine())
        End Sub

        Public Shared Sub LogError(ByVal ex As Exception)
            System.IO.File.AppendAllText(ErrorLogFileName, DateTime.Now.ToString() + " " + ex.ToString() + System.Environment.NewLine())
        End Sub

        Public Shared Sub LogInformation(ByVal msg As String)
            System.IO.File.AppendAllText(InformationLogFileName, DateTime.Now.ToString() + " " + msg + System.Environment.NewLine())
        End Sub

    End Class

End Namespace
