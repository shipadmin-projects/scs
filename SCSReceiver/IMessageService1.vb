﻿' NOTE: You can use the "Rename" command on the context menu to change the interface name "IService1" in both code and config file together.
<ServiceContract()>
Public Interface IMessageService1

    <OperationContract()>
    Function GetData(ByVal value As Integer) As String

    <OperationContract()>
    Function GetDataUsingDataContract(ByVal composite As CompositeType) As CompositeType

    ' TODO: Add your service operations here

    <OperationContract()>
    Function GetMessageDetails(ByVal Company As String) As List(Of MessageComposite)


    <OperationContract()>
    Sub ImportErrorMessage(ByVal message As String)

    <OperationContract()>
    Sub ImportTableCompareData(ByVal application As String, ByVal company As String, ByVal installation As String, ByVal message As String)

End Interface

' Use a data contract as illustrated in the sample below to add composite types to service operations.

<DataContract()>
Public Class MessageComposite

    <DataMember()>
    Public Property ID() As Integer
    <DataMember()>
    Public Property MessageID() As Guid
    <DataMember()>
    Public Property Application() As String
    <DataMember()>
    Public Property Company() As String

End Class
<DataContract()>
Public Class CompositeType

    <DataMember()>
    Public Property BoolValue() As Boolean

    <DataMember()>
    Public Property StringValue() As String

End Class
