﻿

' NOTE: You can use the "Rename" command on the context menu to change the class name "Service1" in code, svc and config file together.
' NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.vb at the Solution Explorer and start debugging.
Imports System.Data.Entity
Imports System.IO
Imports System.Xml.Serialization

Public Class Service1
    Implements IMessageService1
    'Public db As New SCSDB
    Public Sub New()
        Helpers.LogHelper.LogInformation("SCS Receiver started")
    End Sub

    Public Function GetData(ByVal value As Integer) As String Implements IMessageService1.GetData
        Return String.Format("You entered: {0}", value)
    End Function

    Public Function GetDataUsingDataContract(ByVal composite As CompositeType) As CompositeType Implements IMessageService1.GetDataUsingDataContract
        If composite Is Nothing Then
            Throw New ArgumentNullException("composite")
        End If
        If composite.BoolValue Then
            composite.StringValue &= "Suffix"
        End If
        Return composite
    End Function

    Public Function GetMessageDetails(ByVal Company As String) As List(Of MessageComposite) Implements IMessageService1.GetMessageDetails


        '  Dim Msg As List(Of SCSWeb.Message) = New List(Of SCSWeb.Message)

        Dim msgcomlst As List(Of MessageComposite) = New List(Of MessageComposite)
        'Using src1 As New SCSWeb.SCSDB
        '    Msg = src1.Messages.ToList()

        '    For Each m As SCSWeb.Message In Msg
        '        Dim msgcom As MessageComposite = New MessageComposite()
        '        msgcom.ID = m.ID
        '        msgcom.MessageID = m.MessageID
        '        'msgcom.Application = m.Application
        '        'msgcom.Company = m.Application
        '        msgcomlst.Add(msgcom)
        '    Next

        'End Using

        Return msgcomlst
    End Function

    ''' <summary>
    ''' Import data for table compare between FM and VM
    ''' </summary>
    ''' <param name="message">message is on the format in the remarks field</param>
    ''' <remarks>
    ''' <tablecompare installation="FMBeta" company="Shipadmin" application="FM">
    ''' <vessel id="M/V Ocean Thulitt">
    '''   <vesseltable TableNameFM="tbl_Vaccine" TableNameVM="Vaccine" TableRowsVM="3" TableRowsFM="44"/>
    '''   <vesseltable TableNameFM="tbl_PersonVaccine" TableNameVM="PersonVaccine" TableRowsVM="455" TableRowsFM="455"/>
    '''  </vessel>
    '''  <vessel id="Poseidon">
    '''    <vesseltable TableNameFM="tbl_Vaccine" TableNameVM="Vaccine" TableRowsVM="3" TableRowsFM="44"/>
    '''   </vessel>
    ''' </tablecompare>
    '''</remarks>
    Public Sub ImportTableCompareData(ByVal application As String, ByVal company As String, ByVal installation As String, ByVal message As String) Implements IMessageService1.ImportTableCompareData
        Try
            Importers.TableCompareImporter.Import(application, company, installation, message)
        Catch ex As Exception
            Helpers.LogHelper.LogError(ex, application + " " + company + " " + installation + " " + message)
        End Try
    End Sub



    ''' <summary>
    ''' Import Error Messages that will be sent by the Shipadmin Control System Sender
    ''' </summary>
    ''' <param name="message">XML Document File</param>
    ''' <remarks></remarks>
    Public Sub ImportErrorMessage(ByVal message As String) Implements IMessageService1.ImportErrorMessage
        Try
            Importers.MessageImporter.Import(message)
        Catch ex As Exception
            Helpers.LogHelper.LogError(ex, message)
            Throw
        End Try


        'Try
        '    Dim Msg As List(Of SCSWeb.Message) = New List(Of SCSWeb.Message)

        '    Dim msgcomlst As List(Of MessageComposite) = New List(Of MessageComposite)

        '    Dim lng_Teller As Long = 0
        '    Dim str_XML As String = ""
        '    Dim ts As Long = 0

        '    Dim objsample As Message = New Message()
        '     Dim xObj As New System.Xml.Serialization.XmlSerializer(objsample.GetType)

        '    objsample = xObj.Deserialize(New System.IO.StringReader(message))


        '    Return msgcomlst
        '    Return Nothing
        'Catch ex As Exception
        '    Helpers.LogHelper.LogError(ex, message)
        '    Throw
        'End Try


    End Sub

End Class
