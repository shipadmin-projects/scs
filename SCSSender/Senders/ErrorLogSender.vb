﻿Imports System.Xml

Namespace Senders

    Public Class ErrorLogSender
        Public Shared Sub ProcessErrorLogs(ByVal backupFolder As String, ByVal installation As Settings.SettingsErrorLog)
            For Each filename As String In System.IO.Directory.GetFiles(installation.path, "*.xml")
                ProcessErrorLog(backupFolder, installation, filename)
            Next
        End Sub


        Public Shared Sub ProcessErrorLog(ByVal backupFolder As String, ByVal s As Settings.SettingsErrorLog, ByVal filename As String)
            Dim importer As SCSImporter.SCSImporter
            Try
                importer = New SCSImporter.SCSImporter()
                importer.Url = System.Configuration.ConfigurationManager.AppSettings("SCSImporter")
                Dim xml As String = System.IO.File.ReadAllText(filename)
                SetHeadersIfBlank(xml, s, filename)
                importer.ImportErrorMessage(xml)
                BackupFile(filename, backupFolder, s, xml)
            Catch ex As Exception
                Throw New Exception("Error processing error log for the file " + filename, ex)
            End Try
        End Sub

        Public Shared Function GetApplicationFromFilename(ByVal filename As String) As String
            If (filename.Contains("VM")) Then
                Return "VM"
            ElseIf (filename.Contains("FM")) Then
                Return "FM"
            ElseIf (filename.Contains("Synch")) Then
                Return "Synch"
            Else
                Return "Unknown Application"
            End If
        End Function

        Public Shared Function GetInstallationFromFileName(ByVal filename As String, ByVal connectionstring As String) As String
            Dim imonumber = System.IO.Path.GetFileName(filename).Split("_").GetValue(0).ToString()
            Dim name As String = Helpers.DBHelper.GetStringFromQuery("SELECT VesselName FROM tbl_Vessel WHERE VesselID = '" + imonumber + "'", connectionstring)
            If (GetApplicationFromFilename(filename) = "Synch") Then
                name += " (Synch)"
            End If
            Return name
        End Function

        Public Shared Sub SetHeadersIfBlank(ByRef xml As String, ByVal s As Settings.SettingsErrorLog, ByVal filename As String)
            Dim doc = New XmlDocument()
            doc.LoadXml(xml)

            Dim root = doc.DocumentElement
            Dim nodes = root.SelectNodes("Error")

            Dim application = GetApplicationFromFilename(filename)
            Dim installation = s.installation
            If (installation = "Synch" Or application = "VM") Then
                installation = GetInstallationFromFileName(filename, s.connectionstring)
                If (installation = "") Then Throw New Exception("VM installation for filename " + filename + " is not found in FM and cannot be exported is blank and cannot be exporter.")

            End If

            Dim company As String = s.company


            For Each node As XmlElement In nodes
                If (node.Attributes("Installation") Is Nothing) Then
                    node.SetAttribute("Installation", installation)
                Else
                    If (node.Attributes("Installation").Value.ToLower() = "unknown") Then
                        node.SetAttribute("Installation", installation)
                    End If
                End If

                If (node.Attributes("company") Is Nothing) Then
                    node.SetAttribute("company", company)
                Else
                    If (node.Attributes("company").Value.ToLower() = "unknown") Then
                        node.SetAttribute("company", company)
                    End If
                End If

                If (node.Attributes("application") Is Nothing) Then
                    node.SetAttribute("application", application)
                Else
                    If (node.Attributes("application").Value.ToLower() = "unknown") Then
                        node.SetAttribute("application", application)
                    End If
                End If

                If (node.Attributes("uniqueid") Is Nothing) Then
                    node.SetAttribute("uniqueid", Guid.NewGuid().ToString())
                End If

                For Each stringNode As XmlElement In node.SelectNodes("Message")
                    stringNode.InnerXml = "<![CDATA[" + stringNode.InnerXml + "]]>"
                Next

                For Each stringNode As XmlElement In node.SelectNodes("Exception")
                    stringNode.InnerXml = "<![CDATA[" + stringNode.InnerXml + "]]>"
                Next
            Next
            xml = doc.OuterXml
        End Sub

        Public Shared Sub BackupFile(ByVal FileName As String, ByVal BackupFolderBase As String, ByVal s As Settings.SettingsErrorLog, ByVal xmlSent As String)

            Dim stamp As DateTime = DateTime.Now

            Dim BackupFolderCompanyInstallation As String = BackupFolderBase
            BackupFolderCompanyInstallation = System.IO.Path.Combine(BackupFolderCompanyInstallation, s.company)
            BackupFolderCompanyInstallation = System.IO.Path.Combine(BackupFolderCompanyInstallation, s.installation)
            BackupFolderCompanyInstallation = System.IO.Path.Combine(BackupFolderCompanyInstallation, DateTime.Now.Year.ToString())
            BackupFolderCompanyInstallation = System.IO.Path.Combine(BackupFolderCompanyInstallation, DateTime.Now.Month.ToString())
            BackupFolderCompanyInstallation = System.IO.Path.Combine(BackupFolderCompanyInstallation, DateTime.Now.Day.ToString())

            If (System.IO.Directory.Exists(BackupFolderCompanyInstallation) = False) Then
                System.IO.Directory.CreateDirectory(BackupFolderCompanyInstallation)
            End If

            Dim BackFolderCompanyInstallationFile As String = System.IO.Path.Combine(BackupFolderCompanyInstallation, System.IO.Path.GetFileName(FileName) + "_" + stamp.Hour.ToString() + "_" + stamp.Minute.ToString() + "_" + stamp.Second.ToString() + System.IO.Path.GetExtension(FileName))
            Dim BackFolderCompanyInstallationFileSent As String = System.IO.Path.Combine(BackupFolderCompanyInstallation, System.IO.Path.GetFileName(FileName) + "_" + stamp.Hour.ToString() + "_" + stamp.Minute.ToString() + "_" + stamp.Second.ToString() + "_sent" + System.IO.Path.GetExtension(FileName))

            ' Moving original file
            If (System.IO.File.Exists(BackFolderCompanyInstallationFile)) Then
                ' System.IO.File.Delete(BackFolderCompanyInstallationFile)
                BackFolderCompanyInstallationFile += "_" + Guid.NewGuid().ToString() + ".xml"
            End If
            System.IO.File.Move(FileName, BackFolderCompanyInstallationFile)

            ' Saving sent information
            If (System.IO.File.Exists(BackFolderCompanyInstallationFileSent)) Then
                BackFolderCompanyInstallationFileSent += "_" + Guid.NewGuid().ToString() + ".xml"
            End If
            System.IO.File.AppendAllText(BackFolderCompanyInstallationFileSent, xmlSent)

        End Sub

    End Class

End Namespace
