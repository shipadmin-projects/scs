﻿Namespace Senders

    Public Class TableCompareSender

        Public Property TableCompare As TableCompareSender

        Public Shared Sub SendTableCompareData(ByVal s As Settings.SettingsTableCompare)
            Dim importer As SCSImporter.SCSImporter

            Try
                importer = New SCSImporter.SCSImporter()
                importer.Url = System.Configuration.ConfigurationManager.AppSettings("SCSImporter")
                Dim xml As String = GetTableCompareXml(s)
                importer.ImportTableCompareData(s.application, s.company, s.installation, xml)
            Catch ex As Exception
                Throw New Exception("Error processing tablecompare data for " + s.connectionstring, ex)
            End Try
        End Sub

        Public Shared Function GetTableCompareData(ByVal tc As Settings.SettingsTableCompare) As DataTable
            Dim tb As DataTable = Nothing
            Dim sql As String = "SELECT tc.VesselID, v.VesselName, tc.TblName, tc.VmCount, tc.FMCount "
            sql += "               FROM TableCount tc"
            sql += "                     INNER JOIN tbl_Vessel v On v.VesselID = tc.VesselID "
            sql += "        ORDER BY tc.TblName"

            Helpers.DBHelper.FillSqlDataTable(tb, sql, tc.connectionstring)

            Return tb
        End Function

        Public Shared Function GetVessels(ByVal tc As Settings.SettingsTableCompare) As DataTable
            Dim tb As DataTable = Nothing
            Dim sql As String = "SELECT VesselID, VesselName FROM tbl_Vessel ORDER BY VesselName"

            Helpers.DBHelper.FillSqlDataTable(tb, sql, tc.connectionstring)

            Return tb
        End Function

        Public Shared Function GetTableCompareXml(ByVal tc As Settings.SettingsTableCompare) As String
            Dim sbXml As System.Text.StringBuilder = New System.Text.StringBuilder()
            Dim tbTableCompare As DataTable = GetTableCompareData(tc)
            Dim tbVessels As DataTable = GetVessels(tc)

            sbXml.Append("<?xml version=""1.0"" encoding=""utf-8"" ?>" + System.Environment.NewLine())
            sbXml.Append("<tablecompare version=""1.0"" installation=""" + tc.installation + """ company=""" + tc.company + """ application=""" + tc.application + """>" + System.Environment.NewLine())


            For Each vesselRow As DataRow In tbVessels.Rows
                sbXml.Append("<vessel id=""" + vesselRow("VesselName").ToString() + """>" + System.Environment.NewLine())

                For Each tableCompareRow As DataRow In tbTableCompare.Select("VesselID = '" + vesselRow("VesselID").ToString() + "'")
                    sbXml.Append("<vesseltable TableNameFM=""" + tableCompareRow("TblName").ToString() + """ TableNameVM=""" + GetVMTableName(tableCompareRow("TblName").ToString()) + """ TableRowsVM=""" + tableCompareRow("VmCount").ToString() + """ TableRowsFM=""" + tableCompareRow("FmCount").ToString() + """/>" + System.Environment.NewLine())
                Next

                sbXml.Append("</vessel>" + System.Environment.NewLine())
            Next

            sbXml.Append("</tablecompare>")

            Return sbXml.ToString()
        End Function

        Public Shared Function GetVMTableName(ByVal FMTableName As String) As String

            If (FMTableName.StartsWith("tbl_")) Then
                Return FMTableName.Substring(4)
            Else
                Return FMTableName
            End If

        End Function

    End Class

End Namespace
