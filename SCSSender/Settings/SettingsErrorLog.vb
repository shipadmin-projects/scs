﻿Namespace Settings

    Public Class SettingsErrorLog

        Public Property installation As String
        Public Property company As String
        Public Property application As String
        Public Property path As String
        Public Property connectionstring As String

        Public Sub New()
            Me.installation = ""
            Me.company = ""
            Me.application = ""
            Me.path = ""
            Me.connectionstring = ""
        End Sub

        Public Sub New(ByVal installation As String, ByVal company As String, ByVal application As String, ByVal path As String, ByVal connectionstring As String)
            Me.installation = installation
            Me.company = company
            Me.application = application
            Me.path = path
            Me.connectionstring = connectionstring
        End Sub
    End Class

End Namespace
