﻿
Namespace Settings

    Public Class SettingsTableCompare
        Public Property installation As String
        Public Property company As String
        Public Property application As String
        Public Property connectionstring As String

        Public Sub New()
            Me.installation = ""
            Me.company = ""
            Me.application = ""
            Me.connectionstring = ""
        End Sub

        Public Sub New(ByVal installation As String, ByVal company As String, ByVal application As String, ByVal connectionstring As String)
            Me.installation = installation
            Me.company = company
            Me.application = application
            Me.connectionstring = connectionstring
        End Sub

    End Class

End Namespace