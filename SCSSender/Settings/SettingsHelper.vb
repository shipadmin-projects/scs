﻿Imports System.Xml

Namespace Settings

    Public Class SettingsHelper

        Private Sub New()
        End Sub

        Public Shared Function ReadSettings(ByVal settingsFilePath As String) As Settings
            Dim s As New Settings()
            Dim xtr As New XmlTextReader(settingsFilePath)
            xtr.WhitespaceHandling = WhitespaceHandling.None
            xtr.Read()

            While Not xtr.EOF

                If (xtr.Name = "settings" And s.BackupFolder = "") Then
                    s.BackupFolder = xtr.GetAttribute("backupfolder")
                End If

                If (xtr.Name = "settings" And s.ReceiverURL = "") Then
                    s.ReceiverURL = xtr.GetAttribute("receiverurl")
                End If

                If (xtr.Name = "errorlogs") Then
                    xtr.Read()
                    While xtr.Name = "errorlog"
                        s.ErrorLogs.Add(New SettingsErrorLog(xtr.GetAttribute("installation"), xtr.GetAttribute("company"), xtr.GetAttribute("application"), xtr.GetAttribute("path"), xtr.GetAttribute("connectionstring")))
                        xtr.Read()
                    End While
                End If

                If (xtr.Name = "tablecompares") Then
                    xtr.Read()
                    While xtr.Name = "tablecompare"
                        s.TableCompares.Add(New SettingsTableCompare(xtr.GetAttribute("installation"), xtr.GetAttribute("company"), xtr.GetAttribute("application"), xtr.GetAttribute("connectionstring")))
                        xtr.Read()
                    End While
                End If

                xtr.Read()
            End While
            xtr.Close()
            Return s
        End Function



    End Class

End Namespace