﻿Namespace Settings

    Public Class Settings
        Public Property BackupFolder As String
        Public Property ReceiverURL As String
        Public Property ErrorLogs As SettingsErrorLogs
        Public Property TableCompares As FMTableCompare


        Public Sub New()
            Me.BackupFolder = ""
            Me.ReceiverURL = ""
            Me.ErrorLogs = New SettingsErrorLogs()
            Me.TableCompares = New FMTableCompare()
        End Sub
    End Class

End Namespace