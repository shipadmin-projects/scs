﻿Public Class Main

    Private Shared SettingsInfo As Settings.Settings

    Public Shared Sub Main()
        Try
            Helpers.LogHelper.LogInformation("SCSSender started")
            SettingsInfo = Settings.SettingsHelper.ReadSettings(System.IO.Path.Combine(System.Configuration.ConfigurationManager.AppSettings("ApplicationPath"), "settings.xml"))
            SendTableCompareData()
            SendErrorLogs()
            Helpers.LogHelper.LogInformation("SCSSender ended")
        Catch ex As Exception
            Try
                Helpers.LogHelper.LogError(ex)
            Finally
                Application.Exit()
            End Try
        Finally
            Application.Exit()
        End Try
    End Sub

    Private Shared Sub SendErrorLogs()
        For Each er As Settings.SettingsErrorLog In SettingsInfo.ErrorLogs
            Try
                Senders.ErrorLogSender.ProcessErrorLogs(SettingsInfo.BackupFolder, er)
            Catch ex As Exception
                Helpers.LogHelper.LogError(ex)
            End Try
        Next
    End Sub

    Private Shared Sub SendTableCompareData()
        For Each tc As Settings.SettingsTableCompare In SettingsInfo.TableCompares
            Try
                Senders.TableCompareSender.SendTableCompareData(tc)
            Catch ex As Exception
                Helpers.LogHelper.LogError(ex)
            End Try
        Next
    End Sub

End Class
