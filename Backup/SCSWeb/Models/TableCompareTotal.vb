﻿Public Class TableCompareTotal
    Public Property Name As String
    Public Property RowsEqual As Integer
    Public Property RowsDifferent As Integer

    Public ReadOnly Property RowsTotal As Integer
        Get
            Return RowsEqual + RowsDifferent
        End Get
    End Property

    Public ReadOnly Property StatusColor As String
        Get
            If (RowsDifferent > 0) Then
                Return "Red"
            Else
                Return "Green"
            End If
        End Get
    End Property
End Class
