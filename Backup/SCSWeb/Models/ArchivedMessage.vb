﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Globalization
Imports System.Data.Entity


<Table("ArchivedMessage")> _
Public Class ArchivedMessage
    Inherits BaseDBO
    Public Property MessageID As Guid
    Public Property ApplicationID As Integer
    Public Property CompanyID As Integer
    Public Property InstallationID As Integer
    Public Property Status As String
    Public Property StatusChangedByUserID As Integer
    Public Property Type As String
    <Display(Name:="Header")> _
    Public Property MessageHeader As String
    <Display(Name:="Details")> _
    Public Property MessageDetails As String
    Public Property Timestamp As DateTime
End Class
