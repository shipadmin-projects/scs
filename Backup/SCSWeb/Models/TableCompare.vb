﻿Public Class TableCompare
    Inherits BaseDBO

    Public Property VmID As Integer
    Public Property FmID As Integer
    Public Property CompanyID As Integer
    Public Property TableNameFM As String
    Public Property TableNameVM As String
    Public Property TableRowsVM As Integer
    Public Property TableRowsFM As Integer

    Public VmInstallation As Installation
    Public FmInstallation As Installation
    Public Company As Company

    Public ReadOnly Property IsRowsDifferent As Boolean
        Get
            Return Not (TableRowsFM = TableRowsVM)
        End Get
    End Property

    Public ReadOnly Property RowDifference As Integer
        Get
            Return Math.Abs(TableRowsFM - TableRowsVM)
        End Get
    End Property

    Public ReadOnly Property StatusColor As String
        Get
            If (IsRowsDifferent) Then
                Return "Red"
            Else
                Return "Green"
            End If
        End Get
    End Property

End Class
