﻿Imports System.Data.Entity

Public Class SCSDB
    Inherits DbContext

    Public Sub New()
        MyBase.New("SCSConnection")
    End Sub

    Public Property UserProfiles As DbSet(Of UserProfile)
    Public Property Messages As DbSet(Of Message)
    Public Property ArchivedMessages As DbSet(Of ArchivedMessage)
    Public Property Applications As DbSet(Of Application)
    Public Property Companies As DbSet(Of Company)
    Public Property Installations As DbSet(Of Installation)
    Public Property Users As DbSet(Of User)
    Public Property TableCompares As DbSet(Of TableCompare)
End Class
