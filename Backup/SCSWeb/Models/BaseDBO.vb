﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Globalization
Imports System.Data.Entity

Public Class BaseDBO

    <Key()>
    Public Property ID As Integer

    <ScaffoldColumn(True)> _
    <DisplayFormat(ApplyFormatInEditMode:=True, DataFormatString:="{0:dd.MM.yyyy HH:mm}")> _
    Public Property Updated As DateTime

    <ScaffoldColumn(False)> _
    <DisplayFormat(ApplyFormatInEditMode:=True, DataFormatString:="{0:dd.MM.yyyy HH:mm}")> _
    Public Property Created As DateTime

End Class
