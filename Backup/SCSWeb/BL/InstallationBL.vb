﻿Public Class InstallationBL

    Public Shared Function GetInstallations(ByVal CID As Integer, ByVal AID As Integer) As List(Of Installation)
        Dim db As New SCSDB
        Dim items As List(Of Installation)

        If (CID > 0 And AID > 0) Then
            items = (From i In db.Installations
                         Where i.CompanyID = CID And i.ApplicationID = AID
                         Select i).ToList()
        ElseIf (CID > 0 And AID < 0) Then
            items = (From i In db.Installations
                        Where i.CompanyID = CID
                        Select i).ToList()
        ElseIf (CID < 0 And AID > 0) Then
            items = (From i In db.Installations
                        Where i.ApplicationID = AID
                        Select i).ToList()
        Else
            items = (From i In db.Installations
                         Select i).ToList()
        End If

        Return items
    End Function

    Public Shared Function GetInstallations(ByVal CID As Integer) As List(Of Installation)
        Return GetInstallations(CID, -1)
    End Function

    Public Shared Function GetInstallations() As List(Of Installation)
        Return GetInstallations(-1, -1)
    End Function

   


End Class
