﻿Imports System.Data.Entity

Public Class CompanyController
    Inherits System.Web.Mvc.Controller

    Private db As New SCSDB

    '
    ' GET: /Company/
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Index() As ActionResult
        Return View(db.Companies.OrderBy(Function(a) a.Name).ToList())
    End Function

    '
    ' GET: /Company/Details/5
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Details(Optional ByVal id As Integer = Nothing) As ActionResult
        Dim company As Company = db.Companies.Find(id)
        If IsNothing(company) Then
            Return HttpNotFound()
        End If
        Return View(company)
    End Function

    '
    ' GET: /Company/Create
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Create() As ActionResult
        Return View()
    End Function

    '
    ' POST: /Company/Create

    <HttpPost()> _
    <ValidateAntiForgeryToken()> _
     <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Create(ByVal company As Company) As ActionResult
        If ModelState.IsValid Then
            db.Companies.Add(company)
            db.SaveChanges()
            Return RedirectToAction("Index")
        End If

        Return View(company)
    End Function

    '
    ' GET: /Company/Edit/5
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Edit(Optional ByVal id As Integer = Nothing) As ActionResult
        Dim company As Company = db.Companies.Find(id)
        If IsNothing(company) Then
            Return HttpNotFound()
        End If
        Return View(company)
    End Function

    '
    ' POST: /Company/Edit/5

    <HttpPost()> _
    <ValidateAntiForgeryToken()> _
     <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Edit(ByVal company As Company) As ActionResult
        If ModelState.IsValid Then
            db.Entry(company).State = EntityState.Modified
            db.SaveChanges()
            Return RedirectToAction("Index")
        End If

        Return View(company)
    End Function

    '
    ' GET: /Company/Delete/5
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Delete(Optional ByVal id As Integer = Nothing) As ActionResult
        Dim company As Company = db.Companies.Find(id)
        If IsNothing(company) Then
            Return HttpNotFound()
        End If
        Return View(company)
    End Function

    '
    ' POST: /Company/Delete/5

    <HttpPost()> _
    <ActionName("Delete")> _
    <ValidateAntiForgeryToken()> _
     <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function DeleteConfirmed(ByVal id As Integer) As RedirectToRouteResult
        Dim company As Company = db.Companies.Find(id)
        db.Companies.Remove(company)
        db.SaveChanges()
        Return RedirectToAction("Index")
    End Function

    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        db.Dispose()
        MyBase.Dispose(disposing)
    End Sub

End Class