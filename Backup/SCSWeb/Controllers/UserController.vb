﻿Imports System.Data.Entity


Public Class UserController
    Inherits System.Web.Mvc.Controller

    Private db As New SCSDB

    '
    ' GET: /User/
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Index() As ActionResult
        Return View(db.Users.ToList())
    End Function

    ''get user list
    <HttpPost>
     <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Index(ByVal SearchString As String) As ActionResult

        Dim User = From u In db.Users
                     Select u
                    Where u.Username.Contains(SearchString)

        ViewBag.SearchString = SearchString
        Return View(User.ToList())
    End Function
    '
    ' GET: /User/Details/5
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Details(Optional ByVal id As Integer = Nothing) As ActionResult
        Dim user As User = db.Users.Find(id)
        If IsNothing(user) Then
            Return HttpNotFound()
        End If
        Return View(user)
    End Function

    '
    ' GET: /User/Create
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Create() As ActionResult
        Return View()
    End Function

    '
    ' POST: /User/Create

    <HttpPost()> _
    <ValidateAntiForgeryToken()> _
     <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Create(ByVal user As User) As ActionResult
        If ModelState.IsValid Then

            Dim highest As Integer = 0
            For Each x As User In db.Users.ToList()
                If highest < x.ID Then
                    highest = x.ID
                End If
            Next

            Dim crypt As CryptographyBL = New CryptographyBL()

            user.Password = crypt.Encrypt(user.Password)




            user.ID = highest + 1

            user.Created = DateTime.Now
            user.Updated = DateTime.Now
            db.Users.Add(user)
            db.SaveChanges()
            Return RedirectToAction("Index")
        End If

        Return View(user)
    End Function

    '
    ' GET: /User/Register

    Function Register() As ActionResult
        Return View()
    End Function

    '
    ' POST: /User/Register

    <HttpPost()> _
    <ValidateAntiForgeryToken()> _
     <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Register(ByVal user As User) As ActionResult
        If ModelState.IsValid Then

            Dim highest As Integer = 0
            For Each x As User In db.Users.ToList()
                If highest < x.ID Then
                    highest = x.ID
                End If
            Next

            Dim crypt As CryptographyBL = New CryptographyBL()

            user.Password = crypt.Encrypt(user.Password)


            user.ID = highest + 1
            user.ForViewing = True
            user.Created = DateTime.Now
            user.Updated = DateTime.Now
            db.Users.Add(user)
            db.SaveChanges()
            Return RedirectToAction("Index")
        End If

        Return View(user)
    End Function

    '
    ' GET: /User/Edit/5

    Function Edit(Optional ByVal id As Integer = Nothing) As ActionResult
        Dim user As User = db.Users.Find(id)
      
        If IsNothing(user) Then
            Return HttpNotFound()
        End If
        Return View(user)
    End Function

    '
    ' POST: /User/Edit/5

    <HttpPost()> _
    <ValidateAntiForgeryToken()> _
     <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Edit(ByVal user As User) As ActionResult
        If ModelState.IsValid Then
            user.Updated = DateTime.Now
            Dim crypt As CryptographyBL = New CryptographyBL()
            If user.Password <> "" Then
                user.Password = crypt.Encrypt(user.Password)
                db.Users.Attach(user)
                db.Entry(user).State = EntityState.Modified
                db.SaveChanges()
                Return RedirectToAction("Index")
            Else
                ModelState.AddModelError("", "The password should not be blank.")
                Return View(user)
            End If
        End If

        Return View(user)
    End Function

    '
    ' GET: /User/Delete/5
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Delete(Optional ByVal id As Integer = Nothing) As ActionResult
        Dim user As User = db.Users.Find(id)
        If IsNothing(user) Then
            Return HttpNotFound()
        End If
        Return View(user)
    End Function

    '
    ' POST: /User/Delete/5

    <HttpPost()> _
    <ActionName("Delete")> _
    <ValidateAntiForgeryToken()> _
     <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function DeleteConfirmed(ByVal id As Integer) As RedirectToRouteResult
        Dim user As User = db.Users.Find(id)
        db.Users.Remove(user)
        db.SaveChanges()
        Return RedirectToAction("Index")
    End Function

    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        db.Dispose()
        MyBase.Dispose(disposing)
    End Sub

End Class