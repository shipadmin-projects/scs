﻿Imports System.Data.Entity

Public Class MessageController
    Inherits System.Web.Mvc.Controller

    Private db As New SCSDB

    '
    ' GET: /Message/

    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Index() As ActionResult
        'Dim lstMess As List(Of Message) = New List(Of Message)
        ' lstMess = db.Messages.ToList()
        ' For Each x As Message In lstMess
        ' x.Installation = db.Installations.Find(x.InstallationID)
        'x.Company = db.Companies.Find(x.CompanyID)
        'x.Application = db.Applications.Find(x.ApplicationID)

        '        Next


        ' Return View(db.Messages.ToList())
    End Function

    '
    ' GET: /Message/Details/5
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Details(Optional ByVal id As Integer = Nothing) As ActionResult
        Dim message As Message = db.Messages.Find(id)
        If IsNothing(message) Then
            Return HttpNotFound()
        End If
        Return View(message)
    End Function

    '
    ' GET: /Message/Create
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=True)> _
    Function Create() As ActionResult
        Return View()
    End Function

    '
    ' POST: /Message/Create

    <HttpPost()> _
    <ValidateAntiForgeryToken()> _
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=True)> _
    Function Create(ByVal message As Message, ByVal application As Application, ByVal company As Company) As ActionResult
        If ModelState.IsValid Then
            company.Created = DateTime.Now
            application.Created = DateTime.Now
            message.Created = DateTime.Now
            message.Updated = DateTime.Now

            message.Application = application
            message.Company = company
            message.ApplicationID = application.ID
            message.CompanyID = company.ID
            message.MessageDetails = ""
            message.MessageID = Guid.NewGuid()
            message.Status = "New"

            db.Applications.Add(application)
            db.Companies.Add(company)

            db.Messages.Add(message)
            db.SaveChanges()
            Return RedirectToAction("Index")
        End If

        Return View(message)
    End Function

    '
    ' GET: /Message/Edit/5
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=True)> _
    Function Edit(Optional ByVal id As Integer = Nothing) As ActionResult
        Dim message As Message = db.Messages.Find(id)
        If IsNothing(message) Then
            Return HttpNotFound()
        End If
        Return View(message)
    End Function

    '
    ' POST: /Message/Edit/5

    <HttpPost()> _
    <ValidateAntiForgeryToken()> _
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=True)> _
    Function Edit(ByVal message As Message) As ActionResult
        If ModelState.IsValid Then
            db.Entry(message).State = EntityState.Modified
            db.SaveChanges()
            Return RedirectToAction("Index")
        End If

        Return View(message)
    End Function

    '
    ' GET: /Message/Delete/5
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=True)> _
    Function Delete(Optional ByVal id As Integer = Nothing) As ActionResult
        Dim message As Message = db.Messages.Find(id)
        If IsNothing(message) Then
            Return HttpNotFound()
        End If
        Return View(message)
    End Function

    '
    ' POST: /Message/Delete/5

    <HttpPost()> _
    <ActionName("Delete")> _
    <ValidateAntiForgeryToken()> _
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=True)> _
    Function DeleteConfirmed(ByVal id As Integer) As RedirectToRouteResult
        Dim message As Message = db.Messages.Find(id)
        db.Messages.Remove(message)
        db.SaveChanges()
        Return RedirectToAction("Index")
    End Function

    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        db.Dispose()
        MyBase.Dispose(disposing)
    End Sub

    Function ChangeStatus(ByVal id As Integer, ByVal status As String)
        Dim message As Message = db.Messages.Find(id)
        If status = "New" Then
            message.Status = "Done"
        Else
            message.Status = "New"
        End If

        message.Updated = DateTime.Now
        db.SaveChanges()

        Return RedirectToAction("Index")
    End Function

End Class