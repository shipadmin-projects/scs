﻿Imports System.Data.Entity

Public Class ApplicationController
    Inherits System.Web.Mvc.Controller

    Private db As New SCSDB

    '
    ' GET: /Application/
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Index() As ActionResult
        Return View(db.Applications.OrderBy(Function(a) a.Name).ToList())
    End Function

    '
    ' GET: /Application/Details/5
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Details(Optional ByVal id As Integer = Nothing) As ActionResult
        Dim application As Application = db.Applications.Find(id)
        If IsNothing(application) Then
            Return HttpNotFound()
        End If
        Return View(application)
    End Function

    '
    ' GET: /Application/Create
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Create() As ActionResult
        Return View()
    End Function

    '
    ' POST: /Application/Create

    <HttpPost()> _
    <ValidateAntiForgeryToken()> _
     <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Create(ByVal application As Application) As ActionResult
        If ModelState.IsValid Then
            db.Applications.Add(application)
            db.SaveChanges()
            Return RedirectToAction("Index")
        End If

        Return View(application)
    End Function

    '
    ' GET: /Application/Edit/5
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Edit(Optional ByVal id As Integer = Nothing) As ActionResult
        Dim application As Application = db.Applications.Find(id)
        If IsNothing(application) Then
            Return HttpNotFound()
        End If
        Return View(application)
    End Function

    '
    ' POST: /Application/Edit/5

    <HttpPost()> _
    <ValidateAntiForgeryToken()> _
     <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Edit(ByVal application As Application) As ActionResult
        If ModelState.IsValid Then
            db.Entry(application).State = EntityState.Modified
            db.SaveChanges()
            Return RedirectToAction("Index")
        End If

        Return View(application)
    End Function

    '
    ' GET: /Application/Delete/5
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Delete(Optional ByVal id As Integer = Nothing) As ActionResult
        Dim application As Application = db.Applications.Find(id)
        If IsNothing(application) Then
            Return HttpNotFound()
        End If
        Return View(application)
    End Function

    '
    ' POST: /Application/Delete/5

    <HttpPost()> _
    <ActionName("Delete")> _
    <ValidateAntiForgeryToken()> _
     <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function DeleteConfirmed(ByVal id As Integer) As RedirectToRouteResult
        Dim application As Application = db.Applications.Find(id)
        db.Applications.Remove(application)
        db.SaveChanges()
        Return RedirectToAction("Index")
    End Function

    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        db.Dispose()
        MyBase.Dispose(disposing)
    End Sub

End Class