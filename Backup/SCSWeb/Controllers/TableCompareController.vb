﻿Imports System.Data.Entity

Public Class TableCompareController
    Inherits System.Web.Mvc.Controller


    Private db As New SCSDB
    '
    ' GET: /CompanyTableCompare
    ''' <summary>
    ''' return a list of companies with number of errors and color for status
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Index() As ActionResult

        Dim l As List(Of TableCompareCompany) = New List(Of TableCompareCompany)

        Dim companies = From Company In db.Companies.OrderBy(Function(a) a.Name).ToList()


        For Each c As Company In companies
            Dim ctc As TableCompareCompany = New TableCompareCompany()
            ctc.Name = c.Name
            ctc.ID = c.ID
            ctc.RowsUnequal = TableComparesBL.GetNumberOfUnequalRows(c.ID)
            ctc.RowsEqual = TableComparesBL.GetNumberOfEqualRows(c.ID)
            ctc.TablesUnequal = TableComparesBL.GetNumberOfUnequalTables(c.ID)
            ctc.TablesEqual = TableComparesBL.GetNumberOfEqualTables(c.ID)

            ctc.RowsFM = TableComparesBL.GetNumberOfRowsFM(c.ID)
            ctc.RowsVM = TableComparesBL.GetNumberOfRowsVM(c.ID)
            ctc.Updated = c.Updated



            l.Add(ctc)
        Next

        Return View(l)

    End Function

    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Company(ByVal ID? As Integer) As ActionResult
        Dim l As List(Of TableCompareVessel) = New List(Of TableCompareVessel)
        Dim vessels As List(Of Installation)
        Dim ApplicationID As Integer = (From A In db.Applications Where A.Name = "VM").Single().ID
        If (ID IsNot Nothing) Then
            vessels = (From C In db.Installations Where C.ApplicationID = ApplicationID And C.CompanyID = ID And C.InActive <> 1).ToList()
        Else
            vessels = (From C In db.Installations Where C.ApplicationID = ApplicationID And C.InActive <> 1).ToList()
        End If
        Dim SelectedCompany As SCSWeb.Company = (From C In db.Companies Where C.ID = ID).Single
        ViewBag.CompanyName = SelectedCompany.Name
        ViewBag.CompanyID = SelectedCompany.ID
        For Each i As Installation In vessels
            Dim tcv As TableCompareVessel = New TableCompareVessel()
            tcv.Name = i.Name
            tcv.ID = i.ID
            tcv.RowsUnequal = TableComparesBL.GetNumberOfUnequalRows(ID, i.ID)
            tcv.RowsEqual = TableComparesBL.GetNumberOfEqualRows(ID, i.ID)
            tcv.TablesUnequal = TableComparesBL.GetNumberOfUnequalTables(ID, i.ID)
            tcv.TablesEqual = TableComparesBL.GetNumberOfEqualTables(ID, i.ID)
            tcv.RowsFM = TableComparesBL.GetNumberOfRowsFM(ID, i.ID)
            tcv.RowsVM = TableComparesBL.GetNumberOfRowsVM(ID, i.ID)
            tcv.Updated = i.Updated
            l.Add(tcv)
        Next
        l.Sort(Function(x, y) y.RowsUnequal.CompareTo(x.RowsUnequal))
        Return View(l)
    End Function
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Vessel(ByVal ID? As Integer) As ActionResult

        Dim VmInstallation As SCSWeb.Installation = (From I In db.Installations Where I.ID = ID And I.InActive <> 1).Single
        Dim SelectedCompany As SCSWeb.Company = (From C In db.Companies Where C.ID = VmInstallation.CompanyID).Single
        ViewBag.VesselName = VmInstallation.Name
        ViewBag.CompanyName = SelectedCompany.Name
        ViewBag.CompanyID = VmInstallation.CompanyID
        Dim rows As List(Of TableCompare)
        If (ID IsNot Nothing) Then
            rows = (From TC In db.TableCompares Where TC.VmID = VmInstallation.ID Order By TC.TableNameVM).ToList()
        Else
            rows = (From TC In db.TableCompares Order By TC.TableNameVM).ToList()
        End If

        Return View(rows)
    End Function

    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Table(ByVal ID As String) As ActionResult

        Dim vessels As List(Of Installation)
        Dim VmInstallation As SCSWeb.TableCompare
        Dim SelectedTable As SCSWeb.Company
        Dim VmInstallationName As SCSWeb.Installation

        Dim ApplicationID As Integer = (From A In db.Applications Where A.Name = "VM").Single().ID
        If IsNumeric(ID) = True Then
            Dim CompanyID As Integer = (From t In db.TableCompares Where t.ID = ID).Single().CompanyID
        End If

        'vessels = (From C In db.Installations Where C.ApplicationID = ApplicationID And C.InActive <> 1).ToList()

        If IsNumeric(ID) = True Then
            VmInstallation = (From I In db.TableCompares Where I.ID = ID).Single
            SelectedTable = (From C In db.Companies Where C.ID = VmInstallation.CompanyID).Single
            VmInstallationName = (From I In db.Installations Where I.ID = VmInstallation.VmID And I.InActive <> 1).Single

            ViewBag.CompanyID = SelectedTable.ID
            ViewBag.CompanyName = SelectedTable.Name
            ViewBag.VesselName = VmInstallationName.Name
            ViewBag.VesselID = VmInstallationName.ID
            ViewBag.TableName = VmInstallation.TableNameVM
            ViewBag.ActionNameCompany = "Company"
            ViewBag.ActionNameVessel = "Vessel"
            vessels = (From C In db.Installations Where C.ApplicationID = ApplicationID And C.InActive <> 1 And C.CompanyID = SelectedTable.ID).ToList()
        Else
            VmInstallation = New SCSWeb.TableCompare
            VmInstallation.TableNameVM = ID
            ViewBag.CompanyID = ""
            ViewBag.CompanyName = "Overview"
            ViewBag.VesselName = "Vessel Overview"
            ViewBag.VesselID = "0"
            ViewBag.TableName = ID
            ViewBag.ActionNameCompany = "Overview"
            ViewBag.ActionName = "Overview"
            vessels = (From C In db.Installations Where C.ApplicationID = ApplicationID And C.InActive <> 1).ToList()
        End If
        Dim rows As List(Of TableCompareVessel) = New List(Of TableCompareVessel)
        For Each i As Installation In vessels
            Dim tcv As TableCompareVessel = New TableCompareVessel()
            tcv.Name = i.Name
            tcv.ID = i.ID
            tcv.RowsUnequal = TableComparesBL.GetNumberOfUnequalTableRows(ID, i.CompanyID, i.ID, VmInstallation.TableNameVM) 'tablecompare id, installation's company id, installation id
            tcv.RowsEqual = TableComparesBL.GetNumberOfEqualRowsTable(ID, i.CompanyID, i.ID, VmInstallation.TableNameVM)
            tcv.TablesUnequal = TableComparesBL.GetNumberOfUnequalTableSelected(ID, i.CompanyID, i.ID, VmInstallation.TableNameVM)
            tcv.TablesEqual = TableComparesBL.GetNumberOfEqualTablesSelected(ID, i.CompanyID, i.ID, VmInstallation.TableNameVM)
            tcv.RowsFM = TableComparesBL.GetNumberOfTableRowsFM(ID, i.CompanyID, i.ID, VmInstallation.TableNameVM)
            tcv.RowsVM = TableComparesBL.GetNumberOfTableRowsVM(ID, i.CompanyID, i.ID, VmInstallation.TableNameVM)
            tcv.Updated = TableComparesBL.GetLastUpdate(ID, i.CompanyID, i.ID, VmInstallation.TableNameVM)
            rows.Add(tcv)
        Next
        rows.Sort(Function(x, y) y.RowsUnequal.CompareTo(x.RowsUnequal))
        Return View(rows)
    End Function
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Overview(Optional ByVal ID? As Integer = 0) As ActionResult 'Comment added
        Dim TCOver As List(Of TableCompareOverview) = New List(Of TableCompareOverview)
        Dim tcount = (From TableCompares In db.TableCompares
                        Group TableCompares By TableCompares.TableNameVM Into g = Group
                        Order By
                          TableNameVM
                        Select
                          TableNameVM,
                          totalvm = CType(g.Sum(Function(p) p.TableRowsVM), Int32?),
                          totalfm = CType(g.Sum(Function(p) p.TableRowsFM), Int32?)).ToList()
        For Each item In tcount
            Dim ovr As TableCompareOverview = New TableCompareOverview()
            ovr.Name = item.TableNameVM
            ovr.totalVM = item.totalvm
            ovr.totalFM = item.totalfm
            TCOver.Add(ovr)
        Next
        Return View(TCOver)
    End Function

    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        db.Dispose()
        MyBase.Dispose(disposing)
    End Sub


    'Private db As New SCSDB

    ''
    '' GET: /TableCompare/

    'Function Index() As ActionResult

    '    Dim data = From TableCompare In db.TableCompares.ToList()


    '    For Each d As TableCompare In data
    '        d.VmInstallation = (From x In db.Installations Where x.ID = d.VmID Select x).First()
    '        d.FmInstallation = (From x In db.Installations Where x.ID = d.FmID Select x).First()
    '    Next

    '    Return View(data)

    '    '  Return View(db.TableCompares.ToList())
    'End Function

    ''
    '' GET: /TableCompare/Details/5

    'Function Details(Optional ByVal id As Integer = Nothing) As ActionResult
    '    Dim tablecompare As TableCompare = db.TableCompares.Find(id)
    '    If IsNothing(tablecompare) Then
    '        Return HttpNotFound()
    '    End If



    '    Return View(tablecompare)
    'End Function

    ''
    '' GET: /TableCompare/Create

    'Function Create() As ActionResult
    '    Return View()
    'End Function

    ''
    '' POST: /TableCompare/Create

    '<HttpPost()> _
    '<ValidateAntiForgeryToken()> _
    'Function Create(ByVal tablecompare As TableCompare) As ActionResult
    '    If ModelState.IsValid Then
    '        db.TableCompares.Add(tablecompare)
    '        db.SaveChanges()
    '        Return RedirectToAction("Index")
    '    End If

    '    Return View(tablecompare)
    'End Function

    ''
    '' GET: /TableCompare/Edit/5

    'Function Edit(Optional ByVal id As Integer = Nothing) As ActionResult
    '    Dim tablecompare As TableCompare = db.TableCompares.Find(id)
    '    If IsNothing(tablecompare) Then
    '        Return HttpNotFound()
    '    End If
    '    Return View(tablecompare)
    'End Function

    ''
    '' POST: /TableCompare/Edit/5

    '<HttpPost()> _
    '<ValidateAntiForgeryToken()> _
    'Function Edit(ByVal tablecompare As TableCompare) As ActionResult
    '    If ModelState.IsValid Then
    '        db.Entry(tablecompare).State = EntityState.Modified
    '        db.SaveChanges()
    '        Return RedirectToAction("Index")
    '    End If

    '    Return View(tablecompare)
    'End Function

    ''
    '' GET: /TableCompare/Delete/5

    'Function Delete(Optional ByVal id As Integer = Nothing) As ActionResult
    '    Dim tablecompare As TableCompare = db.TableCompares.Find(id)
    '    If IsNothing(tablecompare) Then
    '        Return HttpNotFound()
    '    End If
    '    Return View(tablecompare)
    'End Function

    ''
    '' POST: /TableCompare/Delete/5

    '<HttpPost()> _
    '<ActionName("Delete")> _
    '<ValidateAntiForgeryToken()> _
    'Function DeleteConfirmed(ByVal id As Integer) As RedirectToRouteResult
    '    Dim tablecompare As TableCompare = db.TableCompares.Find(id)
    '    db.TableCompares.Remove(tablecompare)
    '    db.SaveChanges()
    '    Return RedirectToAction("Index")
    'End Function

    'Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    '    db.Dispose()
    '    MyBase.Dispose(disposing)
    'End Sub

End Class