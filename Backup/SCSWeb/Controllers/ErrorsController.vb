﻿Imports System.Data.Entity

Public Class ErrorsController
    Inherits System.Web.Mvc.Controller

    Private db As New SCSDB
    Const recordsPerPage As Integer = 30
    Function RefreshCount() As ActionResult
        Return PartialView("_ErrorCount")
    End Function
    '
    ' GET: /Errors/
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Index(id As System.Nullable(Of Integer)) As ActionResult
        ' SetViewData()

        If Session("NumbertoSearch") = 0 Then


            Dim selectedCompany As Integer = -1
            Dim selectedInstallation As Integer = -1
            Dim selectedApplication As Integer = -1
            Dim ErrorsList As List(Of ErrorModel) = New List(Of ErrorModel)

            If (Session("Company") <> Nothing) Then
                selectedCompany = Session("Company")
            End If

            If (Session("Application") <> Nothing) Then
                selectedApplication = Session("Application")
            End If

            If (Session("Installation") <> Nothing) Then
                selectedInstallation = Session("Installation")
            End If

            Dim cmps = From Companies In db.Companies Select Companies

            Dim apps = ApplicationBL.GetApplications(selectedCompany) ' From Application In db.Applications Select Application
            Dim ints = InstallationBL.GetInstallations(selectedCompany, selectedApplication) 'From Installations In db.Installations Where Installations.CompanyID = selectedCompany Select Installations


            ViewData("SearchApplication") = New SelectList(apps.OrderBy(Function(a) a.Name).ToList(), "ID", "Name", selectedApplication)
            ViewData("SearchApplicationID") = selectedApplication
            ViewData("SearchCompany") = New SelectList(cmps.OrderBy(Function(a) a.Name).ToList(), "ID", "Name", selectedCompany)
            ViewData("SearchCompanyID") = selectedCompany
            ViewData("SearchInstallation") = New SelectList(ints.OrderBy(Function(a) a.Name).ToList(), "ID", "Name", selectedInstallation)
            ViewData("SearchInstallationID") = selectedInstallation

            'Dim ErrorsList As List(Of ErrorModel) = ErrorsBL.GetErrors("New").Where(Function(a) a.ID = selectedApplication).Where(Function(c) c.ID = selectedCompany).Where(Function(i) i.ID = selectedInstallation).ToList()
            'ErrorsList

            setSearchColumns()

            Dim page = If(id, 0)
            'Session("TotalErrors") = (page + 1) * 30
            ViewBag.TotalErrors = Session("TotalErrors")

            ' If Session("NumbertoSearch") = 0 Then
            If Request.IsAjaxRequest() Then
                ErrorsList = ErrorsBL.GetErrorChunks("", "New", selectedCompany, selectedInstallation, selectedApplication, "", Pages:=page)
                Session("TotalErrors") = Convert.ToInt32(Session("TotalErrors")) + ErrorsList.Count
                Return PartialView("_Errors", ErrorsList)
            End If
            '   End If

            ErrorsList = ErrorsBL.GetErrorChunks("", "New", selectedCompany, selectedInstallation, selectedApplication, "", Pages:=page)
            Session("TotalErrors") = ErrorsList.Count

            Dim Totalcount As Integer = 0

            Totalcount = ErrorsBL.GetErrorCount("New", selectedCompany, selectedInstallation, selectedApplication, "")
            Session("TotalErrors") = ErrorsList.Count

            If IsNothing(Totalcount) Then
                Session("TotalErrorsCount") = 0
            Else
                Session("TotalErrorsCount") = Totalcount
            End If

            Return View("Index", ErrorsList)

        End If
    End Function
    Public Function ShowAllErrorResults(ByRef ErrorsList As List(Of ErrorModel), ByVal Number As String, ByVal SearchString As String, ByVal SearchApplicationID As String, ByVal SearchCompanyID As String, ByVal SearchInstallationID As String, ByVal SearchColumnID As String) As ActionResult
        Dim selectedCompany As Integer = -1
        Dim selectedInstallation As Integer = -1
        Dim selectedApplication As Integer = -1
        Dim selectedcolumnID As Integer = -1

        If (SearchCompanyID <> "") Then
            selectedCompany = Session("Company")
        End If

        If (SearchApplicationID <> "") Then
            selectedApplication = Session("Application")
        End If

        If (SearchInstallationID <> "") Then
            selectedInstallation = Session("Installation")
        End If

        If (SearchString <> "") Then
            SearchString = SearchString.Replace("'", "''")
        End If


        Dim cmps = From Companies In db.Companies Select Companies

        Dim apps = ApplicationBL.GetApplications(selectedCompany) ' From Application In db.Applications Select Application
        Dim ints = InstallationBL.GetInstallations(selectedCompany, selectedApplication) 'From Installations In db.Installations Where Installations.CompanyID = selectedCompany Select Installations


        ViewData("SearchApplication") = New SelectList(apps.OrderBy(Function(a) a.Name).ToList(), "ID", "Name", selectedApplication)
        ViewData("SearchApplicationID") = selectedApplication
        ViewData("SearchCompany") = New SelectList(cmps.OrderBy(Function(a) a.Name).ToList(), "ID", "Name", selectedCompany)
        ViewData("SearchCompanyID") = selectedCompany
        ViewData("SearchInstallation") = New SelectList(ints.OrderBy(Function(a) a.Name).ToList(), "ID", "Name", selectedInstallation)
        ViewData("SearchInstallationID") = selectedInstallation



        ErrorsList = ErrorsBL.showallErrors(Number, "New", selectedCompany, selectedInstallation, selectedApplication, SearchString, SearchColumnID)
        Session("TotalErrors") = ErrorsList.Count
        Session("TotalErrorsCount") = ErrorsList.Count
        Session("NumbertoSearch") = ErrorsList.Count



    End Function

    Public Function [Error](id As System.Nullable(Of Integer)) As ActionResult
        If Session("NumbertoSearch") = 0 Then
            Dim selectedCompany As Integer = -1
            Dim selectedInstallation As Integer = -1
            Dim selectedApplication As Integer = -1

            Dim ErrorsList As List(Of ErrorModel) = New List(Of ErrorModel)


            If (Session("Company") <> Nothing) Then
                selectedCompany = Session("Company")

            End If

            If (Session("Application") <> Nothing) Then
                selectedApplication = Session("Application")

            End If

            If (Session("Installation") <> Nothing) Then
                selectedInstallation = Session("Installation")

            End If

            Dim cmps = From Companies In db.Companies Select Companies

            Dim apps = ApplicationBL.GetApplications(selectedCompany) ' From Application In db.Applications Select Application
            Dim ints = InstallationBL.GetInstallations(selectedCompany, selectedApplication) 'From Installations In db.Installations Where Installations.CompanyID = selectedCompany Select Installations


            ViewData("SearchApplication") = New SelectList(apps.OrderBy(Function(a) a.Name).ToList(), "ID", "Name", selectedApplication)
            ViewData("SearchApplicationID") = selectedApplication
            ViewData("SearchCompany") = New SelectList(cmps.OrderBy(Function(a) a.Name).ToList(), "ID", "Name", selectedCompany)
            ViewData("SearchCompanyID") = selectedCompany
            ViewData("SearchInstallation") = New SelectList(ints.OrderBy(Function(a) a.Name).ToList(), "ID", "Name", selectedInstallation)
            ViewData("SearchInstallationID") = selectedInstallation

            Session("Company") = Nothing
            Session("Application") = Nothing
            Session("Installation") = Nothing


            Dim page = If(id, 0)


            Session("TotalErrors") = (page + 1) * 30


            ' If Session("NumbertoSearch") = 0 Then
            If Request.IsAjaxRequest() Then
                ErrorsList = ErrorsBL.GetErrorChunks("", "New", selectedCompany, selectedInstallation, selectedApplication, "", Pages:=page)
                Session("TotalErrors") = Convert.ToInt32(Session("TotalErrors")) + ErrorsList.Count
                Return PartialView("_Errors", ErrorsList)
            End If

            ' End If

            ErrorsList = ErrorsBL.GetErrorChunks("", "New", selectedCompany, selectedInstallation, selectedApplication, "", Pages:=page)
            ViewBag.TotalErrors = Session("TotalErrors")
            Session("TotalErrors") = ErrorsList.Count
            'ViewBag.TotalErrors = GetPaginatedErrors(page, ErrorsList).Count()

            Return View("Index", ErrorsList)
        End If
    End Function


    Private Function GetPaginatedErrors(Optional page As Integer = 1, Optional Err As List(Of ErrorModel) = Nothing) As List(Of ErrorModel)
        'Dim skipRecords = page * recordsPerPage
        Dim totRecords As Integer = page * recordsPerPage

        'ViewBag.TotalErrors = totRecords

        Dim listOfProducts As List(Of ErrorModel) = Err
        If totRecords = 0 Then
            Return listOfProducts.OrderBy(Function(x) x.ID).Take(recordsPerPage).ToList()
        End If
        Return listOfProducts.OrderBy(Function(x) x.ID).Take(totRecords).ToList()
        'Return listOfProducts.OrderBy(Function(x) x.ID).Skip(skipRecords).Take(recordsPerPage).ToList()
    End Function

    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Index1(ByVal number As String, ByVal SearchString As String, ByVal SearchColumnID As String)
        Return Index(number, SearchString, "", "", "", SearchColumnID, "")
    End Function

    <HttpPost>
     <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Index(ByVal number As String, ByVal SearchString As String, ByVal SearchApplicationID As String, ByVal SearchCompanyID As String, ByVal SearchInstallationID As String, ByVal SearchColumnID As String, Optional ByVal search As String = "") As ActionResult
        Dim show As Boolean = False

        Session("NumbertoSearch") = 0
        ' SetViewData()


        'Dim errors = From Messages In db.Messages
        '                   Select Messages
        '                  Where (SearchString = "" Or Messages.MessageDetails.Contains(SearchString)) And (SearchApplication = "" Or Messages.ApplicationID = CInt(SearchApplication)) And (SearchCompany = "" Or Messages.CompanyID = CInt(SearchCompany)) And (SearchInstallation = "" Or Messages.InstallationID = CInt(SearchInstallation))

        ViewBag.SearchString = SearchString

        'If errors.ToList().Count = 0 Then
        '    Dim lstErrors As New List(Of SCSWeb.Message)
        '    Return View(lstErrors)
        'End If

        'errors = errors.ToList()

        'For Each e As Message In errors
        '    e.Application = (From x In db.Applications Where x.ID = e.ApplicationID Select x).First()
        '    e.Installation = (From x In db.Installations Where x.ID = e.InstallationID Select x).First()
        '    e.StatusChangedByUser = (From x In db.Users Where x.ID = e.StatusChangedByUserID Select x).First()
        '    e.Company = (From x In db.Companies Where x.ID = e.CompanyID Select x).First()
        'Next

        Dim selectedCompany As Integer = -1
        Dim selectedInstallation As Integer = -1
        Dim selectedApplication As Integer = -1


        If SearchCompanyID <> "" Then
            If (SearchCompanyID.ToString <> Session("Company")) Then
                Session("TotalErrors") = Nothing
            End If

            show = True
        End If


        If SearchApplicationID <> "" Then
            If (SearchApplicationID.ToString <> Session("Application")) Then
                Session("TotalErrors") = Nothing
            End If
            show = True
        End If


        If SearchInstallationID <> "" Then
            If (SearchInstallationID.ToString <> Session("Installation")) Then
                Session("TotalErrors") = Nothing
            End If

            show = True
        End If

        If SearchString <> "" Then
            show = True
        End If


        If (SearchCompanyID <> "") Then
            selectedCompany = Integer.Parse(SearchCompanyID)
            Session("Company") = selectedCompany
        End If

        If (SearchApplicationID <> "") Then
            selectedApplication = Integer.Parse(SearchApplicationID)
            Session("Application") = selectedApplication
        End If

        If (SearchInstallationID <> "") Then
            selectedInstallation = Integer.Parse(SearchInstallationID)
            Session("Installation") = selectedInstallation
        End If



        Dim cmps = From Companies In db.Companies Select Companies

        Dim apps = ApplicationBL.GetApplications(selectedCompany) ' From Application In db.Applications Select Application
        Dim ints = InstallationBL.GetInstallations(selectedCompany, selectedApplication) 'From Installations In db.Installations Where Installations.CompanyID = selectedCompany Select Installations

        ViewData("SearchApplication") = New SelectList(apps.OrderBy(Function(a) a.Name).ToList(), "ID", "Name")
        ViewData("SearchCompany") = New SelectList(cmps.OrderBy(Function(a) a.Name).ToList(), "ID", "Name")
        ViewData("SearchInstallation") = New SelectList(ints.OrderBy(Function(a) a.Name).ToList(), "ID", "Name")


        setSearchColumns()

        SearchString = SearchString.Replace("'", "''")

        Dim ErrorsList As New List(Of SCSWeb.ErrorModel)
        Dim Totalcount As Integer = 0




        If search = "ShowAllErrorResults" Then
            ShowAllErrorResults(ErrorsList, number, SearchString, SearchApplicationID, SearchCompanyID, SearchInstallationID, SearchColumnID)
        Else
            If SearchColumnID <> "" Or number <> "" Then
                ErrorsList = ErrorsBL.GetErrorChunks(number, "New", selectedCompany, selectedInstallation, selectedApplication, SearchString, Pages:=0, SpecifiedColumn:=SearchColumnID)
                Totalcount = ErrorsBL.GetErrorCount("New", selectedCompany, selectedInstallation, selectedApplication, SearchString, SpecifiedColumn:=SearchColumnID)
            Else
                ErrorsList = ErrorsBL.GetErrorChunks(number, "New", selectedCompany, selectedInstallation, selectedApplication, SearchString, Pages:=0)
                Totalcount = ErrorsBL.GetErrorCount("New", selectedCompany, selectedInstallation, selectedApplication, SearchString)
            End If

            Session("TotalErrors") = ErrorsList.Count
            If IsNothing(Totalcount) Then
                Session("TotalErrorsCount") = 0
            Else
                Session("TotalErrorsCount") = Totalcount
            End If
            Session("NumbertoSearch") = ErrorsList.Count
        End If


        ViewBag.Show = show

        Return View("Index", ErrorsList)



    End Function

    Sub SetViewData()
        Dim selectedCompany As Integer = -1
        Dim selectedInstallation As Integer = -1
        Dim selectedApplication As Integer = -1

        If (Session("Company") <> Nothing) Then
            selectedCompany = Session("Company")

        End If

        If (Session("Application") <> Nothing) Then
            selectedApplication = Session("Application")

        End If

        If (Session("Installation") <> Nothing) Then
            selectedInstallation = Session("Installation")

        End If

        Dim apps = ApplicationBL.GetApplications(selectedCompany) ' From Application In db.Applications Select Application
        Dim ints = InstallationBL.GetInstallations(selectedCompany, selectedApplication) 'From Installations In db.Installations Where Installations.CompanyID = selectedCompany Select Installations


        Dim cmps = From Companies In db.Companies Select Companies


        ViewData("SearchApplication") = New SelectList(apps.OrderBy(Function(a) a.Name).ToList(), "ID", "Name")
        ViewData("SearchCompany") = New SelectList(cmps.OrderBy(Function(a) a.Name).ToList(), "ID", "Name")
        ViewData("SearchInstallation") = New SelectList(ints.OrderBy(Function(a) a.Name).ToList(), "ID", "Name")



    End Sub

    Protected Sub setSearchColumns()
        Dim columns As New List(Of SelectListItem)

        Dim column As New SelectListItem
        column.Text = "ID"
        column.Value = "m.ID"
        columns.Add(column)

        column = New SelectListItem
        column.Text = "Message Details"
        column.Value = "MessageDetails"
        columns.Add(column)

        column = New SelectListItem
        column.Text = "Message Header"
        column.Value = "MessageHeader"
        columns.Add(column)

        column = New SelectListItem
        column.Text = "Specific Timestamp"
        column.Value = "Timestamp"
        columns.Add(column)

        'column = New SelectListItem
        'column.Text = "Timestamp Range"
        'column.Value = "TimestampRange"
        'columns.Add(column)

        ViewData("SearchColumns") = New SelectList(columns, "Value", "Text")

    End Sub


    '<HttpPost>
    'Function Search(ByVal SearchString As String, ByVal SearchApplication As String, ByVal SearchCompany As String, ByVal SearchInstallation As String) As ActionResult

    '    Dim errors = From Messages In db.Messages
    '                 Select Messages
    '                Where (SearchString = "" Or Messages.MessageDetails.Contains(SearchString)) And (SearchApplication = "" Or Messages.ApplicationID = CInt(SearchApplication)) And (SearchCompany = "" Or Messages.CompanyID = CInt(SearchCompany)) And (SearchInstallation = "" Or Messages.InstallationID = CInt(SearchInstallation))

    '    ViewBag.SearchString = SearchString
    '    Return RedirectToAction("Index")

    'End Function



    '
    ' GET: /Errors/Details/5
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Details(Optional ByVal id As Integer = Nothing) As ActionResult
        Dim message As Message = db.Messages.Find(id)
        Dim InsName As String = db.Installations.Find(message.InstallationID).Name
        Dim CmpName As String = db.Companies.Find(message.CompanyID).Name
        Dim AppName As String = db.Applications.Find(message.ApplicationID).Name
        Dim uName As String
        Try
            uName = db.Users.Find(message.StatusChangedByUserID).Name
        Catch ex As NullReferenceException
            uName = "Not Set"
        End Try
        ViewBag.InsName = InsName
        ViewBag.CmpName = CmpName
        ViewBag.AppName = AppName
        ViewBag.uName = uName
        If IsNothing(message) Then
            Return HttpNotFound()
        End If
        Return View(message)
    End Function

    '<HttpPost>
    <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function ChangeStatus(Optional ByVal id As Integer = Nothing, Optional ByVal status As String = "") As ActionResult
        Dim message As Message = db.Messages.Find(id)
        If IsNothing(message) Then
            Return HttpNotFound()
        Else
            If ModelState.IsValid Then

                message.Status = status
                message.StatusChangedByUserID = CInt(Session("UserID"))
                message.Updated = DateTime.Now()
                message.Timestamp = DateTime.Now()
                db.Entry(message).State = EntityState.Modified
                db.SaveChanges()

            End If
        End If
        Return RedirectToAction("Index")
    End Function

    <HttpPost()>
     <PermissionBL(LoginPage:="~/Account/Login", NeedFullAccess:=False)> _
    Function Submit(ByVal id As String, ByVal submitButton As String) As ActionResult


        If submitButton = "Done" Then
            Return ChangeStatus(CInt(id), "Done")
        Else
            Return ChangeStatus(CInt(id), "New")
        End If



    End Function


    Function ChangeMultipleStatus(ByVal id As Integer(), ByVal status As String) As ActionResult

        Dim message As Message

        For Each num As Integer In id
            message = db.Messages.Find(num)

            If IsNothing(message) = False Then

                If ModelState.IsValid Then
                    message.Status = status
                    message.StatusChangedByUserID = CInt(Session("UserID"))
                    message.Updated = DateTime.Now()
                    message.Timestamp = DateTime.Now()
                    db.Entry(message).State = EntityState.Modified
                    db.SaveChanges()
                End If

            End If

        Next

        Return RedirectToAction("Index")

    End Function

    Function ChangeSingleStatus(ByVal id As Integer(), ByVal status As String) As ActionResult

        Dim message As Message

        For Each num As Integer In id
            message = db.Messages.Find(num)

            If IsNothing(message) = False Then

                If ModelState.IsValid Then
                    message.Status = status
                    message.StatusChangedByUserID = CInt(Session("UserID"))
                    message.Updated = DateTime.Now()
                    message.Timestamp = DateTime.Now()
                    db.Entry(message).State = EntityState.Modified
                    db.SaveChanges()
                End If

            End If

        Next

        Return RedirectToAction("Index")

    End Function

    <HttpPost()>
    Function SubmitMultiple(ByVal selectedObjects As Integer(), ByVal submitButton As String) As ActionResult


        If submitButton = "Mark as Done" Then
            Return ChangeMultipleStatus(selectedObjects, "Done")
        Else
            Return ChangeSingleStatus(selectedObjects, "Done")
        End If


    End Function

    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        db.Dispose()
        MyBase.Dispose(disposing)
    End Sub







End Class