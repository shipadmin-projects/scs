﻿@ModelType IEnumerable(Of SCSWeb.User)

@Code
    ViewData("Title") = "Users"
End Code

<h2>Index</h2>

<p>
    @Html.ActionLink("Create New", "Create")
</p>

<p>
   @Using Html.BeginForm("Index", "User", FormMethod.Post)
        @Html.TextBox("SearchString")
        @<input type="submit" value="Search" />
    End Using
</p>
    

    
<table>
    <tr>
        <th>
            @Html.DisplayNameFor(Function(model) model.Email)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.Username)
        </th>
    @*    <th>
            @Html.DisplayNameFor(Function(model) model.Password)
        </th>*@
        <th>
            @Html.DisplayNameFor(Function(model) model.Name)
        </th>
               <th>
            @Html.DisplayNameFor(Function(model) model.ForViewing)
        </th>
@*        <th>
            @Html.DisplayNameFor(Function(model) model.Updated)
        </th>

        <th>
            @Html.DisplayNameFor(Function(model) model.Created)
        </th>*@
        <th></th>
    </tr>

@For Each item In Model
    Dim currentItem = item
    @<tr>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Email)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Username)
        </td>
@*        <td>
           @Html.DisplayFor(Function(modelItem) currentItem.Password)
        </td>*@
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Name)
        </td>
         <td>
            @Html.DisplayFor(Function(modelItem) currentItem.ForViewing)
        </td>
@*        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Updated)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Created)
        </td>*@
        <td>
            @Html.ActionLink("Edit", "Edit", New With {.id = currentItem.ID}) |
            @Html.ActionLink("Details", "Details", New With {.id = currentItem.ID}) |
            @Html.ActionLink("Delete", "Delete", New With {.id = currentItem.ID})
        </td>
    </tr>
Next

</table>
