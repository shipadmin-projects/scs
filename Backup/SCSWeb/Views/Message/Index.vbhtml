﻿@ModelType IEnumerable(Of SCSWeb.Message)

@Code
    ViewData("Title") = "Index"
End Code


@If Session("ViewingOnly").ToString() = "True" Then
     @Html.Partial("_ViewOnlyIndex", Model)
Else
     @Html.Partial("_FullAccessIndex", Model)
End If

