﻿@ModelType SCSWeb.Message

@Code
    ViewData("Title") = "Details"
End Code

<h2>Details</h2>

<fieldset>
    <legend>Message</legend>

    <div class="display-label">
        @Html.DisplayNameFor(Function(model) model.Application)
    </div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Application)
    </div>

    <div class="display-label">
        @Html.DisplayNameFor(Function(model) model.Company)
    </div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Company)
    </div>
</fieldset>
<p>
    @*@Html.ActionLink("Edit", "Edit", New With {.id = Model.PrimaryKey}) |*@
    @Html.ActionLink("Back to List", "Index")
</p>
