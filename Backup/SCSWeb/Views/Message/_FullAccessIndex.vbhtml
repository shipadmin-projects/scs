﻿@ModelType IEnumerable(Of SCSWeb.Message)

<h2>Index</h2>

<p>
    @Html.ActionLink("Create New", "Create")
</p>
<table>
    <tr>
        <th></th>
        <th>
            @Html.DisplayNameFor(Function(model) model.Application.Name)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.Company.Name)
        </th>
            <th>
            @Html.DisplayNameFor(Function(model) model.Installation.Name)
        </th>
            <th>
            @Html.DisplayNameFor(Function(model) model.MessageHeader)
        </th>
                    <th>
            @Html.DisplayNameFor(Function(model) model.MessageDetails)
        </th>

    </tr>

    @For Each item In Model
        Dim currentItem = item
        @<tr>
            <td>
                @If item.Status = "New" Then
                    @Html.ActionLink("New", "ChangeStatus", New With {.id = currentItem.ID, .status = "New"})
                Else
                    @Html.ActionLink("Done", "ChangeStatus", New With {.id = currentItem.ID, .status = "Done"})
                End If
                

            </td>
            <td>
                @Html.DisplayFor(Function(modelItem) currentItem.Application.Name)
            </td>
            <td>
                @Html.DisplayFor(Function(modelItem) currentItem.Company.Name)
            </td>
                        <td>
                @Html.DisplayFor(Function(modelItem) currentItem.Installation.Name)
            </td>
                           <td>
                @Html.DisplayFor(Function(modelItem) currentItem.MessageHeader)
            </td>
                                 <td>
                @Html.DisplayFor(Function(modelItem) currentItem.MessageDetails)
            </td>

        </tr>
    Next
</table>
