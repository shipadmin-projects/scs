﻿@ModelType SCSWeb.Message

@Code
    ViewData("Title") = "Details"
End Code

<h2>Details</h2>

<fieldset>
    <legend>Message</legend>

    <div class="display-label">
        @Html.DisplayNameFor(Function(model) model.ApplicationID)
    </div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.ApplicationID) --
        @ViewBag.AppName
    </div>

    <div class="display-label">
        @Html.DisplayNameFor(Function(model) model.CompanyID)
    </div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.CompanyID) --
        @ViewBag.CmpName
    </div>

    <div class="display-label">
        @Html.DisplayNameFor(Function(model) model.InstallationID)
    </div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.InstallationID) --
        @ViewBag.InsName
    </div>

    <div class="display-label">
        @Html.DisplayNameFor(Function(model) model.Status)
    </div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Status)
    </div>

    <div class="display-label">
        @Html.DisplayNameFor(Function(model) model.StatusChangedByUserID)
    </div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.StatusChangedByUserID) --
        @ViewBag.uName
    </div>

    <div class="display-label">
        @Html.DisplayNameFor(Function(model) model.Type)
    </div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Type)
    </div>

    <div class="display-label">
        @Html.DisplayNameFor(Function(model) model.MessageHeader)
    </div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.MessageHeader)
    </div>

    <div class="display-label">
        @Html.DisplayNameFor(Function(model) model.MessageDetails)
    </div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.MessageDetails)
    </div>

    <div class="display-label">
        @Html.DisplayNameFor(Function(model) model.Timestamp)
    </div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Timestamp)
    </div>

       <div class="display-label">
        @Html.DisplayNameFor(Function(model) model.Updated)
    </div>
    <div class="display-field">
        @Html.DisplayFor(Function(model) model.Updated)
    </div>
   
</fieldset>
<p>
    @*@Html.ActionLink("Edit", "Edit", New With {.id = Model.PrimaryKey}) |*@
    @Html.ActionLink("Back to List", "Index")
</p>
