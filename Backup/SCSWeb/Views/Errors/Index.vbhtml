﻿@ModelType IEnumerable(Of SCSWeb.ErrorModel)

@Code
    ViewData("Title") = "Errors"

End Code


<script type="text/javascript">

    var page = 0;
    var _inCallback = false;

    function loadProducts() {
        if (page > -1 && !_inCallback) {
            _inCallback = true;
            page++;
            $('div#loading').html('<p><img src="../Content/Images/loading.gif"></p>');

            $.get("/Errors/Index/" + page, function (data) {
                if (data != '') {
                    
                    $("#productList").append(data);
                    
                }
                else {
                    page = -1;
                }

                _inCallback = false;
                $('div#loading').empty();
                $('#PartialDiv').load('@Url.Action("RefreshCount", "Errors")');

            });
        }
    }

    var dcList = true;

    $(window).scroll(function () {

        if ($(window).scrollTop() == $(document).height() - $(window).height()) {

            loadProducts();
        }
    });
</script>

<script type="text/javascript">
    function onSelectedIndexChanged() {
        var sub = document.getElementsByName("search");
        sub.value = "";
        document.forms[0].submit();
    }

    function selectDeselectAll(checked) {
        var index;
        var checkboxes = document.getElementsByName("selectedObjects");
        for (index = 0; index < checkboxes.length; ++index) {
            checkboxes[index].checked = checked;
        }
    }
   

</script>

<h2>Errors</h2>

<p>
    @Using Html.BeginForm("Index", "Errors", FormMethod.Post)
        @Html.TextBox("SearchString")
        @<input type="submit" value="Search" name="search" />

        @*Dim newgrid = New WebGrid(Model, {"ID", "MessageHeader", "Application.Name"}, "ID", 16, True, True, Nothing, Nothing, Nothing, Nothing, "ID", Nothing, Nothing)

            @newgrid.GetHtml(
                tableStyle:="grid",
                headerStyle:="header",
                columns:=newgrid.Columns(
                newgrid.Column("ID", "ID", Nothing, Nothing, True),
                newgrid.Column("Message Header", format:=Function(str) Html.ActionLink(CType(str, System.Web.Helpers.WebGridRow).Value.MessageHeader, "Details", New With {.id = CType(str, System.Web.Helpers.WebGridRow).Value.ID}))
                )) *@

        @Html.DropDownList("SearchCompanyID", New SelectList(ViewData("SearchCompany"), "Value", "Text"), "-- Select Company -- ", New With {.onChange = "onSelectedIndexChanged()"})
        @Html.DropDownList("SearchApplicationID", New SelectList(ViewData("SearchApplication"), "Value", "Text"), "-- Select Application -- ", New With {.onChange = "onSelectedIndexChanged()"})
        @Html.DropDownList("SearchInstallationID", New SelectList(ViewData("SearchInstallation"), "Value", "Text"), "-- Select Installation -- ", New With {.onChange = "onSelectedIndexChanged()"})
        @Html.DropDownList("SearchColumnID",  New SelectList(ViewData("SearchColumns"), "Value", "Text"), "-- Select Column -- ", New With {.onChange = "onSelectedIndexChanged()"})

        @<br />
        @If (ViewBag.Show = True) Then
            @Html.TextBox("Number")
            @<input type="checkbox" name="search" value="ShowAllErrorResults" onchange="onSelectedIndexChanged();" />
            @Html.Displayname("Show All errors found")
        End If
        
    End Using

</p>

<p>
     @If (ViewBag.SearchString) <> "" Then
        @Html.DisplayName("You searched for: ") 
        @ViewBag.SearchString 
     End If

</p>

<div id="p1">

</div>

<div id="PartialDiv">
    @Html.Partial("_ErrorCount")
  
</div>




@*<p>
    @Session("TotalErrors") 
</p>*@
@Using Html.BeginForm("SubmitMultiple", "Errors")

    @<input type="submit" name="submitButton" value="Mark as Done" />

@<table style="width: 1000px;">

    <tr>
        <th style="width: 30px;">
            <input type="checkbox" name="selectedAllObjects" onchange="selectDeselectAll(this.checked);" style="border: 10px solid black">
        </th>

        <th style="width: 60px;">
            ID
        </th>

        <th style="width: 200px;">
            Installation
        </th>

        <th style="width: 190px;">
            Timestamp
        </th>

        <th style="width: 190px;">
            Error
        </th>

        <th style="width: 220px;">
            Details
        </th>


       @* <th style="width: 50px;"></th>*@

    </tr>
</table>
@<div id="productList">
    @Html.Partial("_Errors")
</div>
End Using
<div id="loading"></div>

