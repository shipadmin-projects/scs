﻿@ModelType IEnumerable(Of SCSWeb.Message)

@Code
    ViewData("Title") = "Errors"
End Code


    
<h2>Errors</h2>

<p>
    @ViewBag.TotalErrors new errors
</p>

<p>s
    @Using Html.BeginForm("Search", "Errors", FormMethod.Post)
        @Html.TextBox("SearchString")
        @<input type="submit" value="Search" />
    End Using
       
</p>
<p>
    You searched for: @ViewBag.SearchString
</p>

<table>
    <tr>
        <th>
           Application
        </th>
        <th>
            Company
        </th>
        <th>
            Installation
        </th>
        <th>
           Status
        </th>
        <th>
            Status Changed By
        </th>
        <th>
           Type
        </th>
        <th>
           MessageHeader
        </th>
        <th>
            MessageDetails
        </th>
        <th>
           Timestamp
        </th>
        <th></th>
    </tr>

@For Each item In Model
    Dim currentItem = item
    @<tr>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Application.Name)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Company.Name)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Installation.Name)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Status)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.StatusChangedByUser.Name)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Type)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.MessageHeader)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.MessageDetails)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Timestamp)
        </td>
        <td>
            @Html.ActionLink("Details", "Details", New With {.id = currentItem.ID})        </td>
    </tr>
Next

</table>
