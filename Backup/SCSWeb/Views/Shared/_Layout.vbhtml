﻿<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>@ViewData("Title") - SCS</title>
        <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
        <meta name="viewport" content="width=device-width" />
        @Styles.Render("~/Content/Shipadmin")
        @Styles.Render("~/Content/css")
        @Scripts.Render("~/bundles/modernizr")
        @Scripts.Render("~/bundles/jqueryval")
    </head>
    <body>
        <header>
            <div class="content-wrapper">
                <div class="float-left">
                    <p class="site-title">@Html.ActionLink("Shipadmin Control System", "Index", "Home")                       
                    </p>
                </div>
                <div class="float-right">
                    <section id="login">
                        @Html.Partial("_LoginPartial")
                    </section>
                         @Html.Partial("_NavigationPartial")
                  @*  <nav>
                        <ul id="menu">
                            <li>@Html.ActionLink("Home", "Index", "Home")</li>
                            
                             <li>@Html.ActionLink("Errors", "Index", "Errors")</li>
                            <li>@Html.ActionLink("Table Compare", "Index", "TableCompare")</li>
                            <li>@Html.ActionLink("Applications", "Index", "Application")</li>
                            <li>@Html.ActionLink("Companies", "Index", "Company")</li>
                            <li>@Html.ActionLink("Messages", "Index", "Message")</li>
                             <li>@Html.ActionLink("Installations", "Index", "Installation")</li>
                             <li>@Html.ActionLink("Users", "Index", "User")</li>

                        </ul>
                    </nav>*@
                </div>
            </div>
        </header>
        <div id="body">
            @RenderSection("featured", required:=false)
            <section class="content-wrapper main-content clear-fix">
                @RenderBody()
            </section>

        </div>
        <footer>
            <div class="content-wrapper">
                <div class="float-left">
                    <p>&copy; @DateTime.Now.Year - Shipadmin Control System</p>
                </div>
            </div>
        </footer>

    
        @RenderSection("scripts", required:=False)

        <script type="text/javascript" src="//assets.zendesk.com/external/zenbox/v2.6/zenbox.js"></script>
        <style type="text/css" media="screen, projection">
            @@import url(//assets.zendesk.com/external/zenbox/v2.6/zenbox.css);
        </style>
        <script type="text/javascript">
            if (typeof (Zenbox) !== "undefined") {
                Zenbox.init({
                    dropboxID: "20302466",
                    url: "https://shipadmin.zendesk.com",
                    tabTooltip: "Support",
                    tabImageURL: "https://assets.zendesk.com/external/zenbox/images/tab_support.png",
                    tabColor: "black",
                    tabPosition: "Left"
                });
            }
        </script>

        <script type="text/javascript" src="//assets.zendesk.com/external/zenbox/v2.6/zenbox.js"></script>
        <style type="text/css" media="screen, projection">
        @@import url(//assets.zendesk.com/external/zenbox/v2.6/zenbox.css);
        </style>
        <script type="text/javascript">
            if (typeof (Zenbox) !== "undefined") {
                Zenbox.init({
                    dropboxID: "20291923",
                    url: "https://shipadmin.zendesk.com",
                    tabTooltip: "Support",
                    tabImageURL: "https://assets.zendesk.com/external/zenbox/images/tab_support.png",
                    tabColor: "black",
                    tabPosition: "Left"
                });
            }
        </script>
    </body>
</html>
