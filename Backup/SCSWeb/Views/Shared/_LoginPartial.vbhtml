﻿@If Session("UserID") <> Nothing Then
    Dim db As New SCSWeb.SCSDB
    Dim i As Integer = Convert.ToInt32(Session("UserID"))
    Dim U As SCSWeb.User = db.Users.Where(Function(x) x.ID = i).FirstOrDefault()
    @<text>
        @*Hello, @Html.ActionLink(User.Identity.Name, "Manage", "Account", routeValues:=Nothing, htmlAttributes:=New With {.class = "username", .title = "Manage"})!*@
      @*  @Using Html.BeginForm("LogOff", "Account", FormMethod.Post)
           @<input type="button" name="Logoff" id="Logoff"/>
            @<a href="javascript:document.getElementById('logoutForm').submit()">Log off</a>
        End Using*@
       <ul>
        Hello! @U.Name
        <li>@Html.ActionLink("LogOff", "LogOut", "Account")</li>
           </ul>
    </text>
    
Else
    @<ul>
      @*  <li>@Html.ActionLink("Register", "Register", "Account", routeValues:=Nothing, htmlAttributes:=New With {.id = "registerLink"})</li>*@
      @*  <li>@Html.ActionLink("Register", "Register", "User", routeValues:=Nothing, htmlAttributes:=New With {.id = "registerLink"})</li>*@
        <li>@Html.ActionLink("Log in", "Login", "Account", routeValues:=Nothing, htmlAttributes:=New With {.id = "loginLink"})</li>
    </ul>
End If
