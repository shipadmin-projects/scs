﻿@ModelType IEnumerable(Of SCSWeb.Installation)

@Code
    ViewData("Title") = "Installations"
End Code

<h2>Installations</h2>

@Using Html.BeginForm("Index", "Installation", FormMethod.Post)

    @Html.DropDownList("SearchCompany", New SelectList(ViewData("SearchCompany"), "Value", "Text"), "-- Select Company -- ", New With {.onChange = "this.form.submit();"}) 
    @Html.DropDownList("SearchApplication", New SelectList(ViewData("SearchApplication"), "Value", "Text"), "-- Select Application -- ", New With {.onChange = "this.form.submit();"}) 

End Using

<table id="iList">
    <tr>
        <th>
            @Html.DisplayNameFor(Function(model) model.Name)
        </th>
        <th>
            Company
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.Updated)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.InActive)
        </th>
    </tr>
        <input type="button" id="sortbtn" value="Save" /><div id="AlertList"></div>
        @For Each item In Model
            Dim currentItem = item
            @<tr>
                <td>
                    @Html.DisplayFor(Function(modelItem) currentItem.Name)
                </td>
                <td>
                    @Html.DisplayFor(Function(modelItem) currentItem.Company.Name)
                </td>
                <td>
                    @Html.DisplayFor(Function(modelItem) currentItem.Updated)
                </td>
                <td>
                    @Html.CheckBox("CheckBox", currentItem.InActiveBool, New With {Key .id = currentItem.ID.ToString})
                </td>
            </tr>
        Next
</table>

@Scripts.Render("~/bundles/jqueryval")



      
 