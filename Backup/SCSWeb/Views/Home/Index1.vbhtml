﻿@Code
    ViewData("Title") = "Home"
    Layout = "~/Views/Shared/_LayoutNoMaster.vbhtml"
End Code
<script type="text/javascript">
 
    function setRefreshTime() {
        setTimeout("refreshTime()", 60000); // every 1 min
        //setTimeout("refreshTime()", 30000); // every 30 sec
        // setTimeout("refreshTime()", 1500); // every 15 sec

    }

    function refreshTime() {
        document.getElementById('refreshbutton').click();

        setRefreshTime();
      
    }

    $(document).ready(function () {
        setRefreshTime();
    });
</script>

            <table style="width:60%;margin-left:-100px;">
                <tr>
                    <td>
                        <div style="margin-left:50px;font-size:24px;">
                            ERRORS:
                        </div>
                        <a href="~/Errors" class="boxlink">
                            <div class="@ViewBag.ErrorsStatusBoxCSS">
                                @ViewData("NewErrorMessages")
                            </div>
                        </a>
                    </td>
                    <td>
                         <div style="margin-left:50px;font-size:24px;">
                            DIFFERENCES:
                        </div>
            
                        <a href="~/TableCompare" class="boxlink">
                            <div class="@ViewBag.TableCompareStatusBoxCSS">
                                @ViewData("TableCompareUnequalTables") / @ViewData("TableCompareUnequalRows")
                            </div>
                        </a>
                    </td>
                </tr>
            </table>
       


<div style="margin-left:-50px;font-size:24px;">
            Acitivities within 24hrs
</div>
<div style="margin-left:-50px;font-size:16px;">
             <br /> 
             New errors: &nbsp; <strong>@ViewData("NewErrors24Hrs") </strong>
             <br /> 
             Errors updated to Done: &nbsp;<strong> @ViewData("NewErrorsDone24Hrs")  </strong>
@*             <br /> 
             Table differences: &nbsp; <strong>@ViewData("TableCompareUnequalTables24hrs") / @ViewData("TableCompareUnequalRows24hrs") </strong>*@
</div> 

@Using Html.BeginForm("Index1", "Home")
                  
  @<input type="submit" name="submitButton" id="refreshbutton" value="Done" style="display:none" /> 
              
        
End Using  
 