﻿@Code
    ViewData("Title") = "Home"
End Code

<table>
    <tr>
        <td>
            <div style="margin-left:50px;font-size:24px;">
                ERRORS:
            </div>
            <a href="~/Errors" class="boxlink">
                <div class="@ViewBag.ErrorsStatusBoxCSS">
                    @ViewData("NewErrorMessages")
                </div>
            </a>
        </td>
        <td>
             <div style="margin-left:50px;font-size:24px;">
                DIFFERENCES:
            </div>
            
            <a href="~/TableCompare" class="boxlink">
                <div class="@ViewBag.TableCompareStatusBoxCSS">
                    @ViewData("TableCompareUnequalTables") / @ViewData("TableCompareUnequalRows")
                </div>
            </a>
        </td>
    </tr>
</table>
<div style="margin-left:40px;font-size:16px;">
    @Html.ActionLink("Status Screen", "Index1")  

    <br>
    <br>
    TOP 10 Errors:

</div>
 


 <div style="margin-top:40px;margin-left:40px;font-size:12px;">
        @If TempData("Top10Errors").count > 0 Then
            Dim errors As List(Of SCSWeb.TopErrorsModel) = TempData("Top10Errors")
            Dim error1 As SCSWeb.TopErrorsModel
            @<div>
                <table>
                      <tr>
                          <th>Count   <br />   <br /></th>
                          <th>Message Details   <br />   <br /></th>
                       
                     </tr>
                      

                        @For Each error1 In errors
                       
                        @<tr>
                         <td style="vertical-align:top;">  
                             @Html.ActionLink(error1.Number, "Index1", "Errors", New With {.SearchString = error1.MessageDetails}, System.DBNull.Value) 
                          </td>
                          <td style="vertical-align:top;"> 
                              @error1.MessageDetails
                          </td>
                         </tr>                 
                        Next
                        
                     
                   </table>
            </div>
        End if 
</div>


 