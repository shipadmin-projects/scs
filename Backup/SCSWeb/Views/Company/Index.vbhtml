﻿@ModelType IEnumerable(Of SCSWeb.Company)

@Code
    ViewData("Title") = "Companies"
End Code

<h2>Companies</h2>

<table>
    <tr>
        <th>
            @Html.DisplayNameFor(Function(model) model.Name)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.Updated)
        </th>
        <th></th>
    </tr>

@For Each item In Model
    Dim currentItem = item
    @<tr>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Name)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) currentItem.Updated)
        </td>
    </tr>
Next

</table>
